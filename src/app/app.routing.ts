import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { FormsComponent } from './views/base/forms.component';
import {RegisterUserComponent} from './views/register-user/register-user.component';
import {ForgotPasswordComponent} from './views/forgot-password/forgot-password.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'form',
    component: FormsComponent,
    data: {
      title: 'Form'
    }
  },
  {
    path: 'register',
    component: RegisterUserComponent,
    data: {
      title: 'Registro'
    },
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent,
    data: {
      title: 'Cambiar contraseña'
    },
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'enterprises',
        loadChildren: () => import('./views/enterprise/enterprise.module').then(m => m.EnterpriseModule)
      },
      {
        path: 'users',
        loadChildren: () => import('./views/user/user.module').then(m => m.UserModule)
      },
      {
        path: 'roles',
        loadChildren: () => import('./views/rol/rol.module').then(m => m.RolModule)
      },
      {
        path: 'reportes-conciliacion',
        loadChildren: () => import('./views/reports/reports-conciliation/reports-conciliation.module').then(m => m.ReportsConciliationModule)
      },
      {
        path: 'reportes-interfaces',
        loadChildren: () => import('./views/reports/reports-interfaces/reports-interfaces.module').then(m => m.ReportsInterfacesModule)
      },
      {
        path: 'reportes-outs',
        loadChildren: () => import('./views/reports/reports-out/reports-out.module').then(m => m.ReportsOutModule)
      },
      {
        path: 'config-conciliation',
        loadChildren: () => import('./views/config-conciliation/config-conciliation.module').then(m => m.ConfigConciliationModule)
      },
      {
        path: 'base',
        loadChildren: () => import('./views/base/base.module').then(m => m.BaseModule)
      },
      {
        path: 'templates',
        loadChildren: () => import('./views/templates/templates.module').then(m => m.TemplatesModule)
      },
      {
        path: 'template-out',
        loadChildren: () => import('./views/template-out/template-out.module').then(m => m.TemplateOutModule)
      },
      {
        path: 'out',
        loadChildren: () => import('./views/out/out.module').then(m => m.OutModule)
      },
      {
        path: 'charts',
        loadChildren: () => import('./views/chartjs/chartjs.module').then(m => m.ChartJSModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'icons',
        loadChildren: () => import('./views/icons/icons.module').then(m => m.IconsModule)
      },
      {
        path: 'notifications',
        loadChildren: () => import('./views/notifications/notifications.module').then(m => m.NotificationsModule)
      },
      {
        path: 'theme',
        loadChildren: () => import('./views/theme/theme.module').then(m => m.ThemeModule)
      },
      {
        path: 'widgets',
        loadChildren: () => import('./views/widgets/widgets.module').then(m => m.WidgetsModule)
      },
      {
        path: 'interface',
        loadChildren: () => import('./views/interface/interface.module').then(m => m.InterfaceModule)
      }
    ]
  },
  { path: '**', component: P404Component }
];

// @NgModule({
//   imports: [ RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' }) ],
//   exports: [ RouterModule ]
// })
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
