import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'textTransform'
})
export class TextTransformPipe implements PipeTransform {
  transform(value: any, cont?: number): any {
    
    if (value === null || value === '') {
      return value = '-';
      
    } else {
      if (value && value.length > 9) {
        const valor = value.slice(0, cont ? cont : 9) + (value.length <= cont ? '' : '...')
        return valor;
      } else {
        return value;
      }
    }
  }
}
@Pipe({
  name: 'textTransformTemplate'
})
export class TextTransformPipeTemplate implements PipeTransform {
  transform(value: any, cont?: number): any {
    if (value === null || value === '') {
      return '-';
    } else {
      if (value && value.length > 11) {
        return value.slice(0, cont ? cont : 11) + (value.length <= cont ? '' : '...');
      } else {
        return value;
      }
    }
  }
}

@Pipe({
  name: 'slash'
})
export class Slash implements PipeTransform {
  transform(value: string, cont?: number): string {
   
   if (value === null || value === '') {
      return '-';
    } else {
      if (value && value.length > 11) {
        return value.slice(0, cont ? cont : 11) + (value.length <= cont ? '' : '...');
      } else {
        return value;
      }
    }
   
  }
}
@Pipe({
  name: 'conciliationData'
})
export class conciliationData implements PipeTransform {
  transform(value: string): string {
   
   if (value === null || value === '') {
      return '-';
    } else {
      if (value && value.length > 11) {
        return value.slice(5,value.length );
      } else {
        return value;
      }
    }
   
  }
}