import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'slashTransform'
})
export class SlashTransformPipe implements PipeTransform {

  transform(value: string): string {
    // var parts = value.split("/");
    // var result = parts[parts.length - 1]
    const newValue = /[^/]*$/.exec(value)[0];
    return newValue;
  }

}
