import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextTransformPipe, TextTransformPipeTemplate, conciliationData } from './text-transform.pipe';
import { SlashTransformPipe } from './slash-transform.pipe';



@NgModule({
  declarations: [TextTransformPipe, TextTransformPipeTemplate, SlashTransformPipe, conciliationData],
  imports: [
    CommonModule
  ],
  exports: [TextTransformPipe, TextTransformPipeTemplate, SlashTransformPipe, conciliationData],
  providers : [TextTransformPipe, TextTransformPipeTemplate, SlashTransformPipe, conciliationData]
})
export class PipesModule { }
