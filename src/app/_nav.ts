import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    title: true,
    name: 'Administración'
  },
  {
    name: 'Empresas',
    icon: 'icon-home',
    url: '/enterprises',
  },
  {
    name: 'Usuarios',
    icon: 'icon-people',
    url: '/users',

  },
  {
    name: 'Roles',
    url: '/roles',
    icon: 'icon-shield',

  },
  {
    title: true,
    name: 'Operaciones'
  },
  {
    name: 'Templates',
    icon: 'icon-docs',
    url: '/templates',

  },
  {
    name: 'Interfaces',
    icon: 'icon-docs',
    url: '/interface',
  },
  {
    name: 'Conf Conciliaciones',
    icon: 'icon-docs',
    url: '/config-conciliation',

  },
  {
    divider: true
  },

  {
    title: true,
    name: 'Reportes'
  },
  {
    name: 'Reportes',
    icon: 'icon-docs',
    url: '/reportes',

  },
  {
    divider: true
  }
];


