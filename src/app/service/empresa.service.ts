import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Enterprise } from '../models/enterprise';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class EmpresaService {

  constructor(private http: HttpClient) { }
  private baseUrl = environment.apiUrl + 'enterprises'
  
   createEnterprise(body: any): Observable<Enterprise> {
    return this.http.post<Enterprise>(this.baseUrl, body);
  }
}
