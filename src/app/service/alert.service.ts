import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { Alert } from '../models/alert'

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  Alert: any;
  constructor() {
    this.Alert = Swal.mixin({
      showCloseButton: false,
      heightAuto: false,
      //  confirmButtonText: '',
      // cancelButtonText: 'Cerrar',

    });
  }

  success(resp: Alert) {
    this.Alert.fire({
      icon: 'success',
      title: 'Correcto',
      text: resp.text,
      timer: 3000,
      showCancelButton: false,
      showConfirmButton: false
    });
  }

  error(resp: Alert) {

    if (resp.text === 'Unauthorized') {
      const resp = 'No autorizado';
    }
    this.Alert.fire({
      icon: 'error',
      title: resp.title,
      text: resp.text,
      timer: 4000,
      cancelButtonColor: 'red',
      showCancelButton: false,
      showConfirmButton: false,
      cancelButtonText: 'Cerrar',
      allowEscapeKey: false,
      allowOutsideClick: false,
    });
  }

  info(resp: Alert) {
    this.Alert.fire({
      icon: 'info',
      title: resp.title,
      text: resp.text,
      timer: 4000,
      showCancelButton: false,
      showConfirmButton: false
    });
  }
}
