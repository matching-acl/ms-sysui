import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[onlyLetters]'
})
export class OnlyLettersDirective {

  constructor() { }

  
  @HostListener('keypress', ['$event'])
  keypress(event) {
    const pattern = /^[a-zA-Z\s]*$/;
    let inputChar = String.fromCharCode(
      event.which ? event.which : event.keyCode
    );

    if (!pattern.test(inputChar)) {
      event.preventDefault();
      return false;
    }
  }

}
