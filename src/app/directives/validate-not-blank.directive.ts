import { Directive, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[appValidateNotBlank]'
})
export class ValidateNotBlankDirective {

  constructor(public ngControl: NgControl) { }

  @HostListener('keyup', ['$event'])
  keyup(event) {
    let value = event.target.value
    if (value.trim() === "") {
      if (!this.ngControl.control.hasError('required')) {
        this.ngControl.control.setErrors({ required: true });
      }
    }
  }
  @HostListener('blur', ['$event'])
  setInputFocusOut(event) {
    let value = event.target.value
    this.ngControl.control.setValue(value.trim());
    this.ngControl.control.updateValueAndValidity();
  
    
  }

}
