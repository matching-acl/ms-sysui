import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ValidateNotBlankDirective } from './validate-not-blank.directive';
import { CharactersValidDirective } from './characters-valid.directive';
import { OnlyNumberDirective } from './only-number.directive';
import { OnlyLettersDirective } from './only-letters.directive';



@NgModule({
  declarations: [ValidateNotBlankDirective, CharactersValidDirective, OnlyNumberDirective, OnlyLettersDirective],
  imports: [
    CommonModule
  ],
  exports: [ValidateNotBlankDirective, CharactersValidDirective, OnlyNumberDirective, OnlyLettersDirective]
})
export class DirectivesModule { }
