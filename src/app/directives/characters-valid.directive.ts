import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[passwordCharacters]'
})
export class CharactersValidDirective {

  constructor() { }

  @HostListener('keypress', ['$event'])
    keypressPass(event: any) {
    // const value = event.target.value;
     const charCode = event.which ? event.which : event.keyCode;
     if (charCode < 48 || charCode > 57  && charCode < 65 || charCode > 90 && charCode < 97 || charCode > 122) {
      return false;
    }
  }

  @HostListener('keypress', ['$event'])
    onlyNumber(event: any) {
    // const value = event.target.value;
     const charCode = event.which ? event.which : event.keyCode;
     if (charCode < 48 || charCode > 57  && charCode < 65 || charCode > 90 && charCode < 97 || charCode > 122) {
      return false;
    }
  }

}
