import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[onlyNumber]'
})
export class OnlyNumberDirective {

  constructor() { }

  @HostListener('keypress', ['$event'])
    keypressNumber(event: any) {
    
     const charCode = event.which ? event.which : event.keyCode;
     if (charCode < 48 || charCode > 57 ) {
      return false;
    }
  }

}
