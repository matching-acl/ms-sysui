import {Component, OnInit, TemplateRef} from '@angular/core';
import {Router} from '@angular/router';
// @ts-ignore
import {CLIENT_ID, ENTERPRISE_CODE, ENTERPRISE_NAME, PERMISSIONS, ROLE, USER_NAME, VERSION_SYSTEM} from '../../views/login/models/login';
import {INavData} from '@coreui/angular';
// @ts-ignore
import {PrivilegesName} from '../../views/rol/model/rol';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
// @ts-ignore
import {LoginService} from '../../views/login/service/login.service';
import {Enterprise, EnterprisesAllUser} from '../../views/enterprise/model/enterprise';
import {ListVersions} from './models/layout.model';
import { FormBuilder} from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit {
  public sidebarMinimized = false;
  navItems: INavData[] = [];
  userName = null;
  enterpriseName = null;
  enterpriseCode = null;
  role = null;
  otherEnterprise = null;
  client_id = null;
  version = null;
  year: number;
  modalRef: BsModalRef;
  listVersions: ListVersions[] = [];
  enterpriseSelected: Enterprise = null;
  spinner = false;
  constructor(
    private router: Router,
    private modalService: BsModalService,
    private loginService: LoginService,
    private fb: FormBuilder
  ) {
    this.year = new Date().getFullYear();
    this.updateValuesEnterprises();
  }

  updateValuesEnterprises() {
    this.userName = localStorage.getItem(USER_NAME);
    this.enterpriseName = localStorage.getItem(ENTERPRISE_NAME);
    this.enterpriseCode = localStorage.getItem(ENTERPRISE_CODE);
    this.role = localStorage.getItem(ROLE);
    this.client_id = localStorage.getItem(CLIENT_ID);

    this.loginService.getEnterprisesByUser(this.userName).subscribe((data) => {
      if ( data && data.enterprises && data.enterprises.elements && data.enterprises.elements.length > 0 ) {
        this.otherEnterprise = (data.enterprises.elements && data.enterprises.elements.length > 1) ?
          (data.enterprises.elements.filter(f => f.code !== this.enterpriseCode) ?
            (data.enterprises.elements.filter(f => f.code !== this.enterpriseCode).map(
              (e: any) =>
                <EnterprisesAllUser>{
                  name: e.name,
                  code: e.code
                })) : []) : null;
      }
    }, (err) => {});

    this.loginService.getListVersions(+this.client_id).subscribe((data) => {
      console.log({data: data});
      if ( data && data.length > 0) {
        console.log({DATAA: data});
        this.listVersions = data;
      }
    }, (err) => {});

    this.setNavItems(this.permissions);
  }

  get permissions(): string[] {
    const permissions = localStorage.getItem(PERMISSIONS);
    return permissions ? permissions.split(',') : [];
  }

  setNavItems(permission: string[]) {
    if (permission && permission.length > 0) {
      this.navItems = [];
      permission.forEach(perm => {
        // console.log({perm: perm});
         if (perm === PrivilegesName.admin) {
           this.navItems.push(
             {
               title: true,
               name: 'Administración'
             });
           this.navItems.push(
             {
               name: 'Empresas',
               icon: 'icon-home',
               url: '/enterprises',
             });
           this.navItems.push(
             {
               name: 'Usuarios',
               icon: 'icon-people',
               url: '/users',
             }
           );
           this.navItems.push(
             {
               name: 'Roles',
               url: '/roles',
               icon: 'icon-shield',
             }
           );
         }
         if (perm === PrivilegesName.gestion) {
           this.navItems.push(
             {
               title: true,
               name: 'Configuración'
             });
           this.navItems.push(
             {
               name: 'Templates entrada',
               icon: 'icon-docs',
               url: '/templates',

             });
             this.navItems.push(
               {
                 name: 'Carga interfaces',
                 icon: 'icon-docs',
                 url: '/interface',
               });
          //  this.navItems.push(
          //    {
          //      name: 'Templates salida',
          //      icon: 'icon-docs',
          //      url: '/template-out',

          //    });
          //  this.navItems.push(
          //    {
          //      name: 'Destinos salida',
          //      icon: 'icon-docs',
          //      url: '/out',
          //    });
           this.navItems.push(
             {
               name: 'Conciliaciones',
               icon: 'cil-puzzle',
               url: '/config-conciliation',

             }
           );
         }
         if (perm === PrivilegesName.reportes) {
          this.navItems.push(
            {
              divider: true
            });
           this.navItems.push(
            {
              title: true,
              name: 'Reportes'
            });
           this.navItems.push(
            {
              name: 'Conciliación',
              icon: 'cil-task',
              url: '/reportes-conciliacion',
            });
           this.navItems.push(
            {
              name: 'Interfaces',
              icon: 'cil-task',
              url: '/reportes-interfaces',
            });
          //  this.navItems.push(
          //   {
          //     name: 'Interfaces de salida',
          //     icon: 'cil-task',
          //     url: '/reportes-outs',
          //   });
           this.navItems.push(
            {
              divider: true
            }
          );
        }
      });
    } else {
      this.router.navigateByUrl('/login');
    }
  }

  ngOnInit(): void {
  }

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  Logout() {
    localStorage.clear();
    this.router.navigateByUrl('/login');
  }

  get OtherEnterprise() {
    return this.otherEnterprise;
  }

  onChange(enterprise: Enterprise) {
    console.log({EVENTO: enterprise});
    this.enterpriseSelected = enterprise;
  }

  changeEnterprise() {
    const enterprise_code = this.enterpriseSelected.code;
    const enterprise_name = this.enterpriseSelected.name;
    console.log({enterprise_name: enterprise_name, enterprise_code: enterprise_code});
    setTimeout(() => {
      Swal.fire({
        icon: 'info',
        text: `¿Está seguro que desea cambiar a ${enterprise_name}?`,
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Aceptar'
      }).then((result) => {
        if (result.isConfirmed) {
          this.spinner = true;
          this.loginService.getChangeEnterprise(this.userName, enterprise_code).subscribe((data) => {
            console.log({dataaaCHANGEEE: data});
            if (data && data.status === '200') {
              if (data.active && data.role && data.role.privileges && data.role.privileges.length > 0) {
                const role = data.role.name;
                let permission_add = [];
                data.role.privileges.forEach(p => permission_add.push(p.name));
                permission_add = permission_add.sort();
                const enterprises_name = data.schemaName;
                const user_id = data.id;
                const client_id = data.clientId;
                this.loginService.setLocalStorageAndRedirectByLogin(enterprise_code, enterprises_name, this.userName,
                  user_id, permission_add, role, client_id);
                this.updateValuesEnterprises();
              }
            }
            this.spinner = false;
          }, (err) => {
            console.log({ERRORRR: err}) ;
            this.spinner = false;
          });
        }
      });
    });
  }

}
