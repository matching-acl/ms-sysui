export class ListVersions {
  id: number;
  version?: string;
  description?: string;
  created?: string;
  updated?: string;
  payment?: boolean;
  active?: boolean;
}
