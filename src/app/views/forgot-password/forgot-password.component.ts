import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ForgotPasswordService} from './service/forgot-password.service';
import {AlertService} from '../../service/alert.service';
import Swal from "sweetalert2";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  spinner = false;
  form: FormGroup;
  navigation_state: any;
  constructor(
    private _fb: FormBuilder,
    private _router: Router,
    private _forgotPasswordService: ForgotPasswordService,
    private _alert: AlertService) {
    const navigation = this._router.getCurrentNavigation();
    this.navigation_state = null;
    if (navigation && navigation.extras && navigation.extras.state) {
      this.navigation_state = navigation.extras.state;
      console.log({NAV_STATE: this.navigation_state});
    }
  }

  ngOnInit(): void {
    this.createForm();

    const inputs = document.querySelectorAll('.input');
    const selects = document.querySelectorAll('.select');

    function focusFunc() {
      const parent = this.parentNode.parentNode;
      parent.classList.add('focus');
    }
    function blurFunc() {
      const parent = this.parentNode.parentNode;
      if (this.value === '') {
        parent.classList.remove('focus');
      }
    }

    inputs.forEach(input => {
      input.addEventListener('focus', focusFunc);
      input.addEventListener('blur', blurFunc);
    });
    selects.forEach(select => {
      select.addEventListener('focus', focusFunc);
      select.addEventListener('blur', blurFunc);
    });
  }
  createForm() {
    this.form = this._fb.group({
      user_name: [this.navigation_state && this.navigation_state.user_name ? this.navigation_state.user_name : null,
                  [Validators.required]],
      security_code: [null, [Validators.required]]
    });
  }

  // Formularios validos
  get userNameValid() {
    return this.form.get('user_name').valid;
  }

  get securityCodeValid() {
    return this.form.get('security_code').valid;
  }
  // Formularios invalidos
  get userNameInvalid() {
    return this.form.get('user_name').invalid && this.form.get('user_name').touched;
  }

  get securityCodeInvalid() {
    return this.form.get('security_code').invalid && this.form.get('security_code').touched;
  }

  onClick(control_name: string) {
    this.form.controls[`${control_name}`].reset();
  }

  handelSave() {
    const user_name = this.form.controls['user_name'].value;
    const security_code = this.form.controls['security_code'].value.toString();
    this.spinner = true;
    this.form.disable();

    this._forgotPasswordService.getForgotPassword(user_name, security_code).subscribe((data) => {
      console.log({dataaa: data});
      if (data && data.status === '200') {
        setTimeout(() => {
          Swal.fire({
            icon: 'info',
            title: 'Correcto',
            text: 'Recuperación exitosa. La contraseña fue enviada a su correo.',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar'
          }).then((result) => {
            if (result.isConfirmed) {
              this.spinner = false;
              this.form.enable();
              this._router.navigateByUrl('/login');
            }
          });
        });
      } else {
        this._alert.error({
          title: 'Error',
          text: 'Ha ocurrido un error al intentar recuperar la contarseña. Contacte al administador del sistema.'
        });
      }
        this.spinner = false;
        this.form.enable();
    }, (err) => {
      console.log({ERRORRR: err}) ;
      this.spinner = false;
      this.form.enable();
      this._alert.error({
        title: 'Error',
        text: 'Ha ocurrido un error, por favor vuelva a intentarlo más tarde.'
      });
    });
  }



}
