import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})

export class ForgotPasswordService {
  private BaseUrl = environment.coreApiUrl;
  username: any = 'admin';
  password: any = 'admin';
  authorization = 'Basic ' + btoa(this.username + ':' + this.password);

  headers(enterprise_code: string) {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization', this.authorization);
    if (enterprise_code) {
      header = header.set('xTenantId', enterprise_code);
    }
    return header;
  }

  constructor(
    private http: HttpClient) {
  }

  getForgotPassword (user: string, security_code: string ): Observable<any> {
    const body = {'username': user, 'recoveryCode': security_code, 'status': ''};
    return this.http.post(this.BaseUrl + 'tenant/security/forgot_password', body, { headers: this.headers(null)});
  }
}
