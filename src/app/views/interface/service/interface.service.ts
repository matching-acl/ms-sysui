import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment.prod';
import {Interfaces} from '../model/interface.model';

@Injectable({
  providedIn: 'root'
})

export class InterfaceService {

  constructor(private http: HttpClient) {}

  getConnectionOrigin(enterprise_code: string): Observable<any> {

    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');


    let params = new HttpParams();
    params = params.set('is_enum', '0');
    params = params.set('model', 'tc_origin');
    params = params.set('schema', enterprise_code);

    const pr = {'is_enum': '1', 'model': 'tc_origin', 'schema': enterprise_code};

    return this.http.post(environment.pythonApiUrl + 'read', pr, {headers: header});

  }
  getTotalItems(enterprise_code: string): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
    const pr = {'model': 'interfaces', 'schema': enterprise_code};

    return this.http.post(environment.pythonApiUrl + 'total_items', pr, {headers: header});

  }



  getInterfaces(enterprise_code: string,  number_page? : number, size_page? : number): Observable<Interfaces[]> {

    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
    let params = new HttpParams();
    params = params.set('is_enum', '0');
    params = params.set('model', 'interfaces');
    params = params.set('schema', enterprise_code);
    
    const pr = {
      'number_page': number_page ? number_page + 1 : 1,
      'size_page' : size_page ? size_page : 10,
      'is_enum': '0',
      'model': 'interfaces',
      'schema': enterprise_code
    };

    return this.http.post<Interfaces[]>(environment.pythonApiUrl + 'read', pr, {headers: header});

  }

  updateActiveInterfaces(id: number, active: boolean, enterprise_code: string): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');

    const data = {
      'model': 'interfaces',
      'schema': enterprise_code,
      'id_name': 'id',
      'object_new': {
        'id' : id,
        'active' : active
      }
    };

    return this.http.post(environment.pythonApiUrl + 'update', data, {headers: header});
  }

  getTemplates(enterprise_code: string): Observable<any> {

    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
    let params = new HttpParams();
    params = params.set('is_enum', '0');
    params = params.set('model', 'tc_origin');
    params = params.set('schema', 'acl_koncilia');

    const pr = {'is_enum': '0', 'model': 'templates', 'schema': enterprise_code};

    return this.http.post(environment.pythonApiUrl + 'read', pr, {headers: header});

  }
  getTemplatesReady(enterprise_code: string): Observable<any> {

    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
    let params = new HttpParams();
    params = params.set('is_enum', '0');
    params = params.set('model', 'tc_origin');
    params = params.set('schema', 'acl_koncilia');

    const filters = {
      'e_state': 'Listo',
     'active' : true
      
    }

    const pr = {
      'is_enum': '0',
      'model': 'templates',
      'schema': enterprise_code,
      'filters': filters
      
    };

    return this.http.post(environment.pythonApiUrl + 'read', pr, {headers: header});

  }

  getTemplateById(id, enterprise_code: string): Observable<any> {
    let header = new HttpHeaders();
    const filter = enterprise_code + '.templates.id';
    const obj: any = {[filter]: id};

    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
    const pr = {'is_enum': '0', 'model': 'templates', 'schema': enterprise_code, 'filters' : obj};

    return this.http.post(environment.pythonApiUrl + 'read', pr, {headers: header});
  }

  testFile(fileToUpload: File, formato, separator, enterprise_code: string): Observable<any> {
    console.log(formato + '-' + separator);
    let header = new HttpHeaders();
    header = header.set('responseType', 'text/html; charset=utf-8');
    const formData: FormData = new FormData();
    let format = JSON.stringify(formato);
    format = format.replaceAll('true', '"true"');
    formData.append('separator', separator);
    formData.append('header', format);
    formData.append('schema', enterprise_code);
    formData.append('model', 'interface_format');
    formData.append('file', fileToUpload);
    return this.http.post<any>(environment.goApiUrl + 'validate_file', formData);
  }


  createInterface(data): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
    return this.http.post(environment.pythonApiUrl + 'create', data, {headers: header});
  }

  getInterfaceById(id: number, enterprise_code: string): Observable<any> {

    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');

    const pr = {'schema' : enterprise_code, 'model' : 'interfaces', 'id_name' : 'id', 'id' : id};

    return this.http.post(environment.pythonApiUrl + 'get', pr, {headers: header});
  }


  updateInterface(data): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');

    return this.http.post(environment.pythonApiUrl + 'update', data, {headers: header});
  }

  fileTestVaild(fileToUpload: File, formato, separator, isTitled, enterprise_code: string) {
    console.log(fileToUpload);
    console.log(formato);
    console.log(separator);
    console.log(isTitled);
    let header = new HttpHeaders();
    header = header.set('responseType', 'text/html; charset=utf-8');
    const formData: FormData = new FormData();

    let format = JSON.stringify(formato);
    format = format.replaceAll('true', '"true"');

    formData.append('separator', separator);
    formData.append('header', format);
    formData.append('schema', enterprise_code);
    formData.append('model', 'interfaces');
    formData.append('is_titled', isTitled);
    formData.append('file', fileToUpload);
    return this.http.post<any>(environment.goApiUrl + 'validate_file', formData);

  }

  testConnection(data): Observable<any> {
    return this.http.post(environment.pythonApiUrl + 'sftp_test_connection', data);
  }


}
