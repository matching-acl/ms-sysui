import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {InterfaceComponent} from './interface.component';
import {AddInterfaceComponent} from './components/add-interface/add-interface.component';
import {EditInterfaceComponent} from './components/edit-interface/edit-interface.component';
import {AuthGuardService} from '../login/service/auth-guard.service';

const routes: Routes = [
  { path: '', component: InterfaceComponent, canActivate: [AuthGuardService]},
  { path: 'create', component: AddInterfaceComponent, canActivate: [AuthGuardService]},
  { path: 'edit/:id', component: EditInterfaceComponent, canActivate: [AuthGuardService]}
];

@NgModule ({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InterfaceRoutingModule {}
