import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { InterfaceComponent } from './interface.component';
import {InterfaceRoutingModule} from './interface.routing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {AppBreadcrumbModule} from '@coreui/angular';
import {ModalModule} from 'ngx-bootstrap/modal';
import { AddInterfaceComponent } from './components/add-interface/add-interface.component';
import { EditInterfaceComponent } from './components/edit-interface/edit-interface.component';
import {QuartzCronModule} from '@sbzen/ng-cron';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PipesModule } from '../../pipes/pipes.module';
import { DirectivesModule } from '../../directives/directives.module';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';
import { CustomMatPaginatorIntl } from '../paginator-es';




@NgModule({
  declarations: [InterfaceComponent, AddInterfaceComponent, EditInterfaceComponent],
  imports: [
    MatPaginatorModule,
    PipesModule,
    DirectivesModule,
    TooltipModule,
    PopoverModule,
    CommonModule,
    InterfaceRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    AppBreadcrumbModule,
    QuartzCronModule,
    ModalModule.forRoot()
  ],
   providers: [
    {
      provide: MatPaginatorIntl,
      useClass: CustomMatPaginatorIntl,
    },
    DatePipe,
  ]
})
export class InterfaceModule { }
