import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ConnectionOrigen, FormatTemplate} from '../../model/interface.model';
import {InterfaceService} from '../../service/interface.service';
import {CronOptions} from 'ngx-cron-editor';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import {ENTERPRISE_CODE, USER_ID} from '../../../login/models/login';
import { IsLoadingService } from '@service-work/is-loading';


@Component({
  selector: 'app-add-interface',
  templateUrl: './add-interface.component.html',
  styleUrls: ['./add-interface.component.scss'],
  styles: [`  :host >>> .tooltip-inner {
        background-color: #153f59;
        color: #fff;
      }
      :host >>> .tooltip.top .tooltip-arrow:before,
      :host >>> .tooltip.top .tooltip-arrow {
        border-top-color: #153f59;
      }`]
})
export class AddInterfaceComponent implements OnInit {
  @ViewChild('file') fileInput: ElementRef;
  active = true;
  add_interface_form: FormGroup;
  format: FormatTemplate[];
  fileToUpload: File = null;
  columsHeader: any;
  templateSource: any;
  templateSeparator: any;
  templateId: any;
  selectTemplate = false;
  conection_origen: ConnectionOrigen[];
  templates: FormatTemplate[];
  cronValue = '2,0,4,3,1 0/1 3/2 ? * 4/5 *';
  message  = `<b><i>00FECHA01012021REV03</i></b></br>01105100003455212341<br>01108500007894432513`;
  isTitled: any;
  responseServer: boolean;
  responseMessage: string = '';
  urlSelected: boolean;
  sftpSelected: boolean;
  connSelect: string = 'SFTP';
  connTypeSelected: any;
  isValidFile: boolean;
  validLines: any;
  isInvalidFile: boolean;
  invalidLines: any;
  ipUrl: any;
  entryFolder: any;
  status: boolean = false;
  fileKies: any[] = [];
  erroArray: any[] = [];
  hasKies: any;
  datas: any;
  enterprise_code: string;
  user_id: any;
  processed = false;

  @ViewChild('crontab') myId: ElementRef;

  cronOptions: CronOptions = {

    defaultTime: '00:00:00',

    hideMinutesTab: true,
    hideHourlyTab: true,
    hideDailyTab: true,
    hideWeeklyTab: false,
    hideMonthlyTab: false,
    hideYearlyTab: false,
    hideAdvancedTab: true,
    hideSpecificWeekDayTab : false,
    hideSpecificMonthWeekTab : false,

    use24HourTime: true,
    hideSeconds: false,

    cronFlavor: 'quartz', // standard or quartz
    formInputClass: '',
    formSelectClass: '',
    formRadioClass: '',
    formCheckboxClass: ''
  };
  constructor(
    private fb: FormBuilder,
    private interfaceService: InterfaceService,
    private router: Router,
  private loading : IsLoadingService) { }

  ngOnInit(): void {
    this.enterprise_code = localStorage.getItem(ENTERPRISE_CODE);
    this.user_id = localStorage.getItem(USER_ID);
    this.setData();
    this.createForm();
    this.getTemplates();
    this.isTitled = false;
    this.responseServer = false;
    this.urlSelected = false;
    this.sftpSelected = true;
    this.connTypeSelected = '2';
    this.isValidFile = false;
    this.isInvalidFile = false;
  }
  getTemplates() {
    this.interfaceService.getTemplates(this.enterprise_code).subscribe((datas) => {
      console.log(datas);
      this.templates = datas;
    }, error => {console.log({data: error});
    });
  }

  setData() {
    this.interfaceService.getConnectionOrigin(this.enterprise_code).subscribe((data) => {
      console.log({dataa: data});
      this.conection_origen = data;
    }, error => {console.log({data: error});
    });
  }

  createForm() {
    this.add_interface_form = this.fb.group({
      cronForm: [this.cronValue, []],
      load_name: [null, [Validators.required, Validators.minLength(3)]],
      format_template: [null, [Validators.required]],
      file_name: ['', []],
      connection_url: [null],
      connection_ip: [null, [Validators.required]],
      connection_folder: [null, [Validators.required]],
      connection_port: [22, [Validators.required]],
      connection_type: [null, [Validators.required]],
      connection_user_name: [null, [Validators.required,  Validators.minLength(3)]],
      connection_password: [null, [Validators.required]]
    });
  }

  changeStatus(event) {
    if ( event.target.checked ) {
      this.isTitled = true;
    } else {
      this.isTitled = false;
    }
  }




  // Formularios invalidos
  get loadNameNotValid() {
    return this.add_interface_form.get('load_name').invalid && this.add_interface_form.get('load_name').touched;
  }
  get connectionUrlNotValid() {
    return this.add_interface_form.get('connection_url').invalid && this.add_interface_form.get('connection_url').touched;
  }
  get connectionPortNotValid() {
    return this.add_interface_form.get('connection_port').invalid && this.add_interface_form.get('connection_port').touched;
  }
  get connectionUserNameNotValid() {
    return this.add_interface_form.get('connection_user_name').invalid && this.add_interface_form.get('connection_user_name').touched;
  }
  get connectionPasswordNotValid() {
    return this.add_interface_form.get('connection_password').invalid && this.add_interface_form.get('connection_password').touched;
  }
  get connectionIpNotValid() {
    return this.add_interface_form.get('connection_ip').invalid && this.add_interface_form.get('connection_ip').touched;
  }
  get connectionFolderNotValid() {
    return this.add_interface_form.get('connection_folder').invalid && this.add_interface_form.get('connection_folder').touched;
  }
  get hourNotValid() {
    return this.add_interface_form.get('cron_hour').invalid && this.add_interface_form.get('cron_hour').touched;
  }
  get minNotValid() {
    return this.add_interface_form.get('cron_min').invalid && this.add_interface_form.get('cron_min').touched;
  }
  get daysMonthNotValid() {
    return this.add_interface_form.get('cron_days_month').invalid && this.add_interface_form.get('cron_days_month').touched;
  }
  get daysWeekNotValid() {
    return this.add_interface_form.get('cron_days_week').invalid && this.add_interface_form.get('cron_days_week').touched;
  }
  get monthNotValid() {
    return this.add_interface_form.get('cron_month').invalid && this.add_interface_form.get('cron_month').touched;
  }

  get disableTestConnection() {

    if (this.connTypeSelected === '1') {
      return this.add_interface_form.get('connection_url').invalid ||
      this.add_interface_form.get('connection_user_name').invalid ||
      this.add_interface_form.get('connection_password').invalid ||
      this.add_interface_form.get('connection_port').invalid ||
      this.add_interface_form.get('connection_type').invalid;
    } else {
      return this.add_interface_form.get('connection_ip').invalid ||
      this.add_interface_form.get('connection_user_name').invalid ||
      this.add_interface_form.get('connection_password').invalid ||
      this.add_interface_form.get('connection_port').invalid ||
      this.add_interface_form.get('connection_type').invalid ||
      this.add_interface_form.get('connection_folder').invalid;
    }

  }

  // formularios validos
  get loadNameValid() {
    return this.add_interface_form.get('load_name').valid && this.add_interface_form.get('load_name').touched;
  }
  get connectionUrlValid() {
    return this.add_interface_form.get('connection_url').valid && this.add_interface_form.get('connection_url').touched;
  }
  get connectionPortValid() {
    return this.add_interface_form.get('connection_port').valid && this.add_interface_form.get('connection_port').touched;
  }
  get connectionUserNameValid() {
    return this.add_interface_form.get('connection_user_name').valid && this.add_interface_form.get('connection_user_name').touched;
  }
  get connectionPasswordValid() {
    return this.add_interface_form.get('connection_password').valid && this.add_interface_form.get('connection_password').touched;
  }
  get connectionIpValid() {
    return this.add_interface_form.get('connection_ip').valid && this.add_interface_form.get('connection_ip').touched;
  }
  get connectionFolderValid() {
    return this.add_interface_form.get('connection_folder').valid && this.add_interface_form.get('connection_folder').touched;
  }
  get hourValid() {
    return this.add_interface_form.get('cron_hour').valid && this.add_interface_form.get('cron_hour').touched;
  }
  get minValid() {
    return this.add_interface_form.get('cron_min').valid && this.add_interface_form.get('cron_min').touched;
  }
  get daysMonthValid() {
    return this.add_interface_form.get('cron_days_month').valid && this.add_interface_form.get('cron_days_month').touched;
  }
  get daysWeekValid() {
    return this.add_interface_form.get('cron_days_week').valid && this.add_interface_form.get('cron_days_week').touched;
  }
  get monthValid() {
    return this.add_interface_form.get('cron_month').valid && this.add_interface_form.get('cron_month').touched;
  }
  onOptionsSelected(value: string) {
    this.columsHeader = [];
    this.hasKies = false;
    this.selectTemplate = true;
    console.log (value);

    // tslint:disable-next-line:radix
    const idTemplate: number = parseInt(value);

    this.interfaceService.getTemplateById(idTemplate, this.enterprise_code).subscribe((data) => {
      this.columsHeader = data[0].header;
      this.datas = data[0].header;
      this.templateSeparator = data[0].separator;
      this.templateId = data[0].id;
      console.log(data[0]);
    });
  }

  onConnectionTypeSelected(value: string) {
    console.log(value);

    if (value === 'URL') {
      this.connTypeSelected = '1';
      this.urlSelected = true;
      this.sftpSelected = false;
      this.add_interface_form.controls['connection_url'].setValidators([Validators.required]);
      this.add_interface_form.controls['connection_url'].updateValueAndValidity();
      this.add_interface_form.controls['connection_ip'].clearValidators();
      this.add_interface_form.controls['connection_ip'].setValue('');
      this.add_interface_form.controls['connection_ip'].updateValueAndValidity();
      this.add_interface_form.controls['connection_folder'].clearValidators();
      this.add_interface_form.controls['connection_folder'].setValue('');
      this.add_interface_form.controls['connection_folder'].updateValueAndValidity();
    } else {
      this.connTypeSelected = '2';
      this.urlSelected = false;
      this.sftpSelected = true;
      this.add_interface_form.controls['connection_url'].clearValidators();
      this.add_interface_form.controls['connection_url'].setValue('');
      this.add_interface_form.controls['connection_url'].updateValueAndValidity();
      this.add_interface_form.controls['connection_ip'].setValidators([Validators.required]);
      this.add_interface_form.controls['connection_ip'].updateValueAndValidity();
      this.add_interface_form.controls['connection_folder'].setValidators([Validators.required]);
      this.add_interface_form.controls['connection_folder'].updateValueAndValidity();
    }
  }

  hasName(o) {
    return o != null;
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.add_interface_form.controls['file_name'].setValue(this.fileToUpload.name);
  }

  clearFile() {
    this.fileInput.nativeElement.value = null;
    this.add_interface_form.controls['file_name'].setValue(null);
  }

  validarFormato() {

    let rows = this.datas;
    let headers = [];
    let arr = [];
    for (let row of rows) {
      let obj = {
        position: row.position,
        name: row.name,
        code: row.code,
        length: parseInt(row.length)
      };
      arr.push(obj);
    }

    if (this.fileToUpload == null) {
      this.responseMessage = '<i>Debes adjuntar un archivo para validar</i>';
    } else if (!this.selectTemplate) {
      this.responseMessage = '<i>Debes seleccionar un formato de interfaz</i>';
    } else {
      this.interfaceService.fileTestVaild(this.fileToUpload, arr, this.templateSeparator, this.isTitled, this.enterprise_code).
      subscribe(data => {
        console.log(data);
          for (const i of data.Data.Data) {
            console.log(i.processed);
            if (i.processed === 1) {
              console.log('color negro');
              this.processed = true;
            } else {
              console.log('color rojo');
              this.processed = false;
            }
          }
          if(data.Data.StatusCode == '200') {
            this.responseServer = true;
            this.responseMessage = '<i>El archivo de prueba cumple con el formato de la interfaz</i>';
          } else {
            this.responseServer = true;
            this.responseMessage = '<i>El archivo contiene registros que <b>no cumplen con el formato definido</b>. Puede continuar con el proceso, pero es posible que la carga no se ejecute correctamente. Intente probar otro formato o utilizar otro archivo.</i>';
          }

          data.Data.Header.forEach((element) => {
            headers.push(element);
          });
          this.columsHeader = [];
          this.columsHeader = headers;
          //console.log(headers);

          this.hasKies = true;
          this.erroArray = [];
          this.fileKies = [];
          for (let ob of data.Data.Data) {
            //console.log(ob)
            let ar: any[] = [];
            headers.forEach((v, i) => {
              //console.log(v.name)
              Object.entries(ob.keys).forEach(
                ([key, value]) => {
                  //console.log(key)
                  if (v.code == key.toUpperCase()) {
                    //console.log(i + '-' + v + '-' + value);
                    ar.push(value);
                  }
                }
              );
            })
          this.erroArray.push(ob.linea);
          this.fileKies.push(ar);
          //console.log(this.fileKies);
        }
      },
      error => {
        this.fileToUpload = null;
        this.clearFile();
        this.responseServer = true;
        this.responseMessage = '<i>Ocurrió un error desconocido, por favor intente nuevamente más tarde</i>';
      });
    }
  }

  SaveInterface() {

    if (this.connTypeSelected === '1') {
      this.ipUrl = this.add_interface_form.controls['connection_url'].value;
      this.entryFolder = '';
    } else {
      this.ipUrl = this.add_interface_form.controls['connection_ip'].value;
      this.entryFolder = this.add_interface_form.controls['connection_folder'].value;
    }
    console.log(this.connTypeSelected);
    console.log(this.entryFolder);

    const saveData = {
      'model': 'interfaces',
      'schema': this.enterprise_code,
      'id_name': 'id',
      'object': {
          'e_origin' : this.add_interface_form.controls['connection_type'].value,
          'user_id' : this.user_id,
          'url' : this.ipUrl,
          'username' : this.add_interface_form.controls['connection_user_name'].value,
          'password' : this.add_interface_form.controls['connection_password'].value,
          'port' : this.add_interface_form.controls['connection_port'].value,
          'entry_dir' : this.entryFolder,
          'name' : this.add_interface_form.controls['load_name'].value,
          'template_id' : this.templateId,
          'cron_time' :  this.cronValue,
          'is_titled' : this.isTitled
      }
    };

    console.log(saveData);

    this.interfaceService.createInterface(saveData).subscribe((data) => {
      if (data.status === '200') {
        Swal.fire(
          'Correcto!',
          'Se ha registrado la interfaz correctamente',
          'success'
        );
        setTimeout(() => {
          Swal.close();
          this.router.navigateByUrl('/interface');
        }, 2000);
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'No se ha podido registrar la interfaz: ' + saveData.object.name
        });
      }
    }, (err) => {
      console.log(err);
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'No se ha podido registrar la interfaz'
      });
    });
  }

  handelTestConnection() {
    let urlTest;

    if (this.connTypeSelected === '1') {
      urlTest = this.add_interface_form.controls['connection_url'].value;

    } else {
      urlTest = this.add_interface_form.controls['connection_ip'].value;
    }

    const testData = {
      'username': this.add_interface_form.controls['connection_user_name'].value,
      'password': this.add_interface_form.controls['connection_password'].value,
      'url': urlTest,
      'port': this.add_interface_form.controls['connection_port'].value,
      'e_origin' : this.add_interface_form.controls['connection_type'].value
    };

    this.loading.add(

      this.interfaceService.testConnection(testData).subscribe((data) => {
        if (data.message === 'true') {
          this.active = false;
          Swal.fire(
            'Correcto!',
            'Prueba de conexión correcta',
            'success'
          );
        } else {
          this.active = true;
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'No se ha podido realizar la conexión '
          });
        }
      }, (err) => {
        this.active = true;
        console.log(err);
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'No se ha podido realizar la conexión'
        });
      })
    )
  }

  private capitalizeString(cadena: string) {
    if (cadena && cadena.length > 0) {
      return cadena.charAt(0).toUpperCase() + cadena.slice(1);
    } return cadena;
  }
}
