import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {ConnectionOrigen, FormatTemplate} from '../../model/interface.model';
import {InterfaceService} from '../../service/interface.service';
import Swal from 'sweetalert2';
import {ENTERPRISE_CODE, USER_ID} from '../../../login/models/login';
import { IsLoadingService } from '@service-work/is-loading';

@Component({
  selector: 'app-edit-interface',
  templateUrl: './edit-interface.component.html',
  styleUrls: ['./edit-interface.component.scss'],
  styles: [`  :host >>> .tooltip-inner {
    background-color: #153f59;
    color: #fff;
  }
  :host >>> .tooltip.top .tooltip-arrow:before,
  :host >>> .tooltip.top .tooltip-arrow {
    border-top-color: #153f59;
  }`]
})
export class EditInterfaceComponent implements OnInit {
  @ViewChild('file') fileInput: ElementRef;
  active = true;
  edit_interface_form: FormGroup;
  format: FormatTemplate[];
  fileToUpload: File = null;
  columsHeader: any;
  columnTestHeader: any;
  templateSource: any;
  selectTemplate = false;
  conection_origen: ConnectionOrigen[];
  cronValue = '';
  currentFormat: number;
  templateId: any;
  interfaceId: any;
  sub: any;
  id_url: number;
  templateSeparator: any;
  templates: FormatTemplate[];
  message  = `<b><i>00FECHA01012021REV03</i></b></br>01105100003455212341<br>01108500007894432513`;
  isTitled: any;
  isValidFile: boolean;
  validLines: any;
  isInvalidFile: boolean;
  invalidLines: any;
  responseServer: boolean;
  responseMessage: string = '';
  urlSelected: boolean;
  sftpSelected: boolean;
  connSelect: string = 'SFTP';
  connTypeSelected: any;
  datas: any;
  ipForm: any;
  urlForm: any;
  folderForm: any;
  ipUrl: any;
  entryFolder: any;
  fileKies: any[] = [];
  erroArray: any[] = [];
  hasKies: any;
  status: boolean = false;
  enterprise_code; string;
  user_id: any;
  processed = false;

  constructor(
    private fb: FormBuilder,
    private interfaceService: InterfaceService,
    private route: ActivatedRoute,
    private router: Router,
   private loading : IsLoadingService) { }

  ngOnInit(): void {
    this.enterprise_code = localStorage.getItem(ENTERPRISE_CODE);
    this.user_id = localStorage.getItem(USER_ID);
    this.sub = this.route.params.subscribe(params => {
      this.id_url = +params['id'];
    });

    this.createForm();
    this.getTemplates();
    this.getInterfaceById(this.id_url);
    this.setData();
    this.responseServer = false;
    this.urlSelected = false;
    this.sftpSelected = true;
    this.connTypeSelected = '2';
    this.isValidFile = false;
    this.isInvalidFile = false;
    this.isTitled = false;
    this.hasKies = false;
    console.log(this.isValidFile + '.....' + this.isInvalidFile);
  }
  setData() {
    this.interfaceService.getConnectionOrigin(this.enterprise_code).subscribe((data) => {

      this.conection_origen = data;
    }, error => {console.log({data: error});
    });
  }

  createForm() {
    this.edit_interface_form = this.fb.group({
      cronForm: [],
      load_name: [],
      format_template: [],
      file_name: [],
      connection_url: [],
      connection_ip: [],
      connection_folder: [],
      connection_port: [],
      connection_type: [],
      connection_user_name: [],
      connection_password: []
    });
  }

  getTemplates() {
    this.interfaceService.getTemplatesReady(this.enterprise_code).subscribe((datas) => {
      this.templates = datas;
    }, error => {console.log({data: error});
    });
  }

  getInfoTemplate(id) {
    this.selectTemplate = true;
    this.interfaceService.getTemplateById(id, this.enterprise_code).subscribe((data) => {
      this.datas = data[0].header;
      this.columsHeader = data[0].header;
      this.templateSeparator = data[0].separator;
      this.templateId = data[0].id;
    });
  }

  changeStatus(event) {
    if ( event.target.checked ) {
      this.isTitled = true;
    } else {
      this.isTitled = false;
    }
  }

  getInterfaceById(id: number) {
    this.interfaceService.getInterfaceById(id, this.enterprise_code).subscribe((data) => {
      console.log(data);
      if (data.e_origin === 'URL') {
        this.urlForm = data.e_url;
        this.ipForm = ' ';
        this.folderForm = ' ';
        this.connTypeSelected = '1';

        this.edit_interface_form = this.fb.group({
          cronForm: [data.cron_time, []],
          load_name: [data.name, [Validators.required, Validators.minLength(3)]],
          format_template: [null, []],
          file_name: ['', []],
          connection_url: [this.urlForm, [Validators.required]],
          connection_port: [data.e_port, [Validators.required]],
          connection_ip: [this.ipForm],
          connection_folder: [this.folderForm],
          connection_type: [data.e_origin, [Validators.required]],
          connection_user_name: [data.e_username, [Validators.required,  Validators.minLength(3)]],
          connection_password: [data.e_password, [Validators.required]]
        });

      } else {
        this.ipForm = data.e_url;
        this.folderForm = data.e_entry_dir;
        this.urlForm = '';
        this.connTypeSelected = '2';

        this.edit_interface_form = this.fb.group({
          cronForm: [data.cron_time, []],
          load_name: [data.name, [Validators.required, Validators.minLength(3)]],
          format_template: [null, [Validators.required]],
          file_name: ['', []],
          connection_url: [],
          connection_port: [data.e_port, [Validators.required]],
          connection_ip: [this.ipForm, [Validators.required]],
          connection_folder: [this.folderForm, [Validators.required]],
          connection_type: [data.e_origin, [Validators.required]],
          connection_user_name: [data.e_username, [Validators.required,  Validators.minLength(3)]],
          connection_password: [data.e_password, [Validators.required]]
        });
      }

      this.columsHeader = data.template_header;
      this.cronValue = data.cron_time;
      this.currentFormat = data.template_id;
      this.interfaceId = data.id;
      this.getInfoTemplate(this.currentFormat);
      this.connSelect = data.e_origin;
      this.onConnectionTypeSelected(this.connSelect);
    });
  }


  // Formularios invalidos
  get loadNameNotValid() {
    return this.edit_interface_form.get('load_name').invalid && this.edit_interface_form.get('load_name').touched;
  }
  get connectionUrlNotValid() {
    return this.edit_interface_form.get('connection_url').invalid && this.edit_interface_form.get('connection_url').touched;
  }
  get connectionUserNameNotValid() {
    return this.edit_interface_form.get('connection_user_name').invalid && this.edit_interface_form.get('connection_user_name').touched;
  }
  get connectionPasswordNotValid() {
    return this.edit_interface_form.get('connection_password').invalid && this.edit_interface_form.get('connection_password').touched;
  }
  get connectionIpNotValid() {
    return this.edit_interface_form.get('connection_ip').invalid && this.edit_interface_form.get('connection_ip').touched;
  }
  get connectionFolderNotValid() {
    return this.edit_interface_form.get('connection_folder').invalid && this.edit_interface_form.get('connection_folder').touched;
  }

  get disableTestConnection() {
    if (this.connTypeSelected === '1') {
      return this.edit_interface_form.get('connection_url').invalid ||
      this.edit_interface_form.get('connection_user_name').invalid ||
      this.edit_interface_form.get('connection_password').invalid ||
      this.edit_interface_form.get('connection_port').invalid ||
      this.edit_interface_form.get('connection_type').invalid;
    } else {
      return this.edit_interface_form.get('connection_ip').invalid ||
      this.edit_interface_form.get('connection_user_name').invalid ||
      this.edit_interface_form.get('connection_password').invalid ||
      this.edit_interface_form.get('connection_port').invalid ||
      this.edit_interface_form.get('connection_type').invalid ||
      this.edit_interface_form.get('connection_folder').invalid;
    }
  }

  // formularios validos
  get loadNameValid() {
    return this.edit_interface_form.get('load_name').valid && this.edit_interface_form.get('load_name').touched;
  }
  get connectionUrlValid() {
    return this.edit_interface_form.get('connection_url').valid && this.edit_interface_form.get('connection_url').touched;
  }
  get connectionUserNameValid() {
    return this.edit_interface_form.get('connection_user_name').valid && this.edit_interface_form.get('connection_user_name').touched;
  }
  get connectionPasswordValid() {
    return this.edit_interface_form.get('connection_password').valid && this.edit_interface_form.get('connection_password').touched;
  }
  get connectionIpValid() {
    return this.edit_interface_form.get('connection_ip').valid && this.edit_interface_form.get('connection_ip').touched;
  }
  get connectionFolderValid() {
    return this.edit_interface_form.get('connection_folder').valid && this.edit_interface_form.get('connection_folder').touched;
  }

  onOptionsSelected(value: string) {
    this.columnTestHeader = [];
    this.hasKies = false;
    this.selectTemplate = true;
    console.log (value);

    const idTemplate: number = parseInt(value);

    this.interfaceService.getTemplateById(idTemplate, this.enterprise_code).subscribe((data) => {
      this.columsHeader = data[0].header;
      this.datas = data[0].header;
      this.templateSeparator = data[0].separator;
      this.templateId = data[0].id;
      console.log(data[0]);
    });
  }

  onConnectionTypeSelected(value: string) {
    console.log(value);

    if (value === 'URL') {
      this.connTypeSelected = '1';
      this.urlSelected = true;
      this.sftpSelected = false;
      this.edit_interface_form.controls['connection_url'].setValidators([Validators.required]);
      this.edit_interface_form.controls['connection_url'].updateValueAndValidity();
      this.edit_interface_form.controls['connection_ip'].clearValidators();
      this.edit_interface_form.controls['connection_ip'].updateValueAndValidity();
      this.edit_interface_form.controls['connection_folder'].clearValidators();
      this.edit_interface_form.controls['connection_folder'].updateValueAndValidity();
    } else {
      this.connTypeSelected = '2';
      this.urlSelected = false;
      this.sftpSelected = true;
      this.edit_interface_form.controls['connection_url'].clearValidators();
      this.edit_interface_form.controls['connection_url'].updateValueAndValidity();
      this.edit_interface_form.controls['connection_ip'].setValidators([Validators.required]);
      this.edit_interface_form.controls['connection_ip'].updateValueAndValidity();
      this.edit_interface_form.controls['connection_folder'].setValidators([Validators.required]);
      this.edit_interface_form.controls['connection_folder'].updateValueAndValidity();
    }
  }

  hasName(o) {
    return o != null;
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.edit_interface_form.controls['file_name'].setValue(this.fileToUpload.name);
  }

  validarFormato() {

    let rows = this.datas;
    let headers = [];
    let arr = [];
    for (let row of rows) {
      let obj = {
        position: row.position,
        name: row.name,
        code: row.code,
        length: parseInt(row.length)
      };
      arr.push(obj);
    }

    if (this.fileToUpload == null) {
      this.responseMessage = '<i>Debes adjuntar un archivo para validar</i>';
    } else if (!this.selectTemplate) {
      this.responseMessage = '<i>Debes seleccionar un formato de interfaz</i>';
    } else {
      this.interfaceService.fileTestVaild(this.fileToUpload, arr, this.templateSeparator, this.isTitled, this.enterprise_code).
      subscribe(data => {
        console.log(data.Data);
        for (const i of data.Data.Data) {
            console.log(i.processed);
            if (i.processed === 1) {
              console.log('color negro');
              this.processed = true;
            } else {
              console.log('color rojo');
              this.processed = false;
            }
          }
        if (data.Data.StatusCode == '200') {
          this.responseServer = true;
          this.responseMessage = '<i>El archivo de prueba cumple con el formato de la interfaz</i>';
        } else {
          this.responseServer = true;
          this.responseMessage = '<i>El archivo contiene registros que <b>no cumplen con el formato definido</b>. Puede continuar con el proceso, pero es posible que la carga no se ejecute correctamente. Intente probar otro formato o utilizar otro archivo.</i>';
        }
        this.hasKies = true;
        data.Data.Header.forEach((element) => {
          headers.push(element);
        });
        this.columsHeader = [];
        this.columsHeader = headers;
        //console.log(headers);

        this.erroArray = [];
        this.fileKies = [];
        for (let ob of data.Data.Data) {
          console.log(ob)
          let ar: any[] = [];
          headers.forEach((v, i) => {
            //console.log(v, i)
            Object.entries(ob.keys).forEach(
              ([key, value]) => {
                if (v.code == key.toUpperCase()) {
                  //console.log(i + '-' + v.name + '-' + key + '-' + value);
                  ar.push(value);
                }
              });
          });
          this.erroArray.push(ob.linea);
          this.fileKies.push(ar);
          //console.log(this.erroArray);
        }
      },
      error => {
        this.fileToUpload = null;
        this.clearFile();
        this.responseServer = true;
        this.responseMessage = '<i>Ocurrió un error desconocido, por favor intente nuevamente más tarde</i>';
      });
    }
  }

  clearFile() {
    this.fileInput.nativeElement.value = null;
    this.edit_interface_form.controls['file_name'].setValue(null);
  }

  updateInterface() {

    if (this.connTypeSelected === '1') {
      this.ipUrl = this.edit_interface_form.controls['connection_url'].value;
      this.entryFolder = '';
    } else {
      this.ipUrl = this.edit_interface_form.controls['connection_ip'].value;
      this.entryFolder = this.edit_interface_form.controls['connection_folder'].value;
    }


    const saveData = {
      'model': 'interfaces',
      'schema': this.enterprise_code,
      'id_name': 'id',
      'object_new': {
          'id' : this.interfaceId,
          'e_origin' : this.edit_interface_form.controls['connection_type'].value,
          'user_id' : this.user_id,
          'url' : this.ipUrl,
          'username' : this.edit_interface_form.controls['connection_user_name'].value,
          'password' : this.edit_interface_form.controls['connection_password'].value,
          'port' : this.edit_interface_form.controls['connection_port'].value,
          'entry_dir' : this.entryFolder,
          'name' : this.edit_interface_form.controls['load_name'].value,
          'template_id' : this.templateId,
          'cron_time' :  this.cronValue,
          'is_titled' : this.isTitled
      }
    };

    this.interfaceService.updateInterface(saveData).subscribe((data) => {
      if (data.status === '200') {
        Swal.fire(
          'Correcto!',
          'Se ha actualizado la interfaz correctamente',
          'success'
        );
        setTimeout(() => {
          Swal.close();
          this.router.navigateByUrl('/interface');
        }, 2000);
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'No se ha podido actualizar la interfaz : ' + saveData.object_new.name
        });
      }
    }, (err) => {
      console.log(err);
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'No se ha podido actualizar la interfaz'
      });
    });

  }

  handelTestConnection() {

    let urlTest;

    if (this.connTypeSelected === '1') {
      urlTest = this.edit_interface_form.controls['connection_url'].value;

    } else {
      urlTest = this.edit_interface_form.controls['connection_ip'].value;
    }

    const testData = {
      'username': this.edit_interface_form.controls['connection_user_name'].value,
      'password': this.edit_interface_form.controls['connection_password'].value,
      'url': urlTest,
      'port': this.edit_interface_form.controls['connection_port'].value,
      'e_origin' : this.edit_interface_form.controls['connection_type'].value
    };

    this.loading.add(
      this.interfaceService.testConnection(testData).subscribe((data) => {
      
        if (data.message === 'true') {
          this.active = false;
          Swal.fire(
            'Correcto!',
            'Prueba de conexión correcta',
            'success'
          );
        } else {
          this.active = true;
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'No se ha podido realizar la conexión '
          });
        }
      }, (err) => {
        console.log(err);
        this.active = true;
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'No se ha podido realizar la conexión'
        });
      })

    )
  }

  private capitalizeString(cadena: string) {
    if (cadena && cadena.length > 0) {
      return cadena.charAt(0).toUpperCase() + cadena.slice(1);
    } return cadena;
  }



}
