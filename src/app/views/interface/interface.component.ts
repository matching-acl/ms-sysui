import { Component, OnInit } from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {Router} from '@angular/router';
import {Interfaces} from './model/interface.model';
import {HttpErrorResponse} from '@angular/common/http';
import {IsLoadingService} from '@service-work/is-loading';
import {AlertService} from '../../service/alert.service';
import {InterfaceService} from './service/interface.service';
import Swal from 'sweetalert2';
import {ENTERPRISE_CODE} from '../login/models/login';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'interface',
  templateUrl: './interface.component.html',
  styleUrls: ['./interface.component.scss']
})
export class InterfaceComponent implements OnInit {

  totalItems : number;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [1, 5, 10, 25];
  showFirstLastButtons = true;

  title = 'Carga de Interfaces';
  dataSource: Interfaces[];
  noData = false;
  noDataMessage = 'No se encontraron registros';
  enterprise_code: string;
  modalRef: BsModalRef;
  constructor(
    private _router: Router,
    private loadService: IsLoadingService,
    private alert: AlertService,
    private interfaceService: InterfaceService) { }

  ngOnInit(): void {
    this.enterprise_code = localStorage.getItem(ENTERPRISE_CODE);
    this.getInterfaces();
  }

  handlePageEvent(event: PageEvent) {
    this.totalItems = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.getInterfaces(this.pageIndex, this.pageSize);
    
  }

  getInterfaces(pageIndex? : number, pageSize? : number) {


    this.interfaceService.getTotalItems(this.enterprise_code).subscribe(data => {
      this.totalItems = data.total_items;
      console.log(this.totalItems);
    });

    this.loadService.add(
      this.interfaceService.getInterfaces(this.enterprise_code, pageIndex, pageSize).subscribe(data => {
        console.log({data: data});
        if (data && data.length > 0) {
          this.dataSource = data;
        } else {
            this.noData = true;
          }
        },
        (error: HttpErrorResponse) => {
          this.noData = true;
          this.noDataMessage = 'Error de servidor, favor inténtelo nuevamente.';
        })

    );
  }

  openAddInterface() {
    this._router.navigate(['/interface/create']);
  }

  openEditInterface(id: number) {
    this._router.navigate(['/interface/edit/' + id]);
  }

  openUpdateActiveInterfaces(id: number, active: boolean) {
    this.loadService.add(
      this.interfaceService.updateActiveInterfaces(id, active, this.enterprise_code).subscribe((data) => {
        console.log({data: data});
        if (data && data.status === '200') {
          setTimeout(() => {
            Swal.fire({
              icon: 'success',
              title: 'Correcto',
              text: 'Se ha actualizado el estado de la interface correctamente',
              timer: 3000,
              showCancelButton: false,
              showConfirmButton: false
            });
            this.getInterfaces();
          }, 1500);
        } else {
          setTimeout(() => {
            Swal.fire({
              icon: 'error',
              title: 'Error',
              text: 'No se ha podido actualizar el estado de la interface: ' + data.message,
              timer: 3000,
              showCancelButton: false,
              showConfirmButton: false
            });
          }, 1500);
        }
      }, ( err ) => {
        setTimeout(() => {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'No se ha podido actualizar el estado de la interface',
            timer: 3000,
            showCancelButton: false,
            showConfirmButton: false
          });
        }, 1500);
      }));
  }


}
