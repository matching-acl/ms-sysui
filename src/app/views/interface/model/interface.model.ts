export class Interfaces {
  id?: number;
  name?: string;
  template_id?: string;
  template_name?: string;
  e_origin?: string;
  e_url?: string;
  username?: string;
  password?: string;
  cron_time?: string;
  cron_time_text?: string;
  created?: string;
  updated?: string;
  active?: boolean;
  user_id?: number;
  first_name?: string;
  last_name?: string;
  template_header?: Header[];
}

export class FormatTemplate {
  id: number;
  name?: string;
  tc_int_form_id?: number;
  separator?: string;
  separator_line?: string;
  created?: string;
  active?: boolean;
  e_state?: string;
  header?: Header[];
}

export class Header {
  name?: any;
  length?: string;
  isEditable?: boolean;
}

export class ConnectionOrigen {
  tc_origin: String;
}
