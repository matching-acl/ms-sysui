import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { AlertService } from '../../service/alert.service';
import { OutService } from './service/out.service';
import { IsLoadingService } from '@service-work/is-loading';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-out',
  templateUrl: './out.component.html',
  styleUrls: ['./out.component.scss']
})
export class OutComponent implements OnInit {

  totalItems : number;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [5, 10, 25];
  showFirstLastButtons = true;
  enterprise_code: string;
  user_id: string;
  dataSource = [];
  noDataMessage = 'No se encontraron registros';
  noData = false;
  constructor( private router: Router,
    private alert: AlertService,
    private outService: OutService,
    private loading: IsLoadingService)
  {
    this.enterprise_code = localStorage.getItem('ENTERPRISE_CODE');
    this.user_id = localStorage.getItem('USER_ID');
  }

  ngOnInit(): void {
    this.getOuts();
    this.getTotalItems();
  }

  handlePageEvent(event: PageEvent) {
    this.totalItems = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.getOuts(this.pageIndex, this.pageSize);
  }

  getOuts(pageIndex? : number, pageSize?:number) {
    this.loading.add(
      this.outService.getOuts(this.enterprise_code, pageIndex, pageSize).subscribe(data => {
       
        if (data && data.length > 0) {
          this.dataSource = data;
          console.log(this.dataSource);
        } else {
          this.noData = true;
        }
      },
        (error: HttpErrorResponse) => {
          this.noData = true;
          this.noDataMessage = 'Error de servidor, favor inténtelo nuevamente.';
        })

    );
  }

  getTotalItems() {
    this.outService.getTotalItems(this.enterprise_code).subscribe(data => {
      this.totalItems = data.total_items;
    })
  }

  add() {
    this.router.navigate(['/out/create']);
  }

  edit(id : number) {
    this.router.navigate(['/out/edit/'+ id]);
  }

  updateActive(id: number, value: boolean) {
    const body = {
      model: "outgoings", 
      schema: this.enterprise_code, 
      object_new: {
        id : id,
        active : value
      }    
    }
    this.loading.add(
      this.outService.updateActive(body).subscribe(data => {
          if (data.message) {
            this.getOuts();
          }
      })
    )
  }

}
