import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment.prod';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OutService {

  constructor(private http: HttpClient) { }

  getOuts(enterprise_code: string, pageIndex? : number, pageSize? : number): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization', 'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
  
    const pr = {
          'number_page': pageIndex ? pageIndex + 1 : 1,
          'size_page' : pageSize ? pageSize : 10,
          'model': 'outgoings',
          'schema': enterprise_code
      };

    return this.http.post(environment.pythonApiUrl + 'read',pr, {headers: header});

  }
  creaOut(body: any, number_page? : number, size_page? : number): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
  
    return this.http.post(environment.pythonApiUrl + 'create',body, {headers: header});

  }
    
  getOutEdit(enterprise_code: string, id :number): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
      const pr = {
          'schema': enterprise_code,
          'model': 'outgoings',
          'id' : id
      };

    return this.http.post(environment.pythonApiUrl + 'get',pr, {headers: header});
  }

  updateOut(body: any): Observable<any> {
      let header = new HttpHeaders();
      header = header.set('Content-Type', 'application/json');
      header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
    
      return this.http.post(environment.pythonApiUrl + 'update',body, {headers: header});

    }
    
  getTotalItems(enterprise_code: string): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
    const pr = {'model': 'outgoings', 'schema': enterprise_code};

    return this.http.post(environment.pythonApiUrl + 'total_items', pr, {headers: header});

  }

   updateActive(body: any): Observable<any> {
      let header = new HttpHeaders();
      header = header.set('Content-Type', 'application/json');
      header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
    
      return this.http.post(environment.pythonApiUrl + 'update',body, {headers: header});

    }
}
