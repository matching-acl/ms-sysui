import { DirectivesModule } from './../../directives/directives.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OutRoutingModule } from './out-routing.module';
import { OutComponent } from './out.component';
import { CreateComponent } from './components/create/create.component';
import { EditComponent } from './components/edit/edit.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { PipesModule } from '../../pipes/pipes.module';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [OutComponent, CreateComponent, EditComponent],
  imports: [
    CommonModule,
    OutRoutingModule,
    MatPaginatorModule,
    PipesModule,
    TooltipModule,
    FormsModule,
    DirectivesModule,
    ReactiveFormsModule,
  ]
})
export class OutModule { }
