import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from '../../../../service/alert.service';
import { OutService } from '../../service/out.service';
import { IsLoadingService } from '@service-work/is-loading';
import { InterfaceService } from '../../../interface/service/interface.service';
import { ConnectionOrigen } from '../../../interface/model/interface.model';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  form: FormGroup;
  enterprise_code: string;
  user_id: string;
  sub: any;
  id_url: number;
  conection_origen: ConnectionOrigen[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private alert: AlertService,
    private outService: OutService,
    private fb: FormBuilder,
    private loading: IsLoadingService,
    private interfaceService: InterfaceService)
  {
    this.enterprise_code = localStorage.getItem('ENTERPRISE_CODE');
    this.user_id = localStorage.getItem('USER_ID');
  }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
        this.id_url = +params['id'];
     });
    this.createForm();
    this.getOutById();
    this.setData();
  }

  setData() {
    this.interfaceService.getConnectionOrigin(this.enterprise_code).subscribe((data) => {
      this.conection_origen = data;
    }, error => {console.log({data: error});
    });
  }

  createForm(name? : string, connection_type?: string, connection_ip?: string,  connection_folder?: string, connection_port? : number, connection_user_name? : string, connection_password? : string) {
    this.form = this.fb.group({
      name : [name, Validators.required],
      connection_type : [connection_type, Validators.required],
      connection_ip: [connection_ip, Validators.required],
      connection_folder: [connection_folder, Validators.required],
      connection_port: [connection_port, Validators.required],
      connection_user_name: [connection_user_name, Validators.required],
      connection_password: [connection_password, Validators.required],
    })
  }

  getOutById() {
    this.loading.add(
      this.outService.getOutEdit(this.enterprise_code, this.id_url).subscribe(data => {
        console.log(data);
        this.createForm(data.name, data.e_origin, data.e_url ,data.e_entry_dir, data.e_port, data.e_username, data.e_password);
      })
    );
  }

   //Formularios validos
  get nameValid() {
    return this.form.get('name').valid && this.form.get('name').touched;
  }
  get connectionUrlValid() {
    return this.form.get('connection_url').valid && this.form.get('connection_url').touched;
  }
  get connectionPortValid() {
    return this.form.get('connection_port').valid && this.form.get('connection_port').touched;
  }
 get connectionUserNameValid() {
    return this.form.get('connection_user_name').valid && this.form.get('connection_user_name').touched;
  }
  get connectionPasswordValid() {
    return this.form.get('connection_password').valid && this.form.get('connection_password').touched;
  }
 get connectionIpValid() {
    return this.form.get('connection_ip').valid && this.form.get('connection_ip').touched;
  }
  get connectionFolderValid() {
    return this.form.get('connection_folder').valid && this.form.get('connection_folder').touched;
  }

  //formularios invalidos
  get nameNotValid() {
    return this.form.get('name').invalid && this.form.get('name').touched;
  }
  get connectionUrlNotValid() {
    return this.form.get('connection_url').invalid && this.form.get('connection_url').touched;
  }
  get connectionPortNotValid() {
    return this.form.get('connection_port').invalid && this.form.get('connection_port').touched;
  }
  get connectionUserNameNotValid() {
    return this.form.get('connection_user_name').invalid && this.form.get('connection_user_name').touched;
  }
  get connectionPasswordNotValid() {
    return this.form.get('connection_password').invalid && this.form.get('connection_password').touched;
  }
  get connectionIpNotValid() {
    return this.form.get('connection_ip').invalid && this.form.get('connection_ip').touched;
  }
  get connectionFolderNotValid() {
    return this.form.get('connection_folder').invalid && this.form.get('connection_folder').touched;
  }

  handelSave() {

    const form = this.form.value;
    const body = {
      schema : this.enterprise_code,
      model : "outgoings",
        object_new : {
          id : this.id_url, 
          name : form.name,
          e_origin : form.connection_type,
          url : form.connection_ip,
          username : form.connection_user_name,
          password : form.connection_password,
          entry_dir : form.connection_folder,
          port : form.connection_port,
          user_id: this.user_id,
        }
    }


    this.loading.add(
      this.outService.updateOut(body).subscribe(data => {
        if (data.message === true) {
          this.alert.success({
            text : 'Salida actualizada con exito'
          })
          setTimeout(() => {
           this.router.navigate(['/out']);
          }, 3000);
        }
        
        if (data.message === 'OBJECT EXISTS' || data.status === 400) {
          this.alert.info({
            title: form.name+' '+ 'ya se encuentra en nuestros registros',
            text: 'Por favor, intente con otro nombre',
          })
        }
      })
    )
  }

}
