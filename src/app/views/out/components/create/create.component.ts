import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertService } from '../../../../service/alert.service';
import { OutService } from '../../service/out.service';
import { IsLoadingService } from '@service-work/is-loading';
import { InterfaceService } from '../../../interface/service/interface.service';
import { ConnectionOrigen } from '../../../interface/model/interface.model';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  form: FormGroup;
  enterprise_code: string;
  user_id: string;
  conection_origen: ConnectionOrigen[];
  constructor(
    private router: Router,
    private alert: AlertService,
    private outService: OutService,
    private fb: FormBuilder,
    private loading: IsLoadingService,
    private interfaceService: InterfaceService
  )
  {
    this.enterprise_code = localStorage.getItem('ENTERPRISE_CODE');
    this.user_id = localStorage.getItem('USER_ID');
  }

  ngOnInit(): void {
    this.createForm();
    this.setData();
  }

  

  createForm(value? : any) {
    this.form = this.fb.group({
      name : ['', Validators.required],
      connection_type : [value,Validators.required],
      // connection_url: ['', Validators.required],
      connection_ip: ['', Validators.required],
      connection_folder: [''],
      connection_port: [''],
      connection_user_name: ['', Validators.required],
      connection_password: ['', Validators.required],
    })
  }

  setData() {
    this.interfaceService.getConnectionOrigin(this.enterprise_code).subscribe((data) => {
      this.conection_origen = data;
      this.createForm(data[0].tc_origin);
    }, error => {console.log({data: error});
    });
  }

  //Formularios validos
  get nameValid() {
    return this.form.get('name').valid && this.form.get('name').touched;
  }
  get connectionUrlValid() {
    return this.form.get('connection_url').valid && this.form.get('connection_url').touched;
  }
  get connectionPortValid() {
    return this.form.get('connection_port').valid && this.form.get('connection_port').touched;
  }
 get connectionUserNameValid() {
    return this.form.get('connection_user_name').valid && this.form.get('connection_user_name').touched;
  }
  get connectionPasswordValid() {
    return this.form.get('connection_password').valid && this.form.get('connection_password').touched;
  }
 get connectionIpValid() {
    return this.form.get('connection_ip').valid && this.form.get('connection_ip').touched;
  }
  get connectionFolderValid() {
    return this.form.get('connection_folder').valid && this.form.get('connection_folder').touched;
  }

  //formularios invalidos
  get nameNotValid() {
    return this.form.get('name').invalid && this.form.get('name').touched;
  }
  get connectionUrlNotValid() {
    return this.form.get('connection_url').invalid && this.form.get('connection_url').touched;
  }
  get connectionPortNotValid() {
    return this.form.get('connection_port').invalid && this.form.get('connection_port').touched;
  }
  get connectionUserNameNotValid() {
    return this.form.get('connection_user_name').invalid && this.form.get('connection_user_name').touched;
  }
  get connectionPasswordNotValid() {
    return this.form.get('connection_password').invalid && this.form.get('connection_password').touched;
  }
  get connectionIpNotValid() {
    return this.form.get('connection_ip').invalid && this.form.get('connection_ip').touched;
  }
  get connectionFolderNotValid() {
    return this.form.get('connection_folder').invalid && this.form.get('connection_folder').touched;
  }

  handelSave() {

    const form = this.form.value;
    const body = {
        schema : this.enterprise_code,
        model : "outgoings",
        object : {
          name : form.name,
          e_origin : form.connection_type,
          url : form.connection_ip,
          username : form.connection_user_name,
          password : form.connection_password,
          entry_dir : form.connection_folder,
          port : form.connection_port,
          user_id : this.user_id
        }
    }

    this.loading.add(
      this.outService.creaOut(body).subscribe(data => {
        console.log(data);
       if (data.message === true ) {
          this.alert.success({
            text: 'Salida agregada con exito',
          })
          setTimeout(() => {
           this.router.navigate(['/out']);
          }, 3000);
        }
        if (data.message === 'OBJECT EXISTS' || data.status === 400) {
          this.alert.info({
            title: form.name+' '+ 'ya se encuentra en nuestros registros',
            text: 'Por favor, intentelo nuevamente',
          })
        }

      },
        (error: HttpErrorResponse) => {
        console.log(error.error);
      })
    )
    
  }

}
