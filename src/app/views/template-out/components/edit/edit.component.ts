import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { IsLoadingService } from '@service-work/is-loading';
import { Router, ActivatedRoute } from '@angular/router';
import { TemplateOutService } from '../../service/template-out.service';
import { AlertService } from '../../../../service/alert.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  form: FormGroup;
  enterprise_code: string;
  sub: any;
  id_url: number;
  id_template: number;
  selectedTemplate = [];
  headers = [];
  headersTemp = [];
  code = [];
  headersCompare = [];
  user_id: string;
  isCheck = true;
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private alert: AlertService,
    private serviceTemp: TemplateOutService,
    private fb: FormBuilder,
    private loading: IsLoadingService)
  {
    this.enterprise_code = localStorage.getItem('ENTERPRISE_CODE');
    this.user_id = localStorage.getItem('USER_ID');
  }

  ngOnInit(): void {
     this.sub = this.route.params.subscribe(params => {
        this.id_url = +params['id'];
     });
    this.createForm();
    this.getTemplate();
    this.getTemplateSelect();
    
  }

   getHeader() {
    //  this.headersTemp = [];
    const id = this.form.get('template').value;
    this.headers = [];
    for (let i = 0; i < this.selectedTemplate.length; i++) {
      const element = this.selectedTemplate[i];
      if (element.id === +id) {
        this.headers = element.header
      }
      
    }
   }
  


   getTemplateSelect() {
    this.loading.add(this.serviceTemp.getTemplates(this.enterprise_code).subscribe(data => {
      this.selectedTemplate = data;
     console.log(data);
      
      for (let i = 0; i < this.selectedTemplate.length; i++) {
        const element = this.selectedTemplate[i];
        if (this.form.get('template').value === element.id) {
          this.headersCompare.push(element.header)
        }
        
      }
     
      this.getHeader();
    }));
   
   }
  
 

  createForm(name? : string, id?:number) {
    this.form = this.fb.group(
      {
        name : [name, Validators.required],
        template: [id?id:'', Validators.required],
        check: []
      }
    )
  }

  focusout() {
    const id = this.form.get('template').value
    console.log('focus');
    console.log(id, this.id_template);
    if (this.id_template != id) {
     this.headersTemp = [];
    } else {
      this.getTemplate();
    }
    
  }

  onChange(item: any, e: any, indice: number) {
    
    const id = this.form.get('template').value
    console.log(id, this.id_template);
    if (e.target.checked) {
      this.headersTemp.push(item);
    }
    if (!e.target.checked) {
      if (this.headersTemp.length > 0) {
        for (let i = 0; i < this.headersTemp.length; i++) {
          const element = this.headersTemp[i];
          if (element.code === item.code) {
            this.headersTemp.splice(i,1)
          }
        }
      }
    }
    console.log('reques', this.headersTemp);
  }

  isChange($event) {
    this.isCheck = false;
  }

  getTemplate() {
    this.loading.add(
      this.serviceTemp.getTemplatesEdit(this.enterprise_code, this.id_url).subscribe(data => {
        console.log(data.header);

        this.headersTemp = [];
        this.headersTemp = data.header;
        this.id_template = data.tc_template_id;
        console.log(data.tc_template_id);
        console.log('update/create', this.headersTemp);
        this.createForm(data.name, data.tc_template_id);

        for (const i of data.header) {
           this.code.push(i.code)
        }

       this.headers = [];
        for (let i = 0; i < this.selectedTemplate.length; i++) {
          const element = this.selectedTemplate[i];
          if (element.id === data.tc_template_id) {
            this.headers = element.header
          }
        }
      })
    );
  }

  guardar() {
    console.log(this.headersTemp);
    const form = this.form.value;
    const body = {
      schema : this.enterprise_code,
      model : "outgoings_templates",
      object_new: {
        id : this.id_url, 
        name : form.name,
        template_id : form.template, 
        user_id : +this.user_id, 
        header : this.headersTemp
      }
    }
    console.log(body);
    this.loading.add(
      this.serviceTemp.updateTemplatesOut(body).subscribe(data => {
        console.log(data);
        if (data.status === '200' || data.message === true ) {
          this.alert.success({
            text : 'Template de salida actualizado con exito'
          })
          setTimeout(() => {
             this.router.navigate(['/template-out']);
            }, 3000);
          
        }
        if (data.message === 'OBJECT EXISTS' || data.status === 400 ) {
          this.alert.info({
            title: form.name+' '+ 'ya se encuentra en nuestros registros',
            text: 'Por favor, intentelo nuevamente',
          })
        }
      })
    )
  }

  
  

}
