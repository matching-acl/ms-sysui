import { Component, OnInit } from '@angular/core';
import { TemplateOutService } from '../../service/template-out.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IsLoadingService } from '@service-work/is-loading';
import { AlertService } from '../../../../service/alert.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  form: FormGroup;
  enterprise_code: string;
  user_id: string;
  selectedTemplate = [];
  headers = [];
  headersTemp = [];
  constructor(private router: Router, private alert : AlertService, private serviceTemp: TemplateOutService, private fb : FormBuilder, private loading : IsLoadingService) {
    this.enterprise_code = localStorage.getItem('ENTERPRISE_CODE');
    this.user_id = localStorage.getItem('USER_ID');
  }

  ngOnInit(): void {
    this.getTemplates();
    this.createForm();
  }

  createForm(id?:number) {
    this.form = this.fb.group(
      {
        name : ['', Validators.required],
        template: ['', Validators.required]
      }
    )
  }

  
  getTemplates() {
    this.loading.add(this.serviceTemp.getTemplates(this.enterprise_code).subscribe(data => {
      this.selectedTemplate = data;
      console.log('template select',this.selectedTemplate);
      this.selectedTemplate[0].id
      this.createForm( this.selectedTemplate[0].id);
    }));
   
  }

  getHeader() {
    
    const id = this.form.get('template').value;
    this.headers = [];
    for (let i = 0; i < this.selectedTemplate.length; i++) {
      const element = this.selectedTemplate[i];
      if (element.id === +id) {
        this.headers = element.header
      }
      
    }
  }

  onChange(item: any, e: any, indice : number) {
    
    if (e.target.checked) {
      this.headersTemp.push(item);
    }
    if (!e.target.checked) {
      if (this.headersTemp.length > 0) {
        for (let i = 0; i < this.headersTemp.length; i++) {
          const element = this.headersTemp[i];
          if (element.code === item.code) {
            this.headersTemp.splice(i,1)
          }
          
        }
        
      }
      
    }
    console.log(this.headersTemp);
  }

  guardar() {

    const form = this.form.value;
    const body = {
      schema: this.enterprise_code,
      model: 'outgoings_templates',
      object: {
        name: form.name,
        template_id: +form.template,
        user_id: +this.user_id,
        header : this.headersTemp
        
      }

    }
    console.log('request',body);
    this.loading.add(
      this.serviceTemp.createTemplatesOut(body).subscribe(data => {
        console.log(data);
        if (data.status === '200' || data.message === true ) {
          this.alert.success({
            text: 'Template de salida agregado con exito',
          })
            setTimeout(() => {
             this.router.navigate(['/template-out']);
            }, 3000);
        }
        if (data.message === 'OBJECT EXISTS' || data.status === 400 ) {
          this.alert.info({
            title: form.name+' '+ 'ya se encuentra en nuestros registros',
            text: 'Por favor, intentelo nuevamente',
          })
        }

      })
    )
  }

  

}
