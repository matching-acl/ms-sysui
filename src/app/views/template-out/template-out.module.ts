import { DirectivesModule } from './../../directives/directives.module';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PipesModule } from './../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { TemplateOutRoutingModule } from './template-out-routing.module';
import { TemplateOutComponent } from './template-out.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CreateComponent } from './components/create/create.component';
import { EditComponent } from './components/edit/edit.component';
import { CustomMatPaginatorIntl } from '../paginator-es';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';



@NgModule({
  declarations: [TemplateOutComponent, CreateComponent, EditComponent],
  imports: [
    MatPaginatorModule,
    CommonModule,
    TemplateOutRoutingModule,
    PipesModule,
    TooltipModule,
    DirectivesModule,
    ReactiveFormsModule,
  ],
   providers: [
    {
      provide: MatPaginatorIntl,
      useClass: CustomMatPaginatorIntl,
    },
    DatePipe,
  ]
})
export class TemplateOutModule { }
