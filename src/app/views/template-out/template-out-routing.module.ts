import { AuthGuardService } from './../login/service/auth-guard.service';
import { TemplateOutComponent } from './template-out.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateComponent } from './components/create/create.component';
import { EditComponent } from './components/edit/edit.component';


const routes: Routes = [
  { path: '', component: TemplateOutComponent, canActivate: [AuthGuardService] },
  { path: 'create', component: CreateComponent, canActivate: [AuthGuardService]},
  { path: 'edit/:id', component: EditComponent, canActivate: [AuthGuardService]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TemplateOutRoutingModule { }
