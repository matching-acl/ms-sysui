import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment.prod';
import { Observable } from 'rxjs';
import { Template } from '../../config-conciliation/model/template';

@Injectable({
  providedIn: 'root'
})
export class TemplateOutService {

  constructor(private http: HttpClient) { }


    getTemplatesOut(enterprise_code: string, pageIndex? : number, pageSize? : number): Observable<any> {
      let header = new HttpHeaders();
      header = header.set('Content-Type', 'application/json');
      header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
        const pr = {
            'number_page': pageIndex ? pageIndex + 1 : 1,
            'size_page' : pageSize ? pageSize : 10,
            'model': 'outgoings_templates',
            'schema': enterprise_code
        };

      return this.http.post(environment.pythonApiUrl + 'read',pr, {headers: header});

    }
    getTemplatesEdit(enterprise_code: string, id :number): Observable<any> {
      let header = new HttpHeaders();
      header = header.set('Content-Type', 'application/json');
      header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
        const pr = {
            'schema': enterprise_code,
            'model': 'outgoings_templates',
            'id' : id
        };

      return this.http.post(environment.pythonApiUrl + 'get',pr, {headers: header});
    }
    getTotalItems(enterprise_code: string): Observable<any> {
      let header = new HttpHeaders();
      header = header.set('Content-Type', 'application/json');
      header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
      const pr = {'model': 'outgoings_templates', 'schema': enterprise_code};

      return this.http.post(environment.pythonApiUrl + 'total_items', pr, {headers: header});

    }
    createTemplatesOut(body: any, number_page? : number, size_page? : number): Observable<any> {
      let header = new HttpHeaders();
      header = header.set('Content-Type', 'application/json');
      header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
    
      return this.http.post(environment.pythonApiUrl + 'create',body, {headers: header});

    }
    updateTemplatesOut(body: any): Observable<any> {
      let header = new HttpHeaders();
      header = header.set('Content-Type', 'application/json');
      header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
    
      return this.http.post(environment.pythonApiUrl + 'update',body, {headers: header});

    }
    enableDisableTemplatesOut(body: any): Observable<any> {
      let header = new HttpHeaders();
      header = header.set('Content-Type', 'application/json');
      header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
    
      return this.http.post(environment.pythonApiUrl + 'update',body, {headers: header});

    }
  
    getTemplates(enterprise_code: string, number_page? : number, size_page? : number): Observable<any> {
      let header = new HttpHeaders();
      header = header.set('Content-Type', 'application/json');
      header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
        const pr = {
            'number_page': number_page ? number_page + 1 : 1,
            'size_page' : size_page ? size_page : 10,
            'is_enum': '0',
            'model': 'templates',
            'schema': enterprise_code,
            "filters" : {
              "e_state" : "Listo"
             }
        };

     return this.http.post<any>(environment.pythonApiUrl + 'read', pr, { headers: header });
    }
  

}
