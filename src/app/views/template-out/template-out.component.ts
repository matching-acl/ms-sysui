import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { TemplateOutService } from './service/template-out.service';
import { IsLoadingService } from '@service-work/is-loading';
import { PageEvent } from '@angular/material/paginator';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
  selector: 'app-template-out',
  templateUrl: './template-out.component.html',
  styleUrls: ['./template-out.component.scss']
})
export class TemplateOutComponent implements OnInit {
  totalItems : number;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [5, 10, 25];
  showFirstLastButtons = true;
  noDataMessage = 'No se encontraron registros';
  enterprise_code: string;
  dataSource = [];
  noData = false;
  constructor(private router: Router, private serviceTemp: TemplateOutService, private loading : IsLoadingService) {
    this.enterprise_code = localStorage.getItem('ENTERPRISE_CODE');
   }

  ngOnInit(): void {

    this.getTemplatesOut();
    this.getTotalItems();
    
  }

   handlePageEvent(event: PageEvent) {
    this.totalItems = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.getTemplatesOut(this.pageIndex, this.pageSize)
  }

  getTemplatesOut(pageIndex? : number, pageSize?:number) {
    this.loading.add(
      this.serviceTemp.getTemplatesOut(this.enterprise_code, pageIndex, pageSize).subscribe(data => {
       
        if (data && data.length > 0) {
          this.dataSource = data;
        } else {
          this.noData = true;
        }
      },
        (error: HttpErrorResponse) => {
          this.noData = true;
          this.noDataMessage = 'Error de servidor, favor inténtelo nuevamente.';
        })

    );
  }

  getTotalItems() {
    this.serviceTemp.getTotalItems(this.enterprise_code).subscribe(data => {
      this.totalItems = data.total_items;
      console.log(data);
    })
  }
 

  add() {
    this.router.navigate(['/template-out/create']);
  }

  edit(id : number) {
    this.router.navigate(['/template-out/edit/'+ id]);
  }

  updateActive(id: number, value: boolean) {
    const body = {
      model: "outgoings_templates", 
      schema: this.enterprise_code, 
      object_new: {
        id : id,
        active : value
      }    
    }
    this.loading.add(
      this.serviceTemp.enableDisableTemplatesOut(body).subscribe(data => {
          if (data.message) {
            this.getTemplatesOut();
          }
      })
    )
  }

}
