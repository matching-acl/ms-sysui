import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
// @ts-ignore
import {RegisterUserService} from './service/register-user.service';
import {UserService} from '../user/services/user.service';
import {AlertService} from '../../service/alert.service';
import Swal from "sweetalert2";

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.scss']
})
export class RegisterUserComponent implements OnInit {
  spinner = false;
  form: FormGroup;
  testError = '';
  isError = false;
  hidePassword = true;
  hideConfirmPassword = true;
  constructor(
    private fb: FormBuilder,
    private registerUserService: RegisterUserService,
    private router: Router,
    private alert: AlertService) { }

  ngOnInit(): void {
    this.createForm();

    const inputs = document.querySelectorAll('.input');
    const selects = document.querySelectorAll('.select');

    function focusFunc() {
      const parent = this.parentNode.parentNode;
      parent.classList.add('focus');
    }
    function blurFunc() {
      const parent = this.parentNode.parentNode;
      if (this.value === '') {
        parent.classList.remove('focus');
      }
    }

    inputs.forEach(input => {
      input.addEventListener('focus', focusFunc);
      input.addEventListener('blur', blurFunc);
    });
    selects.forEach(select => {
      select.addEventListener('focus', focusFunc);
      select.addEventListener('blur', blurFunc);
    });
  }
// Formularios validos
  get userNameValid() {
    return this.form.get('user_name').valid && this.form.get('user_name').touched;
  }
  get emailValid() {
    return this.form.get('email').valid && this.form.get('email').touched;
  }
  get passwordValid() {
    return this.form.get('password').valid && this.form.get('password').touched;
  }
  get confirmPasswordValid() {
    return this.form.get('confirm_password').valid && this.form.get('confirm_password').touched &&
      this.form.get('password').value === this.form.get('confirm_password').value;
  }
  get enterpriseNameValid() {
    return this.form.get('enterprise_name').valid && this.form.get('enterprise_name').touched;
  }
  get enterpriseCodeValid() {
    return this.form.get('enterprise_code').valid && this.form.get('enterprise_code').touched;
  }

  // Formularios invalidos
  get userNameInvalid() {
    return this.form.get('user_name').invalid && this.form.get('user_name').touched;
  }
  get usernameInvalidLength() {
    const name = this.form.get('user_name').value;
    return  name && name.length > 0 && this.form.get('user_name').errors && (this.form.get('user_name').touched );
  }
  get emailInvalid() {
    return this.form.get('email').invalid && this.form.get('email').touched;
  }
  get passwordInvalid() {
    const pass = this.form.get('password').value;
    return (pass && pass.length === 0 &&  this.form.get('password').invalid && this.form.get('password').touched) ;
  }
  get enterpriseNameInvalid() {
    return this.form.get('enterprise_name').invalid && this.form.get('enterprise_name').touched;
  }
  get enterpriseCodeInvalid() {
    return this.form.get('enterprise_code').invalid && this.form.get('enterprise_code').touched;
  }

  get passwordInvalidLength() {
    const pass = this.form.get('password').value;
    return  pass && pass.length > 0 && this.form.get('password').errors && this.form.get('password').touched;
  }
  get confirmPasswordInvalid() {
    const confirmPass = this.form.get('confirm_password').value;
    return confirmPass && confirmPass.length === 0 && this.form.get('confirm_password').invalid &&
      this.form.get('confirm_password').touched;
  }
  get passwordIsDifferent() {
    const confirmPass = this.form.get('confirm_password').value;
    return confirmPass && confirmPass.length >= 1 && this.form.get('confirm_password').touched &&
      this.form.get('password').value !== this.form.get('confirm_password').value;
  }

  get comparePassUserName() {
    const value = this.form.get('password').value;
    return value && value.length >= 8 && this.form.get('user_name').value ===  this.form.get('password').value;
  }

  createForm() {
    this.form = this.fb.group({
      user_name: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [ Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
     // password: ['', [Validators.required, Validators.minLength(8)]],
    //  confirm_password: ['', [Validators.required, Validators.minLength(8)], ],
      enterprise_name: ['', [Validators.required]],
      enterprise_code: ['', [Validators.required]]
    });
  }

  onClick(control_name: string) {
    this.form.controls[`${control_name}`].reset();
    this.testError = '';
    this.isError = false;
  }

  handelSave() {
    const user = this.form.controls['user_name'].value;
    const enterprise_name = this.form.controls['enterprise_name'].value;
    const enterprise_code = this.form.controls['enterprise_code'].value;
    const email = this.form.controls['email'].value;

   this.spinner = true;
    this.form.disable();
    this.registerUserService.getRegisterUser(user, email, enterprise_name, enterprise_code).subscribe((data) => {
      console.log({dataaa: data});
      if (data && data.status === '200') {
        setTimeout(() => {
          Swal.fire({
            icon: 'info',
            title: 'Correcto',
            text: 'Registro exitoso. La contraseña fue enviada al correo ingresado.',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar'
          }).then((result) => {
            if (result.isConfirmed) {
              this.spinner = false;
              this.form.enable();
              this.router.navigateByUrl('/login');
            }
          });
        });
      } else if (data && data.status === '501') {
        this.spinner = false;
        this.form.enable();
        this.testError = 'Código de la empresa expirado.';
        this.isError = true;
      } else if (data && data.status === '502') {
        this.spinner = false;
        this.form.enable();
        this.testError = 'Código de la empresa no válido.';
        this.isError = true;
      } else {
        this.spinner = false;
        this.form.enable();
        this.testError = 'Error en el registro.';
        this.isError = true;
      }
    }, (err) => {
      console.log({errrrr: err});
      this.spinner = false;
      this.form.enable();
      this.testError = 'Error en el registro.';
      this.isError = true;
    });
  }
}

