import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})

export class RegisterUserService {
  username: any = 'admin';
  password: any = 'admin';
  authorization = 'Basic ' + btoa(this.username + ':' + this.password);

  headers() {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization', this.authorization);
    return header;
  }
  constructor(
    private http: HttpClient) {
  }

  getRegisterUser (user: string, email: string, enterprise_name: string, enterprise_code: string ): Observable<any> {

    const body = {
      'code': enterprise_code,
      'username': user,
      'email': email,
      'enterpriseName': enterprise_name,
      'status': '',
      'message': true
    };
    return this.http.post(environment.coreApiUrl + 'tenant/security/login/first', body, { headers: this.headers()});
  }
}
