import { formatDate } from '@angular/common';
import { Component, Input, OnInit, Output} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef} from 'ngx-bootstrap/modal';
import { EventEmitter } from '@angular/core';
import Swal from 'sweetalert2';
import { EnterpriseService } from '../../services/enterprise.service';
import {HttpErrorResponse} from '@angular/common/http';
import {AlertService} from '../../../../service/alert.service';

@Component({
  selector: 'add-enterprise-modal',
  templateUrl: './add-enterprise-modal.component.html',
  styleUrls: ['./add-enterprise-modal.component.scss']
})
export class AddEnterpriseModalComponent implements OnInit {
  form: FormGroup;
  xx: boolean = true;
  myDate: any;
  @Input() modal_ref: BsModalRef;
  @Output() executeAddEnterprise: EventEmitter<any> = new EventEmitter();
  hiddenButton = false;
  spinner = false;
  step1 = true;
  step2 = false;
  step3 = false;

  constructor(
    private fb: FormBuilder,
    private enterprise: EnterpriseService,
    private alert: AlertService
  ) { }

  ngOnInit(): void {
    this.createForm();
  }


  createForm() {
    this.form = this.fb.group({
      enterprise_name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
      enterprise_description: ['', [Validators.maxLength(80)]],
    });
  }

  // formularios validos
  get enterpriseNameValid() {

    return this.form.get('enterprise_name').valid && this.form.get('enterprise_name').touched;
  }

  // Formularios invalidos
  get enterpriseNameInvalid() {
    const value = this.form.get('enterprise_name').value;
    return  value.length === 0 && this.form.get('enterprise_name').invalid && this.form.get('enterprise_name').touched;
  }
  get enterpriseNameInvalidLength() {
    const value = this.form.get('enterprise_name').value;
    return  value.length > 0 && this.form.get('enterprise_name').errors && this.form.get('enterprise_name').touched;
  }


  get comparePassEnterpriseName() {
    const value = this.form.get('password').value;
    return value.length >= 8 && this.form.get('enterprise_name').value === this.form.get('password').value;
  }

  get buttonDisabled() {
    return this.form.get('enterprise_name').valid;
  }

  handelCancel() {
    this.modal_ref.hide();
  }


  handelSave() {

    const form = this.form.value;
    this.myDate = formatDate( new Date(), 'yyyy-MM-dd\'T\'HH:mm:ssZ', 'en');
    const body = {
      'active': true,
      'code': '',
      'created': this.myDate,
      'description': form.enterprise_description,
      'id': 5,
      'name': form.enterprise_name,
      'updated': this.myDate
    };

    this.spinner = true;
    this.hiddenButton = true;

    this.enterprise.createEnterprises(body).subscribe(( data ) => {
      console.log(data);

      if (data && data.status === '200') {
        Swal.fire({
          icon: 'success',
          title: 'Correcto',
          text: 'Registro de la empresa exitoso ',
          timer: 3000,
          showCancelButton: false,
          showConfirmButton: false
        });
          this.spinner = false;
          this.hiddenButton = false;
          this.handelCancel();
          this.executeAddEnterprise.emit();

      } else if (data && data.status === '400') {
        this.spinner = false;
        this.handelCancel();
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: data.message,
          timer: 3000,
          showCancelButton: false,
          showConfirmButton: false
        });
      } else {
        this.spinner = false;
        this.handelCancel();
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'No se ha podido registrar la empresa ',
          timer: 3000,
          showCancelButton: false,
          showConfirmButton: false
        });
      }
    }, (error: HttpErrorResponse) => {
      this.spinner = false;
      this.handelCancel();
      this.alert.error({
        title: 'Error',
        text: 'Ha ocurrido un error, por favor vuelva a intentarlo más tarde.'
      });
    });
  }

  nextPrev(step: number) {
    this.step1 = step === -1;
    this.step2 = step === 1;
    this.step3 = step === 2;
  }

}
