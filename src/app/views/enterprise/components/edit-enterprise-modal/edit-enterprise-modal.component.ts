import { formatDate } from '@angular/common';
import { Component, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
// @ts-ignore
import { EnterpriseService } from '../../services/enterprise.service';
import { EventEmitter } from '@angular/core';
import Swal from 'sweetalert2';
import {ENTERPRISE_CODE, ENTERPRISE_NAME, USER_NAME} from '../../../login/models/login';
import {Enterprise} from '../../model/enterprise';
import {Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {AlertService} from '../../../../service/alert.service';

@Component({
  selector: 'app-edit-enterprise-modal',
  templateUrl: './edit-enterprise-modal.component.html',
  styleUrls: ['./edit-enterprise-modal.component.scss']
})
export class EditEnterpriseModalComponent implements OnInit {
  @Input() modal_ref: BsModalRef;
  @Input() dataEdit: Enterprise;
  @Output() executeUpdateEnterprise: EventEmitter<any> = new EventEmitter();
  form: FormGroup;
  myDate: any;
  spinner: boolean;
  enterprise_name: string;
  enterprise_login: boolean;
  constructor(
    private fb: FormBuilder,
    private enterprise: EnterpriseService,
    private router: Router,
    private alert: AlertService
  ) { }

  ngOnInit(): void {
    this.enterprise_name = localStorage.getItem(ENTERPRISE_NAME);
    this.enterprise_login = this.dataEdit && this.dataEdit.name && this.dataEdit.name === this.enterprise_name;
    this.createForm(this.dataEdit);
  }

  createForm(dataEditForm) {
    this.form = this.fb.group({
      enterprise_name: [dataEditForm.name, [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
      enterprise_description: [dataEditForm.description, [Validators.maxLength(80)]]
    });
  }

  // formularios validos
  get enterpriseNameValid() {
    return this.form.get('enterprise_name').valid && this.form.get('enterprise_name').touched;
  }

  // Formularios invalidos
  get enterpriseNameInvalid() {
    const value = this.form.get('enterprise_name').value;
    return  value.length === 0 && this.form.get('enterprise_name').invalid && this.form.get('enterprise_name').touched;
  }
  get enterpriseNameInvalidLength() {
    const value = this.form.get('enterprise_name').value;
    return  value.length > 0 && this.form.get('enterprise_name').errors && this.form.get('enterprise_name').touched;
  }

  handelCancel() {
    this.modal_ref.hide();
  }

  handelSave() {
    this.spinner = true;
    const form = this.form.value;
    this.myDate = formatDate( new Date(), 'yyyy-MM-dd\'T\'HH:mm:ssZ', 'en');
    const saveData = {
      'active': this.dataEdit.active,
      'code': this.dataEdit.code,
      'created': this.dataEdit.created,
      'description': form.enterprise_description,
      'id': this.dataEdit.id,
      'name': form.enterprise_name,
      'updated': this.myDate
    };
    console.log(saveData);

    const redirect_login = (this.enterprise_login && form.enterprise_name !== this.dataEdit.name);
    console.log({redirect_login: redirect_login, enterprise_login: this.enterprise_login, enterprise_nameFORM: form.enterprise_name, NAME: this.dataEdit.name});
    this.enterprise.updateEnterprise(saveData).subscribe((data) => {
      if (data == null) {
        if (redirect_login) {
          setTimeout(() => {
            Swal.fire({
              icon: 'info',
              title: 'Correcto',
              text: 'Al cambiar el nombre de la empresa de la empresa logueada se redireccionará al login.',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Aceptar'
            }).then((result) => {
              if (result.isConfirmed) {
                this.spinner = false;
                this.form.enable();
                this.handelCancel();
                this.router.navigateByUrl('/login');
              }
            });
          });
        } else {
          Swal.fire({
            icon: 'success',
            title: 'Correcto',
            text: 'Empresa actualizado correctamente ',
            timer: 3000,
            showCancelButton: false,
            showConfirmButton: false
          });
          this.spinner = false;
          this.handelCancel();
          this.executeUpdateEnterprise.emit();
        }
      } else {
        this.spinner = false;
        this.handelCancel();
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'No se ha podido registrar la empresa ',
          timer: 3000,
          showCancelButton: false,
          showConfirmButton: false
        });
      }
    },
      (error: HttpErrorResponse) => {
        this.spinner = false;
        this.handelCancel();
        this.alert.error({
          title: 'Error',
          text: 'Ha ocurrido un error, por favor vuelva a intentarlo más tarde.'
        });
      });
  }

}
