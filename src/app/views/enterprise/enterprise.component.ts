import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import {EnterpriseService} from './services/enterprise.service';
import {IsLoadingService} from '@service-work/is-loading';
import {HttpErrorResponse} from '@angular/common/http';
import {ENTERPRISE_NAME, ROLE, USER_NAME} from '../login/models/login';
import {RolesNumber, RolesText} from '../rol/model/rol';
import {Enterprise} from './model/enterprise';
import {PageEvent} from '@angular/material/paginator';
import Swal from "sweetalert2";
import {Router} from '@angular/router';


@Component({
  selector: 'enterprise',
  templateUrl: './enterprise.component.html',
  styleUrls: ['./enterprise.component.scss']
})
export class EnterpriseComponent implements OnInit {
  user: string;
  role: string;
  title = 'Empresas';
  noData = false;
  noDataMessage = 'No se encontraron registros';
  dataSource: Enterprise[];
  dataEdit: Enterprise;
  modalRef: BsModalRef;
  RolesText = RolesText;
  totalItems: number;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [1, 5, 10, 25];
  showFirstLastButtons = true;
  constructor(
    private modalService: BsModalService,
    private loadService: IsLoadingService,
    private enterpriseService: EnterpriseService,
    private loadingService: IsLoadingService,
    private router: Router) {
    this.user = localStorage.getItem('USER_NAME');
    this.role = localStorage.getItem('ROLE');
  }

  ngOnInit(): void {
    this.getEnterprise();
  }

  getEnterprise(pageIndex?: number, pageSize?: number) {
    this.getAllEnterpriseByUser(this.user, pageIndex, pageSize);
  }

  getAllEnterprise(pageIndex?: number, pageSize?: number) {
    this.totalItems = 0;
    this.loadingService.add(
      this.enterpriseService.getAllEnterprises(pageIndex, pageSize).subscribe(data => {
          console.log(data);
          if (data && data.elements) {
            this.dataSource = data.elements;
            this.totalItems = data.totalElements;
          } else {
            this.noData = true;
          }
        },
        (error: HttpErrorResponse) => {
          console.log({error: error});
          this.noData = true;
          this.noDataMessage = 'Error de servidor, favor intentelo nuevamente.';
        })
    );
  }

  getAllEnterpriseByUser(user: string, pageIndex?: number, pageSize?: number) {
    this.totalItems = 0;
    this.loadingService.add(
      this.enterpriseService.getEnterprisesByUser(user, pageIndex, pageSize).subscribe(data => {
          console.log(data);
          if (data && data.enterprises && data.enterprises.elements) {
            this.dataSource = data.enterprises.elements;
            this.totalItems = data.totalElements;
          } else {
            this.noData = true;
          }
        },
        (error: HttpErrorResponse) => {
          console.log({error: error});
          this.noData = true;
          this.noDataMessage = 'Error de servidor, favor intentelo nuevamente.';
        })
    );
  }

  handlePageEvent(event: PageEvent) {
    this.totalItems = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.getEnterprise(this.pageIndex, this.pageSize);
  }

  get status() {
    return !!this.dataSource.filter((data) => data && data.active === false);
  }

  addEnterprise(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    this.modalRef.setClass('modal-lg');
  }

  editEnterprise(template: TemplateRef<any>, data) {
    this.modalRef = this.modalService.show(template);
    this.modalRef.setClass('modal-lg');
    this.dataEdit = data;
  }

  updateActiveEnterprise(id: number, status: boolean) {
    const body = {
      'fieldValue': {active: !status},
      'id': id
    };
    console.log(body);
    setTimeout(() => {
      Swal.fire({
        icon: 'info',
        title: 'Correcto',
        text: 'Para hacer efectivo el cambio, es necesario volver a iniciar sesión. \n' +
          'Gracias por interactuar con Matching!',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Aceptar'
      }).then((result) => {
        if (result.isConfirmed) {
          this.enterpriseService.enableDisableEnterprise(body).subscribe((data) => {
            console.log(data);
            this.router.navigateByUrl('/login');
          });
        }
      });
    });
  }

  isEnterpriseLog(enterprise_name: string) {
    return enterprise_name === localStorage.getItem(ENTERPRISE_NAME);
  }

  hiddenActive() {
    const user_role = localStorage.getItem(ROLE);
    return user_role !== RolesText.admin_holding;
  }

}
