import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment.prod';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EnterpriseService {
  username: any = 'admin';
  password: any = 'admin';
  authorizationData = 'Basic ' + btoa(this.username + ':' + this.password);
  constructor(private http: HttpClient) {}

  getAllEnterprises(pageIndex?: number, pageSize?: number): Observable<any> {

    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization', this.authorizationData);

    const body = {'fields' : {}, 'page': pageIndex, 'search': '', 'size': pageSize};
    return this.http.post(environment.coreApiUrl + 'enterprise/search', body, { headers: header });
  }

  getEnterprisesActive(): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization', this.authorizationData);

    const body = {'fields': {'active': 'true'}, 'page': 0, 'size': 0, 'search': ''};
    return this.http.post(environment.coreApiUrl + 'enterprise/search', body, { headers: header});
  }

  getEnterprisesByUser( user_name: string, pageIndex?: number, pageSize?: number): Observable<any> {
    const username = 'admin';
    const password = 'admin';
    const authorizationData = 'Basic ' + btoa(username + ':' + password);
    console.log(authorizationData);
    let header = new HttpHeaders();

    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization', authorizationData);

    const body = {'fields' : {}, 'page': pageIndex, 'search': user_name, 'size': pageSize};
    console.log({body: body, header: header});
    return this.http.post(environment.coreApiUrl + 'user/tenant', body, { headers: header});
  }


  createEnterprises(data): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization', this.authorizationData);
    console.log(data);

    return this.http.post(environment.coreApiUrl + 'enterprise', data, { headers: header });
  }

  updateEnterprise(data): Observable<any> {
    console.log(data);
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization', this.authorizationData);

    return this.http.put(environment.coreApiUrl + 'enterprise', data, {headers: header});
  }

  enableDisableEnterprise(data): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization', this.authorizationData);

    return this.http.patch(environment.coreApiUrl + 'enterprise', data, {headers: header});
  }


}
