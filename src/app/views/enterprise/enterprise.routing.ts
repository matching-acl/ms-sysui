import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {EnterpriseComponent} from './enterprise.component';
import {AuthGuardService} from '../login/service/auth-guard.service';

const routes: Routes = [
  { path: '', component: EnterpriseComponent, canActivate: [AuthGuardService]}
];

@NgModule ({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EnterpriseRoutingModule {}

