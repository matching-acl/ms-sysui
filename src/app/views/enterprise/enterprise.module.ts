import { DirectivesModule } from '../../directives/directives.module';

import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import { EnterpriseComponent } from './enterprise.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { EnterpriseRoutingModule } from './enterprise.routing';
import { AppBreadcrumbModule } from '@coreui/angular';
import { AddEnterpriseModalComponent } from './components/add-enterprise-modal/add-enterprise-modal.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { EditEnterpriseModalComponent } from './components/edit-enterprise-modal/edit-enterprise-modal.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PipesModule } from '../../pipes/pipes.module';
import {MatPaginatorIntl, MatPaginatorModule} from '@angular/material/paginator';
import {CustomMatPaginatorIntl} from '../paginator-es';




@NgModule({
  declarations: [EnterpriseComponent, AddEnterpriseModalComponent, EditEnterpriseModalComponent],
  imports: [
    PipesModule,
    TooltipModule,
    DirectivesModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    EnterpriseRoutingModule,
    AppBreadcrumbModule,
    ModalModule.forRoot(),
    MatPaginatorModule,

  ],
  entryComponents: [
    AddEnterpriseModalComponent
  ],
  exports: [
    AddEnterpriseModalComponent,
    EditEnterpriseModalComponent
  ],
  providers: [
    {
      provide: MatPaginatorIntl,
      useClass: CustomMatPaginatorIntl,
    },
    DatePipe,
  ]
})
export class EnterpriseModule { }
