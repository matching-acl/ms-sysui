export interface Enterprise {
  active?: boolean;
  code?: string;
  created?: string;
  updated?: string;
  description?: string;
  id?: number;
  message?: boolean;
  name?: string;
  status?: string;
}

export class EnterprisesAllUser {
  code?: string;
  name?: string;
}
