import { Component, OnInit, TemplateRef } from '@angular/core';
import { Template, Header } from './model/origins';
import {IsLoadingService} from '@service-work/is-loading';
import {AlertService} from '../../service/alert.service';
import {TemplateService} from './service/templates.service';
import {HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import {ENTERPRISE_CODE} from '../login/models/login';
import { PageEvent } from '@angular/material/paginator';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.scss']
})
export class TemplatesComponent implements OnInit {


  totalItems : number;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [1, 5, 10, 25];
  showFirstLastButtons = true;

  e_state = '';
  noData = false;
  noDataMessage = 'No se encontraron registros';
  dataSource: Template[];
  modalRef: BsModalRef;
  header: Header[];
  enterprise_code: string;

  constructor(
     private modalService: BsModalService,
    private loadService: IsLoadingService,
    private alert: AlertService,
    private templateService: TemplateService,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this.enterprise_code = localStorage.getItem(ENTERPRISE_CODE);
    this.getTemplates(this.pageIndex, this.pageSize);
  }

  downloadExcel(id : any) {
      this.templateService.downloadExcelById(this.enterprise_code, id).subscribe((data) => {
        if (data) {
        saveAs(data, 'Template.xls');
    }
    })
  }

   handlePageEvent(event: PageEvent) {
    this.totalItems = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.getTemplates(this.pageIndex, this.pageSize)
  }

  openModal(template: TemplateRef<any>, header: any) {
    this.header = header;
    this.modalRef = this.modalService.show(template);
    this.modalRef.setClass('modal-xs');
  }

  getRowValue(value: Header[]): string {

    let returnValue = '';
    value.forEach((element, index) => {
      returnValue += index === value.length - 1 ? element.name : element.name + ',' + ' ';
      // this.header = element;
    });
    return returnValue;
  }

  getTemplates( pageIndex? : number, pageSize? : number ) {
    this.templateService.getTotalItems(this.enterprise_code).subscribe(data => {
      this.totalItems = data.total_items;
      console.log(this.totalItems);
    });
    this.loadService.add(
      this.templateService.getTemplates(this.enterprise_code, pageIndex,pageSize).subscribe(data => {
        console.log(data);
          if (data && data.length > 0) {
            this.dataSource = data;
            this.dataSource.forEach((element) => {
              if (element.e_state === 'Listo') {
                this.e_state = element.e_state;
                console.log(element.e_state);
              }
            });
          } else {
            this.noData = true;
          }
        },
        (error: HttpErrorResponse) => {
          this.noData = true;
          this.noDataMessage = 'Error de servidor, favor inténtelo nuevamente.';
         })
    );
  }

  openAddTemplate() {
    this._router.navigate(['/templates/create']);
  }
  openEditTemplate(id) {
    this._router.navigate(['/templates/edit/' + id]);
  }

  openUpdateActiveTemplate(id: number, active: boolean) {
    this.loadService.add(
      this.templateService.updateActiveTemplate(id, active, this.enterprise_code).subscribe((data) => {

        if (data && data.status === '200') {
          setTimeout(() => {
            Swal.fire({
              icon: 'success',
              title: 'Correcto',
              text: 'Se ha actualizado el estado del template correctamente',
              timer: 3000,
              showCancelButton: false,
              showConfirmButton: false
            });
            this.getTemplates();
          }, 1500);
        } else {
                setTimeout(() => {
                    Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: 'No se ha podido actualizar el estado del template: ' + data.message,
                                timer: 3000,
                                showCancelButton: false,
                                showConfirmButton: false
                              });
                  }, 1500);
                }
              }, ( err ) => {
                setTimeout(() => {
                  Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'No se ha podido actualizar el estado del template',
                    timer: 3000,
                    showCancelButton: false,
                    showConfirmButton: false
                  });
                }, 1500);
              }));
  }
}
