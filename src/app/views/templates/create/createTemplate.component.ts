import { Component, ElementRef, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { origins } from '../model/origins';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { ViewChild } from '@angular/core';
import { TemplateService } from '../service/templates.service';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {ENTERPRISE_CODE, USER_ID} from '../../login/models/login';
import { saveAs } from 'file-saver';

@Component({
  templateUrl: './createTemplate.component.html',
  styleUrls: ['./createTemplate.component.scss'],
  styles: [
    `  :host >>> .tooltip-inner {
        background-color: #153f59;
        color: #fff;
      }
      :host >>> .tooltip.top .tooltip-arrow:before,
      :host >>> .tooltip.top .tooltip-arrow {
        border-top-color: #153f59;
      }`
  ]
})
export class CreateTemplateComponent implements OnInit {

  @ViewChild('file') fileInput: ElementRef;

  processed = false;
  newTemplate: FormGroup;
  fieldsLength: number;
  origins: origins[];
  templateFields: FormGroup;
  control: FormArray;
  touchedRows: any;
  textoConDelimitador: boolean;
  fileToUpload: File = null;
  testForm: string = 'Prueba';
  finalForm: boolean;
  isReady: boolean;
  orderTable: number = 0;
  status: boolean;
  isTitled: any;
  message  = `<b><i>00FECHA01012021REV03</i></b></br>01105100003455212341<br>01108500007894432513`;
  responseServer: boolean;
  responseMessage: string = '';
  fileKies: any[] = [];
  erroArray: any[] = [];
  hasKies: any;
  columsHeader: any[] = [];
  enterprise_code: string;
  user_id = localStorage.getItem(USER_ID);

  constructor(
    private formBuilder: FormBuilder,
    private templateService: TemplateService,
    private router: Router) {

  }

  ngOnInit(): void {
    this.enterprise_code = localStorage.getItem(ENTERPRISE_CODE);
    this.templateService.getOrigenes(this.enterprise_code).subscribe((data) => {
      console.log(data);
      this.origins = data;

    });
    this.status = false;
    this.finalForm = false;
    this.isTitled = false;
    this.textoConDelimitador = false;
    this.responseServer = false;
    this.hasKies = false;

    this.newTemplate = this.formBuilder.group({
      name: ['', Validators.required],
      status: [''],
      format: [null,Validators.required],
      separator: [''],
      file_name: ['', []],
      excel_name: ['', []]
    });

    this.touchedRows = [];

    this.createTemplateField();

    this.addRow();

  }

  downloadExcel() {
     this.templateService.downloadExcel().subscribe((data) => {
        if (data) {
        saveAs(data, 'Template.xls');
    }
    })
  }

  ngAfterOnInit() {
    this.control = this.templateFields.get('tableRows') as FormArray;
    this.fieldsLength = this.templateFields.get('tableRows').value.length;
  }

  createTemplateField() {
    this.templateFields = this.formBuilder.group({
      tableRows: this.formBuilder.array([])
    });
  }

  changeStatus(event) {
    if ( event.target.checked ) {
      this.isTitled = true;
    } else {
      this.isTitled = false;
    }
  }

  initiateForm(): FormGroup {
    return this.formBuilder.group({
      position: [this.orderTable],
      name: ['', Validators.required],
      length: ['', Validators.compose([Validators.required, Validators.min(1)])],
      code: ['']
    });
  }

  addRow() {
    this.orderTable = this.orderTable + 1;
    const control =  this.templateFields.get('tableRows') as FormArray;
    control.push(this.initiateForm());
  }

   importExcel(files: FileList) {
   
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
    this.newTemplate.controls['excel_name'].setValue(this.fileToUpload.name);
    this.templateService.uploadExcel(this.fileToUpload).subscribe(data => {
      console.log(data);
      this.orderTable = 0;
      this.removeTableRow();
      data.forEach(i => {
        this.addRowExcel(i.name, i.length);
      });
    })
  }

  addRowExcel(name?: any, longitud?: any) {
    
    console.log(name);
    this.orderTable = this.orderTable + 1
    const control = <FormArray>this.templateFields.controls['tableRows'];
    control.push(this.formBuilder.group({
      name: [name ? name : ''],
      length: [longitud ? longitud : ''],
      position: [this.orderTable],
      code : ['']
    }));
  }

  removeTableRow() {
    this.orderTable = this.orderTable;
    const control = <FormArray>this.templateFields.controls['tableRows'];
    control.clear();
  }

  deleteRow(index: number) {
    this.orderTable = this.orderTable - 1;
    const control =  this.templateFields.get('tableRows') as FormArray;
    control.removeAt(index);
    control.updateValueAndValidity();

  }



  editRow(group: FormGroup) {
    group.get('isEditable').setValue(true);
  }

  doneRow(group: FormGroup) {
    group.get('isEditable').setValue(false);
  }

  saveUserDetails() {
    console.log(this.templateFields.value);
  }

  get getFormControls() {
    const control = this.templateFields.get('tableRows') as FormArray;
    return control;
  }

 
  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.newTemplate.controls['file_name'].setValue(this.fileToUpload.name);
    console.log(this.fileToUpload);
  }

  clearFile() {
    this.fileInput.nativeElement.value = null;
    this.newTemplate.controls['file_name'].setValue(null);
  }

  populateRow(position, name, length) {
  console.log(name);
  console.log(length);
    
    const control =  this.templateFields.get('tableRows') as FormArray;
    control.push(this.reinitiateForm(position, name, length));
    control.markAsTouched();
  

  }

  reinitiateForm(position,name, length): FormGroup{

    return this.formBuilder.group({
      position: [position],
      name: [name, Validators.required],
      length: [length, Validators.compose([Validators.required, Validators.min(1)])],
      code: ['']
    });
  }

  hasName(o) {
    if (o == null) {
      return false;
    } else {
      return true;
    }
  }

  addRowWithEnter(event, currentRow) {
    const control = this.templateFields.get('tableRows') as FormArray;
    this.touchedRows = control.controls.map(row => row.value);
    let rows = this.touchedRows;
    let counter: number = 0;
    for(let i = 0; i < Object.keys(rows).length; i++){
      counter = i;
    }
    if (counter == currentRow) {
      this.addRow();
    }
    event.preventDefault();
  }

  disableEnter(event){
    event.preventDefault();
  }

  validarFormato() {
    const control = this.templateFields.get('tableRows') as FormArray;
    this.touchedRows = control.controls.map(row => row.value);

    let rows = this.touchedRows;
    let arr = [];
    let headers = [];
    for(let row of rows){
      let obj = {
        position: row.position,
        name: row.name,
        code: row.name,
        length: parseInt(row.length)
      }
      arr.push(obj);
    }

    if(this.fileToUpload == null){
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'Debe subir un archivo para validar su formato'
      })
    }
    else if(Object.keys(rows).length == 0){
      console.log('o')
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'Debe ingresar al menos un campo al detalle del formato'
      })
    }
    else{
      this.templateService.testFile(this.fileToUpload, arr, this.newTemplate.controls['separator'].value, this.isTitled, this.enterprise_code).subscribe(data => {
       console.log('asas',data.Data.Data);
        for (const i of data.Data.Data) {
          console.log(i.processed);
          if (i.processed === 1) {
            console.log('color negro');
            this.processed = true;
          } else {
            console.log('color rojo');
            this.processed = false;
          }
        }
        if(data.Data.StatusCode == '200'){

          this.finalForm = true;
          this.responseServer = true;
          this.responseMessage = '<i>El archivo de prueba cumple con el formato del template</i>';
        }
        else{
          this.finalForm = false;
          this.responseServer = true;
          this.responseMessage = '<i>El archivo contiene registros que <b>no cumplen con el formato definido</b>. Puede continuar con el proceso, pero es posible que la carga no se ejecute correctamente. Intente probar otro formato o utilizar otro archivo.</i>';
        }

        data.Data.Header.forEach((element) => {
          headers.push(element.code);
        });
        this.columsHeader = [];
        this.columsHeader = headers;

        console.log(headers);

        //console.log(rows)
        this.hasKies = true;
        this.erroArray = [];
        this.fileKies = [];
        for(let ob of data.Data.Data){
          let ar: any[] = [];
          headers.forEach((v, i) =>{
            Object.entries(ob.keys).forEach(
              ([key, value]) => {
                if(v == key.toUpperCase()){
                  //console.log(i +'-'+v+'-'+ value)
                  ar.push(value)
                }
              }
            );
          })
          this.erroArray.push(ob.linea);
          this.fileKies.push(ar);
          console.log(this.erroArray)
        }
      },
      error => {
        this.fileToUpload = null;
        this.clearFile();
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Ocurrió un error desconocido, por favor intente más tarde'
        })
      });
    }
  }

  onDrop(event: CdkDragDrop<string[]>) {

    const control = this.templateFields.get('tableRows') as FormArray;
    this.touchedRows = control.controls.map(row => row.value);

    let rows = this.touchedRows;

    
   
    moveItemInArray(rows, event.previousIndex, event.currentIndex);
    this.createTemplateField();
    rows.forEach((temp, idx) => {
      temp.position = idx + 1;
      
     
      this.populateRow(temp.position, temp.name, temp.length);
      
    });
    
    control.updateValueAndValidity();
  }

  onOptionsSelected(value:string){

    if(value == '2'){
      this.textoConDelimitador = true;
      this.newTemplate.controls['separator'].setValidators([Validators.required]);
      this.newTemplate.controls['separator'].updateValueAndValidity();
    }
    else{
      this.textoConDelimitador = false;
      this.newTemplate.controls['separator'].clearValidators();
      this.newTemplate.controls['separator'].updateValueAndValidity();
    }
  }


  callService(saveData){
    this.templateService.saveInterfaceFormat(saveData).subscribe((data) => {
      console.log(data)
      if(data.status == '200'){
        this.newTemplate.reset();
        this.templateFields.reset();
        Swal.fire(
          'Correcto!',
          'Se ha registrado el template correctamente',
          'success'
        );
        setTimeout(() => {
          Swal.close();
          this.router.navigateByUrl('/templates');
        }, 2000);
      }
      else{
       Swal.fire({
          icon: 'error',
          title: 'Error',
          text : 'No se ha podido registrar el template '+ ': ' + saveData.object.name
        });
      }
    },(err) => {
      console.log(err)
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'No se ha podido registrar el template'
      });
    });
  }

  guardar(){
    const control = this.templateFields.get('tableRows') as FormArray;
    this.touchedRows = control.controls.map(row => row.value);
    let rows = this.touchedRows;
    let arrayRows = [];

    for (let element in rows){
      arrayRows.push({
        position: rows[element].position,
        name: rows[element].name,
        length: parseInt(rows[element].length),
        code: rows[element].name,
      });
    }


    if (this.finalForm) {
      Swal.fire({
        title: '¿Desea dejar este formato en estado productivo?',
        text: 'El formato ingresado coincide con el archivo validado',
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí',
        cancelButtonText: 'No'

      }).then((result) => {
        if(result.isConfirmed){
          this.testForm = "Listo";
        }
        let saveData = {
          "model": "templates",
          "schema": this.enterprise_code,
          "id_name": "id",
          "object": {
              "name" : this.newTemplate.controls['name'].value,
              "tc_template_id" : this.newTemplate.controls['format'].value,
              "separator": this.newTemplate.controls['separator'].value,
              "separator_line": "null",
              "e_state" : this.testForm,
              "user_id" : this.user_id,
              "header" :  arrayRows
            }
        };
        this.callService(saveData);
      });
    }
    else{
      let saveData = {
        "model": "templates",
        "schema": this.enterprise_code,
        "id_name": "id",
        "object": {
          "name" : this.newTemplate.controls['name'].value,
          "tc_template_id" : this.newTemplate.controls['format'].value,
          "separator": this.newTemplate.controls['separator'].value,
          "separator_line": "null",
          "e_state" : this.testForm,
          "user_id" : this.user_id,
          "header" :  arrayRows
        }
      };
      this.callService(saveData);
    }


  }

  private capitalizeString(cadena: string) {
    if (cadena && cadena.length > 0) {
      return cadena.charAt(0).toUpperCase() + cadena.slice(1);
    } return cadena;
  }

}
