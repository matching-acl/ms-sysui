import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import Swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { interfaceSelect } from '../../../models/createInterface';
import { currentInterface } from '../../../models/interface';
import { interfaceDetail } from '../../../models/interfaceDetail';
import { TemplateService } from '../service/templates.service';
import { origins } from '../model/origins';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {ENTERPRISE_CODE, USER_ID} from '../../login/models/login';

@Component({
    templateUrl: './editTemplate.component.html',
    styleUrls: ['./editTemplate.component.scss'],
    styles: [
      `  :host >>> .tooltip-inner {
          background-color: #153f59;
          color: #fff;
        }
        :host >>> .tooltip.top .tooltip-arrow:before,
        :host >>> .tooltip.top .tooltip-arrow {
          border-top-color: #153f59;
        }`
    ]
  })

  export class editTemplateComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private templateService: TemplateService,
    private router: Router,
    private route: ActivatedRoute){
  }
  @ViewChild('file') fileInput: ElementRef;
  processed = false;
    editTemplate: FormGroup;
    TemplateFields: FormGroup;
    control: FormArray;
    touchedRows: any;
    interfacesSelect: interfaceSelect[];
    currentInterface: string = '';
    currentFormat: number;
    origins: origins[];
    interfaz: currentInterface;
    interfazSeleccionada: boolean;
    textoConDelimitador: boolean;
    detalleFormato: interfaceDetail[];
    interfaceId: number;
    fileToUpload: File = null;
    finalForm: boolean;
    testForm: string = "Prueba";
    id_url: number;
    sub: any;
    orderTable: number = 0;
    status: boolean;
    isTitled: any;
    message  = `<b><i>00FECHA01012021REV03</i></b></br>01105100003455212341<br>01108500007894432513`;
    responseServer: boolean;
    responseMessage: string = '';
    fileKies: any[] = [];
    erroArray: any[] = [];
    hasKies: any;
    columsHeader: any[] = [];
    headerss: any;
    enterprise_code: string;
    user_id = localStorage.getItem(USER_ID);

    ngOnInit(): void {
      this.enterprise_code = localStorage.getItem(ENTERPRISE_CODE);
      this.isTitled = false;
      this.responseServer = false;
      this.hasKies = false;

      this.sub = this.route.params.subscribe(params => {
        this.id_url = +params['id'];
      });

      this.finalForm = false;
      this.interfazSeleccionada = false;

      this.templateService.getOrigenes(this.enterprise_code).subscribe((datas) => {
        this.origins = datas;
      });

      this.templateService.getTemplate(this.id_url, this.enterprise_code).subscribe((data) => {

        this.interfaz = data[0];
        console.log(data[0])
        this.currentFormat = this.interfaz.tc_template_id;
        this.disableSeparator(this.currentFormat);
        this.editTemplate.controls['name'].setValue(this.interfaz.name);
        this.editTemplate.controls['separator'].setValue(this.interfaz.separator);
        this.editTemplate.controls['format'].setValue(this.currentFormat);
        this.status = data[0].is_titled;

        this.interfaceId = this.interfaz.id;
        this.editTemplate.controls['name'].updateValueAndValidity();
        this.editTemplate.controls['separator'].updateValueAndValidity();
        this.editTemplate.controls['format'].updateValueAndValidity();
        this.headerss = this.interfaz.header;
        let str: any = this.interfaz.header;
        for(let key in str){
          this.populateRow(str[key].position,str[key].name, str[key].length);
        }
      });



      this.editTemplate = this.formBuilder.group({
        name: ['',Validators.required],
        status: [''],
        format: ['',Validators.required],
        separator: [''],
        file_name: ['', []]
      });

      this.iniciarFormularioDetalle();


    }

    iniciarFormularioDetalle(){
      this.TemplateFields = this.formBuilder.group({
        tableRows: this.formBuilder.array([])
      });
    }

    ngAfterOnInit() {
      this.control = this.TemplateFields.get('tableRows') as FormArray;
    }

    initiateForm(): FormGroup {
      return this.formBuilder.group({
        position: [this.orderTable],
        name: ['', Validators.required],
        length: ['', Validators.compose([Validators.required, Validators.min(1)])],
        code: ['']
      });
    }

    changeStatus(event){
      if ( event.target.checked ) {
        this.isTitled = true;
      }
      else{
        this.isTitled = false;
      }
    }

    reinitiateForm(position,name, length): FormGroup{
      return this.formBuilder.group({
        position: [position],
        name: [name, Validators.required],
        length: [length, Validators.compose([Validators.required, Validators.min(1)])],
        code: [''],
      });
    }

    addRow() {
      const control =  this.TemplateFields.get('tableRows') as FormArray;
      control.push(this.initiateForm());
    }

    populateRow(position, name, length) {
      const control =  this.TemplateFields.get('tableRows') as FormArray;
      control.push(this.reinitiateForm(position, name, length));
      control.markAsTouched();
    }

    deleteRow(index: number) {
      const control =  this.TemplateFields.get('tableRows') as FormArray;
      control.removeAt(index);
    }

    editRow(group: FormGroup) {
      group.get('isEditable').setValue(true);
    }

    doneRow(group: FormGroup) {
      group.get('isEditable').setValue(false);
    }

    get getFormControls() {
      const control = this.TemplateFields.get('tableRows') as FormArray;
      return control;
    }


    onOptionsSelected(value:string){
      this.disableSeparator(parseInt(value));
    }



    disableSeparator(value:number){
      if(value == 2){
        this.textoConDelimitador = true;
        this.editTemplate.controls['separator'].setValidators([Validators.required]);
        this.editTemplate.controls['separator'].updateValueAndValidity();
        this.editTemplate.controls['format'].updateValueAndValidity();
        this.currentFormat = value;
      }
      else{
        this.textoConDelimitador = false;
        this.editTemplate.controls['separator'].clearValidators();
        this.editTemplate.controls['separator'].updateValueAndValidity();
        this.editTemplate.controls['format'].updateValueAndValidity();
        this.currentFormat = value;
      }
    }


    handleFileInput(files: FileList) {
      this.fileToUpload = files.item(0);
      this.editTemplate.controls['file_name'].setValue(this.fileToUpload.name);
    }

    clearFile(){
      this.fileInput.nativeElement.value = null;
      this.editTemplate.controls['file_name'].setValue(null);
    }


    hasName(o) {
      if(o == null){
        return false;
      }
      else{
        return true;
      }
    }

    validarFormato() {
      const control = this.TemplateFields.get('tableRows') as FormArray;
      this.touchedRows = control.controls.map(row => row.value);

      let rows = this.headerss;
      let arr = [];
      let headers = [];
      for(let row of rows){
        console.log(row)
        let obj = {
          position: row.position,
          name: row.name,
          code: row.code,
          length: parseInt(row.length)
        }
        arr.push(obj);

      }
      if(this.fileToUpload == null){
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Debe subir un archivo para validar su formato'
        })
      }
      else if(Object.keys(rows).length == 0){
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Debe ingresar al menos un campo al detalle del formato'
        })
      }
      else{
        this.templateService.testFile(this.fileToUpload, arr, this.editTemplate.controls['separator'].value, this.isTitled, this.enterprise_code).subscribe(data => {
           for (const i of data.Data.Data) {
          console.log(i.processed);
          if (i.processed === 1) {
            console.log('color negro');
            this.processed = true;
          } else {
            console.log('color rojo');
            this.processed = false;
          }
        }
            console.log(data)
            if(data.Data.StatusCode == '200'){
              this.finalForm = true;
              this.responseServer = true;
              this.responseMessage = '<i>El archivo de prueba cumple con el formato del template</i>';
            }
            else{
              this.finalForm = false;
              this.responseServer = true;
              this.responseMessage = '<i>El archivo contiene registros que <b>no cumplen con el formato definido</b>. Puede continuar con el proceso, pero es posible que la carga no se ejecute correctamente. Intente probar otro formato o utilizar otro archivo.</i>';
            }

            data.Data.Header.forEach((element) => {
              headers.push(element.code);
            });
            console.log(headers)
            this.columsHeader = [];
            this.columsHeader = headers;

            this.hasKies = true;
            this.erroArray = [];
            this.fileKies = [];
            for(let ob of data.Data.Data){
              let ar: any[] = [];
              headers.forEach((v, i) =>{
                Object.entries(ob.keys).forEach(([key, value]) => {
                    //console.log(value)
                    if(v == key.toUpperCase()){
                      //console.log(i +'-'+v+'-'+ value)
                      ar.push(value)
                    }
                  }
                );
              })
            this.erroArray.push(ob.linea);
            this.fileKies.push(ar);
            console.log(this.fileKies)
            }

        },
        error => {
          this.fileToUpload = null;
          this.clearFile();
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Ocurrió un error desconocido, por favor intente más tarde'
          })
        });
      }
    }

    addRowWithEnter(event, currentRow) {
      const control = this.TemplateFields.get('tableRows') as FormArray;
      this.touchedRows = control.controls.map(row => row.value);
      let rows = this.touchedRows;
      let counter: number = 0;
      for(let i = 0; i < Object.keys(rows).length; i++){
        counter = i;
      }
      if(counter == currentRow){
        this.addRow();
      }
      event.preventDefault();
    }

    disableEnter(event){
      event.preventDefault();
    }

    onDrop(event: CdkDragDrop<string[]>) {

      const control = this.TemplateFields.get('tableRows') as FormArray;
      this.touchedRows = control.controls.map(row => row.value);

      let rows = this.touchedRows;

      moveItemInArray(rows, event.previousIndex, event.currentIndex);
      this.iniciarFormularioDetalle();
      rows.forEach((temp, idx) => {
        temp.position = idx + 1;
        this.populateRow(temp.position, temp.name, temp.length);
      });
      control.updateValueAndValidity();
    }

    callService(saveData){
      this.templateService.updateInterfaceFormat(saveData).subscribe((data) => {

        if(data.status == '200'){
          this.editTemplate.reset();
          this.TemplateFields.reset();
          Swal.fire(
            'Correcto!',
            'Se ha actualizado el formato de interfaz correctamente',
            'success'
          )
          setTimeout(() => {
            Swal.close();
            this.router.navigateByUrl('/templates');
          }, 2000)
        }
        else{
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'No se ha podido actualizar el formato de la interfaz'
          })
        }
      },(err) => {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'No se ha podido actualizar el formato de la interfaz'
        });
      });
    }



    actualizarInterfaz(){
      const control = this.TemplateFields.get('tableRows') as FormArray;
      this.touchedRows = control.controls.map(row => row.value);
      let rows = this.touchedRows;
      let sep: string = '';
      let arrayRows = [];

      for (let element in rows){
        arrayRows.push({
          position: rows[element].position,
          name: rows[element].name,
          length: parseInt(rows[element].length),
          code: rows[element].name
        });
      }

      if(this.currentFormat == 2){
        sep = this.editTemplate.controls['separator'].value;
      }

      if (this.finalForm) {
        Swal.fire({
          title: '¿Desea dejar este formato en estado productivo?',
          text: 'El formato ingresado coincide con el archivo validado',
          icon: 'info',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Sí',
          cancelButtonText: 'No'

        }).then((result) => {
          if(result.isConfirmed){
            this.testForm = "Listo";
          }
          let saveData = {
            "model": "templates",
            "schema": this.enterprise_code,
            "id_name": "id",
            "object_new": {
              "id": this.interfaceId,
              "name" : this.editTemplate.controls['name'].value,
              "tc_template_id" : this.currentFormat,
              "separator": sep,
              "separator_line": "null",
              "e_state" : this.testForm,
              "user_id" : this.user_id,
              "header" :  arrayRows
            }
          };
          this.callService(saveData);
        });
      }
      else{
        let saveData = {
          "model": "templates",
          "schema": this.enterprise_code,
          "id_name": "id",
          "object_new": {
            "id": this.interfaceId,
            "name" : this.editTemplate.controls['name'].value,
            "tc_template_id" : this.currentFormat,
            "separator": sep,
            "separator_line": "null",
            "e_state" : this.testForm,
            "user_id" : this.user_id,
            "header" :  arrayRows
          }
        }
        this.callService(saveData);
      }
    }

    private capitalizeString(cadena: string) {
      if (cadena && cadena.length > 0) {
        return cadena.charAt(0).toUpperCase() + cadena.slice(1);
      }
      return cadena;
    }

}
