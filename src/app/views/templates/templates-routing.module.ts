import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateTemplateComponent } from './create/createTemplate.component';
import { editTemplateComponent } from './edit/editTemplate.component';
import { TemplatesComponent } from './templates.component';
import {AuthGuardService} from '../login/service/auth-guard.service';

const routes: Routes = [
  { path: '', component: TemplatesComponent, canActivate: [AuthGuardService] },
  { path: 'create', component: CreateTemplateComponent, data: { title: 'Generar formato Template'}, canActivate: [AuthGuardService]},
  { path: 'edit/:id', component: editTemplateComponent, data: { title: 'Modificar formato Template'}, canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TemplatesRoutingModule { }
