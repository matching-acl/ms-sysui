import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { TemplatesRoutingModule } from './templates-routing.module';
import { TemplatesComponent } from './templates.component';
import { editTemplateComponent } from './edit/editTemplate.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DragDropModule } from "@angular/cdk/drag-drop";
import {CreateTemplateComponent} from './create/createTemplate.component';
import {ExtendedModule} from '@angular/flex-layout';
import { PipesModule } from '../../pipes/pipes.module';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalHeaderComponent } from './modal-header/modal-header.component';
import { DirectivesModule } from '../../directives/directives.module';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';
import { CustomMatPaginatorIntl } from '../paginator-es';

@NgModule({
  declarations: [ TemplatesComponent, CreateTemplateComponent, editTemplateComponent, ModalHeaderComponent  ],
  imports: [
    MatPaginatorModule,
    PipesModule,
    TooltipModule,
    CommonModule,
    TemplatesRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    DragDropModule,
    ExtendedModule,
    DirectivesModule,
    ModalModule.forRoot()
  ],
   providers: [
    {
      provide: MatPaginatorIntl,
      useClass: CustomMatPaginatorIntl,
    },
    DatePipe,
  ]
 
})
export class TemplatesModule { }
