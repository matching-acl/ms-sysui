export interface origins {
    tc_origin: String;
}

export class Template {
  id?: number;
  name?: string;
  tc_template_id?: number;
  tc_template_name?: string;
  separator?: string;
  separator_line?: string;
  e_state?: string;
  active?: boolean;
  created?: string;
  updated?: string;
  first_name?: string;
  last_name?: string;
  username?: string;
  header?:  Header[];
}

export class Header {
  name?: string;
  length?: number;
  isEditable?: boolean;
}
