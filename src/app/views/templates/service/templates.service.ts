import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {environment} from '../../../../environments/environment.prod';
import { catchError } from 'rxjs/operators';
import {Template} from '../model/origins';


@Injectable({
    providedIn: 'root'
})

export class TemplateService {

  constructor(private http: HttpClient) {}


    getOrigenes(enterprise_code: string): Observable<any> {

        let header = new HttpHeaders();
        header = header.set('Content-Type', 'application/json');
        header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');

        const pr = {'is_enum': '0', 'model': 'tc_templates', 'schema': enterprise_code};

        return this.http.post(environment.pythonApiUrl + 'read', pr, {headers: header});

    }


    saveInterfaceFormat(data): Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Content-Type', 'application/json');
        header = header.set('Authorization',  'Bearer md53b2b0eef\'Authorization\'65b780845');

        return this.http.post(environment.pythonApiUrl + 'create', data, {headers: header});
    }

    getTemplate(id, enterprise_code: string): Observable<any> {
        let header = new HttpHeaders();
        const filter1 = enterprise_code + '.templates.id';
        header = header.set('Content-Type', 'application/json');
        header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
        const pr = {'is_enum': '0', 'model': 'templates', 'schema': enterprise_code, 'filters' : {[filter1] : id}};

        return this.http.post(environment.pythonApiUrl + 'read', pr, {headers: header});
    }


    getInterfaceByName(data: any): Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Content-Type', 'application/json');
        header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');

        return this.http.post(environment.pythonApiUrl + 'read', data, {headers: header});
    }

    updateInterfaceFormat(data): Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Content-Type', 'application/json');
        header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');

        return this.http.post(environment.pythonApiUrl + 'update', data, {headers: header});
    }

    updateActiveTemplate(id: number, active: boolean, enterprise_code: string): Observable<any> {
        let header = new HttpHeaders();
        header = header.set('Content-Type', 'application/json');
        header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');

        const data = {
          'model': 'templates',
          'schema': enterprise_code,
          'id_name': 'id',
          'object_new': {
            'id' : id,
            'active' : active
          }
        };

        return this.http.post(environment.pythonApiUrl + 'update', data, {headers: header});
    }


    testFile(fileToUpload: File, formato, separator, isTitled, enterprise_code: string): Observable<any> {

        let header = new HttpHeaders();
        header = header.set('responseType', 'text/html; charset=utf-8');
        const formData: FormData = new FormData();

        let format = JSON.stringify(formato);
        format = format.replaceAll('true', '"true"');

        console.log(format);

        formData.append('separator', separator);
        formData.append('header', format);
        formData.append('schema', enterprise_code);
        formData.append('model', 'templates');
        formData.append('is_titled', isTitled);
        formData.append('file', fileToUpload);


        return this.http.post<any>(environment.goApiUrl + 'validate_file', formData).pipe(
            catchError(this.erroHandler)
        );
    }
    uploadExcel(fileToUpload: File): Observable<any> {

        let header = new HttpHeaders();
        header = header.set('responseType', 'text/html; charset=utf-8');
        const formData: FormData = new FormData();
        formData.append('file', fileToUpload);


        return this.http.post<any>(environment.pythonApiUrl + 'upload_template_excel', formData).pipe(
            catchError(this.erroHandler)
        );
    }

    erroHandler(error: HttpErrorResponse) {
        console.log(error.message || 'server Error');
        return throwError(error.message || 'server Error');
    }

    getTemplates(enterprise_code: string, number_page? : number, size_page? : number): Observable<Template[]> {
      let header = new HttpHeaders();
      header = header.set('Content-Type', 'application/json');
      header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
      let params = new HttpParams();
      params = params.set('is_enum', '0');
      params = params.set('model', 'tc_origin');
      params = params.set('schema', enterprise_code);

        const pr = {
            'number_page': number_page ? number_page + 1 : 1,
            'size_page' : size_page ? size_page : 10,
            'is_enum': '0',
            'model': 'templates',
            'schema': enterprise_code
        };

      return this.http.post<Template[]>(environment.pythonApiUrl + 'read', pr, {headers: header});

    }
    
    getTotalItems(enterprise_code: string): Observable<any> {
      let header = new HttpHeaders();
      header = header.set('Content-Type', 'application/json');
      header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
      const pr = {
        'model':
          'templates', 'schema': enterprise_code
      };

      return this.http.post(environment.pythonApiUrl + 'total_items', pr, {headers: header});

  }
  
  downloadExcel(): Observable<any>{
    let header = new HttpHeaders();
    header = header.set("Content-Type", "application/json");
    header = header.set("Authorization", 'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');

    return this.http.get(environment.pythonApiUrl + 'download_empty_template_excel', { headers: header, responseType: 'blob' });
  }


  downloadExcelById(code : any, id : any): Observable<any>{
    let header = new HttpHeaders();
    header = header.set("Content-Type", "application/json");
    header = header.set("Authorization", 'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
     const pr = {'id': id, 'schema': code};
    return this.http.post(environment.pythonApiUrl + 'template_to_excel', pr,  { headers: header, responseType: 'blob' });
  }
}
