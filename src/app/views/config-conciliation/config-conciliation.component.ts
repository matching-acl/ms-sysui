import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {IsLoadingService} from '@service-work/is-loading';
import {AlertService} from '../../service/alert.service';
import {HttpErrorResponse} from '@angular/common/http';
import {ConfConciliation} from './model/conf-conciliation';
import Swal from 'sweetalert2';
import {ConciliationService} from './services/conciliation.service';
import {ENTERPRISE_CODE} from '../login/models/login';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-config-conciliation',
  templateUrl: './config-conciliation.component.html',
  styleUrls: ['./config-conciliation.component.scss']
})
export class ConfigConciliationComponent implements OnInit {

  totalItems : number;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [1, 5, 10, 25];
  showFirstLastButtons = true;

  title = 'Config. Conciliaciones';
  noData = false;
  noDataMessage = 'No se encontraron registros';
  dataSource: ConfConciliation[];
  enterprise_code: string;
  constructor(
    private _router: Router,
    private loadService: IsLoadingService,
    private alert: AlertService,
    private conciliationService: ConciliationService
  ) { }

  ngOnInit(): void {
    this.enterprise_code = localStorage.getItem(ENTERPRISE_CODE);
    this.getConfConciliation();
  }

  handlePageEvent(event: PageEvent) {
    this.totalItems = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.getConfConciliation(this.pageIndex, this.pageSize);
    
  }

  getConfConciliation(pageIndex? :number, pageSize?:number) {

    this.conciliationService.getTotalItems(this.enterprise_code).subscribe(data => {
      this.totalItems = data.total_items;
      console.log(this.totalItems);
    });

    this.loadService.add(
      this.conciliationService.getConfConciliation(this.enterprise_code, pageIndex, pageSize).subscribe(data => {
          console.log({data: data});
          this.dataSource = data;
          this.noData = this.dataSource && this.dataSource.length === 0;
        },
        (error: HttpErrorResponse) => {
          console.log(error);
          this.noData = true;
          this.noDataMessage = 'Error de servidor, favor inténtelo nuevamente.';
        })

    );
  }

  openAddConfConciliation() {
    this._router.navigate(['/config-conciliation/create']);
  }

  openEditConfigConciliation(id: number) {
    this._router.navigate(['/config-conciliation/edit/' + id]);
  }
  openTestConciliation(id: number) {
    this._router.navigate(['/config-conciliation/test/' + id]);
  }

  openUpdateActiveConfigConciliation(id: number, active: boolean) {
    this.loadService.add(
      this.conciliationService.updateActiveConfConciliation(id, active, this.enterprise_code).subscribe((data) => {
        console.log({data: data});
        if (data && data.status === '200') {
          setTimeout(() => {
            Swal.fire({
              icon: 'success',
              title: 'Correcto',
              text: 'Se ha actualizado el estado de la configuración de conciliaciones correctamente',
              timer: 3000,
              showCancelButton: false,
              showConfirmButton: false
            });
            this.getConfConciliation();
          }, 1500);
        } else {
          setTimeout(() => {
            Swal.fire({
              icon: 'error',
              title: 'Error',
              text: 'No se ha podido actualizar el estado de la configuración de conciliaciones: ' + data.message,
              timer: 3000,
              showCancelButton: false,
              showConfirmButton: false
            });
          }, 1500);
        }
      }, (err) => {
        setTimeout(() => {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'No se ha podido actualizar el estado de la configuración de conciliaciones',
            timer: 3000,
            showCancelButton: false,
            showConfirmButton: false
          });
        }, 1500);
      }
      ));
  }

}
