import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from '../../../../environments/environment.prod';
import {Observable} from 'rxjs';
import {ConfConciliation} from '../model/conf-conciliation';

@Injectable({
  providedIn: 'root'
})
export class ConciliationService {

  constructor(private http: HttpClient) { }

  private baseUrl = environment.pythonApiUrl;

  getTotalItems(enterprise_code: string): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
    const pr = {'model': 'conf_conciliation', 'schema': enterprise_code};

    return this.http.post(environment.pythonApiUrl + 'total_items', pr, {headers: header});

  }

   
   getTemplatesOutByInterface(enterprise_code: string, id : number): Observable<any> {
      let header = new HttpHeaders();
      header = header.set('Content-Type', 'application/json');
     header = header.set('Authorization', 'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
     
     const pr = {
          
          'model': 'outgoings_templates',
          'schema': enterprise_code,
          'filters': {
            'interface_id': id,
            'active' : true
            }
        };

      return this.http.post(environment.pythonApiUrl + 'read',pr, {headers: header});

    }

  //  getOuts(enterprise_code: string, pageIndex? : number, pageSize? : number): Observable<any> {
  //   let header = new HttpHeaders();
  //   header = header.set('Content-Type', 'application/json');
  //   header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
  //     const pr = {
  //         'number_page': pageIndex ? pageIndex + 1 : 1,
  //         'size_page' : pageSize ? pageSize : 10,
  //         'model': 'outgoings',
  //         'schema': enterprise_code
  //     };

  //   return this.http.post(environment.pythonApiUrl + 'read',pr, {headers: header});

  // }

  getConfConciliation(enterprise_code: string, number_page? : number, size_page? :number ): Observable<ConfConciliation[]> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization', 'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
    let params = new HttpParams();
    params = params.set('is_enum', '0');
    params = params.set('model', 'conf_conciliation');
    params = params.set('schema', enterprise_code);

    const pr = {
      'number_page': number_page ? number_page + 1 : 1,
      'size_page' : size_page ? size_page : 10,
      'model': 'conf_conciliation',
      'schema': enterprise_code
    };

    return this.http.post<ConfConciliation[]>(environment.pythonApiUrl + 'read', pr, {headers: header});

  }

  updateActiveConfConciliation(id: number, active: boolean, enterprise_code: string): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization', 'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');

    const data = {
      'model': 'conf_conciliation',
      'schema': enterprise_code,
      'id_name': 'id',
      'object_new': {
        'id': id,
        'active': active
      }
    };
    return this.http.post(environment.pythonApiUrl + 'update', data, {headers: header});
  }

  getInterfaces(enterprise_code: string): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');

    
    const filters = {"active":true}
    const pr = {
      filters: filters,
      'is_enum': '0',
      'model': 'interfaces',
      'schema': enterprise_code
    };

    return this.http.post(this.baseUrl + 'read', pr, {headers: header});

  }

  getOuts(enterprise_code: string, pageIndex? : number, pageSize? : number): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization', 'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
    const filters = {"active":true}
    const pr = {
          'filters' : filters,
          'number_page': pageIndex ? pageIndex + 1 : 1,
          'size_page' : pageSize ? pageSize : 10,
          'model': 'outgoings',
          'schema': enterprise_code
      };

    return this.http.post(environment.pythonApiUrl + 'read',pr, {headers: header});

  }

  getTemplates(enterprise_code: string): Observable<any> {

    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');

    const pr = {'is_enum': '0', 'model': 'templates', 'schema': enterprise_code};

    return this.http.post(environment.pythonApiUrl + 'read', pr, {headers: header});

  }


  getDataType(enterprise_code: string): Observable<any> {
     let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');

    const pr = {'is_enum': '0', 'model': 'tc_types', 'schema': enterprise_code};

    return this.http.post(this.baseUrl + 'read', pr, {headers: header});
  }

  create(body: any): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');

    return this.http.post(this.baseUrl + 'create', body, {headers: header});
  }


  getInterfaceById(id: number, enterprise_code: string): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization', 'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');

    const pr = {'model': 'conf_conciliation', 'schema': enterprise_code, 'id_name': 'id', 'id' : id};

    return this.http.post<ConfConciliation[]>(environment.pythonApiUrl + 'get', pr, {headers: header});
  }

  updateInterface(data: any): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');

    return this.http.post(this.baseUrl + 'update', data, {headers: header});
  }

  getInterfacesById(id: number, enterprise_code: string ): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');

    const pr = {'schema' : enterprise_code, 'model' : 'interfaces', 'id_name' : 'id', 'id' : id};

    return this.http.post(environment.pythonApiUrl + 'get', pr, {headers: header});
  }


  testConciliation(fileToUpload: File, fileToUpload2: File, kies: any, interfaces: any): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload);
    formData.append('file2', fileToUpload2);
    const intr = JSON.stringify(interfaces);
    formData.append('interfaces', intr);
    const k = JSON.stringify(kies);
    formData.append('keys', k);

    return this.http.post(environment.goApiUrl + 'validate_conciliation', formData);
  }

}
