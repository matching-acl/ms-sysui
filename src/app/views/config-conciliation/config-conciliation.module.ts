import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ConfigConciliationComponent } from './config-conciliation.component';
import { ConfigConciliationAddComponent } from './components/config-conciliation-add/config-conciliation-add.component';
import { ConfigConciliationEditComponent } from './components/config-conciliation-edit/config-conciliation-edit.component';
import {ConfigConciliationRoutingModule} from './config-conciliation.routing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {AppBreadcrumbModule} from '@coreui/angular';
import {QuartzCronModule} from '@sbzen/ng-cron';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import { ConfigConciliationTestComponent } from './components/config-conciliation-test/config-conciliation-test.component';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';
import { CustomMatPaginatorIntl } from '../paginator-es';
import { PipesModule } from '../../pipes/pipes.module';


@NgModule({
  declarations: [
    ConfigConciliationComponent,
    ConfigConciliationAddComponent,
    ConfigConciliationEditComponent,
    ConfigConciliationTestComponent],
  imports: [
    MatPaginatorModule,
    TabsModule,
    PipesModule,
    CommonModule,
    ConfigConciliationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    AppBreadcrumbModule,
    QuartzCronModule,
    ModalModule.forRoot(),
    TooltipModule
  ],
   providers: [
    {
      provide: MatPaginatorIntl,
      useClass: CustomMatPaginatorIntl,
    },
    DatePipe,
  ]
})
export class ConfigConciliationModule { }
