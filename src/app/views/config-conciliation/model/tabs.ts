export interface Tabs {
  title: string;
  content: string;
  active? : boolean,
    removable: boolean;
    disabled: boolean;
    
}