export class ConfConciliation {
  id: number;
  name?: string;
  code?: string;
  created?: string;
  updated?: string;
  active?: boolean;
  cron_time?: string;
  cron_time_text?: string;
  interfaces?: Interface [];
  keys: Tabs [];
  user_id?: number;
  first_name?: string;
  last_name?: string;
  username?: string;
}

export class Interface {
  interfaceId: string;
  name?: string;
  code?: string;
}

export class Tabs {
  keys: Key[];
}

export class Key {
  key?: string;
  type?: string;
  code?: string;
}
