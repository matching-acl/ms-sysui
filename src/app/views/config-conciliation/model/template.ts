
export interface DataType {
  id: number;
  code: string;
  name: string;
  created: string;
  updated: string;
  active: boolean;
}

export interface Template {
  id?: number;
  name?: string;
  code?: string;
  template_id?: number;
  template_name?: string;
  template_code?: string;
  template_header?: Header[];
  e_origin?: string;
  url?: string;
  username?: string;
  password?: string;
  cron_time?: string;
  cron_time_text?: string;
  created?: string;
  updated?: string;
  active?: boolean;
  user_id?: number;
  first_name?: string;
  last_name?: string;
}

export interface Header {
  name?: string;
  length?: number;
  isEditable?: boolean;
  code?: string;
}

export interface Interface {
  id?: number;
  name?: string;
  template_header?: Header[];
  code?: string;
}

export interface InterfaceRequest{
  interfaceId: number;
  name: string;
  code: string;
}
