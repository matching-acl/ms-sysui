import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import {Router} from '@angular/router';
import { ConciliationService } from '../../services/conciliation.service';
import { Template, Interface, DataType } from '../../model/template';
import { Tabs } from '../../model/tabs';
import Swal from 'sweetalert2';
import { ENTERPRISE_CODE, USER_ID } from '../../../login/models/login';
import { OutService } from '../../../out/service/out.service';
import { IsLoadingService } from '@service-work/is-loading';

@Component({
  selector: 'app-config-conciliation-add',
  templateUrl: './config-conciliation-add.component.html',
  styleUrls: ['./config-conciliation-add.component.scss']
})
export class ConfigConciliationAddComponent implements OnInit {
  

  dataValidate: any = [];
  activeButton = true;

  activeSelect = true;
  saveActive = true;
  interface_id: number;
  dinamicData = {};
  enterprise_code: string;
  user_id: any;
  template: Template[];
  selectedItems: Template[] = [];
  form: FormGroup;
  newForm: FormGroup;
  cronValue = '0 0 7 ? * * *';
  interfaces: Interface[];
  DataType: DataType[];
  dataSource = [];
  tabs: Tabs[] = [{
      title: `Campo 1`,
      content: '',
      active : true,
      disabled: false,
      removable: false
  }];
  out_validated = 1;
  outgoings_templates = [];
  templateOut = [];
  typeConciliation = [
    {
      name: 'Conciliados',
      id : 1
    },
    {
      name: 'No conciliados',
      id : 0
    },
  ]
  addNewTab(): void {
    const newTabIndex = this.tabs.length + 1;
    const title = `Campo ${newTabIndex}`;
    this.tabs.push({
      title,
      content: `Dynamic content ${newTabIndex}`,
      disabled: false,
      removable: false
    });
    this.dinamicData[title] = {
      data: [],
      decimal : false,
     };
    this.setDynamicData();
  }

  removeTabHandler(tab: any): void {
    console.log(parseInt(tab.title.match(/\d/g).join('')))
    
    delete this.dinamicData[tab.title];
    this.tabs.splice(this.tabs.indexOf(tab), 1);
    console.log(this.tabs)

    Object.entries(this.form.value).forEach((value, key) => {
      if(value[0].includes(tab.title)){
        this.form.removeControl(value[0]);
        this.form.updateValueAndValidity();
      }
    });

    this.tabs.forEach((value, key) => {
      let numCampo: number = +key+1;
      let title = `Campo ${numCampo}`;
      let content = `Dynamic content ${numCampo}`;
      value.title = title;
      value.content = content;
    })
    this.setDynamicData();
  }

  constructor(
    private fb: FormBuilder,
    private conciliaService: ConciliationService,
    private router: Router,
    private outService: OutService,
    private loading : IsLoadingService) {
    this.dinamicData[this.tabs[0].title] = {
      data: [],
      decimal: false,
    };
  }

  ngOnInit(): void {
    console.log(this.activeButton);
    this.enterprise_code = localStorage.getItem(ENTERPRISE_CODE);
    this.user_id = localStorage.getItem(USER_ID);
    this.createForm();
    this.createNewForm();
    this.getInterfaces();
    this.getDataTypes();
    this.getOuts();
  }

  outChange() {
    if (this.form.get('out').value != '') {
      
      this.activeSelect = false;
      console.log(this.activeSelect);
    } else {
       this.activeSelect = true;
    }
  }

 

   getOuts() {
    this.loading.add(
      this.conciliaService.getOuts(this.enterprise_code ).subscribe(data => {
        this.dataSource = data;
        console.log(this.dataSource);
     })
    )
  }

  getInterfaces(){
    this.conciliaService.getInterfaces(this.enterprise_code).subscribe((data) => {
      this.interfaces = data;
    });
  }

  getDataTypes(){
    this.conciliaService.getDataType(this.enterprise_code).subscribe((data) => {
      this.DataType = data;
    });
  }

  onChangeDato(e : any, tabz : any) {
    this.dataValidate.push(e);
    console.log(this.dataValidate);

    if ( this.dataValidate.length >= 4 && this.selectedItems.length >= 2) {
      this.activeButton = false;
    } else {
      this.activeButton = true;
    }
    
  }
  onChangeTipoDato(e : any, tabz : any) {
    this.dataValidate.push(e);
    console.log(this.dataValidate);
    if (this.dataValidate.length >= 4 && this.selectedItems.length >= 2) {
      this.activeButton = false;
    }else {
      this.activeButton = true;
    }
    
  }

  onKey(value: any, tabz: string, item: any, index: number) {
    
    const id = value.target.selectedOptions[0].id;
    const code = `${tabz}.${item.name}.typeCode`;
    const formControlValue = this.DataType.find((x) => x.id === +id).code;
    this.form.setControl(code, new FormControl(formControlValue));

    if (+id === 78) {
      this.dinamicData[tabz].data[index].decimal = true;
    } else {
      this.dinamicData[tabz].data[index].decimal = false;
    }
  }

  addCode(val: any, value: any, tabz: string, item: any, index: number) {
    const id = item.id;
    const formControlValue = this.interfaces.find((x) => x.id === +id);
    const code = `${tabz}.${item.name}.key`;
    const interfaceCode = `${tabz}.${item.name}.interfaceCode`;

    const codes = formControlValue.template_header.find((x) => x.name === val).code;
    const idInterfaz = this.interfaces.find((x) => x.id === +id).code;
    this.form.setControl(code, new FormControl(codes));
    this.form.setControl(interfaceCode, new FormControl(idInterfaz));

  }

  onInterfaceSelected(value: any) {
    const id = +value;
    this.interface_id = id;
    
    if (this.selectedItems.find((x) => x.id === +id)) {
      // this.form.controls['interface_name'].setValue(this.selectedItems.map((x) => x.id));
      return;
    } 
    this.selectedItems.push(this.interfaces.find((x) => x.id === +id));
    this.setDynamicData();
    
    let name = this.selectedItems[this.selectedItems.length -1]
    
    // this.addInterface(name.name, name.id);
    // this.getTemplatesOutByInterface(id);
  
  }

  // getTemplatesOutByInterface(id : number) {
  //   this.loading.add(
  //     this.conciliaService.getTemplatesOutByInterface(this.enterprise_code, id).subscribe(data => {
  //       console.log('template de salida por interface',data);
  //       this.templateOut[id] = data;
  //       console.log(this.templateOut);
       
  //    })
  //   )
  // }

  touched(e: any) {
    //  this.getTemplatesOutByInterface(e);
  }

  // reset() {
  //   this.form.get('out').setValue('');
  //   let form = this.form.value;
  //   this.templateOut = []
   
  //   for (const i of form.interfaces) {
  //     i.templateOut = '';
      
  //   }

  // }


  resetkey() {
    this.dataValidate = [];
    this.activeButton = true;
    this.createForm();
    this.removeInter();
    this.selectedItems = [];
    this.interfaces = [];
    this.tabs = [{
      title: `Campo 1`,
      content: '',
      active : true,
      disabled: false,
      removable: false
    }];
    this.dinamicData[this.tabs[0].title] = {
      data: [],
      decimal: false,
    };
    // this.setDynamicData();
    this.form.get('interface_name').setValue(null);
    this.getInterfaces();
  }

  setDynamicData() {
   console.log(this.dinamicData)
   for (const key in this.dinamicData) {
      if (Object.prototype.hasOwnProperty.call(this.dinamicData, key)) {
        const inter = this.selectedItems.filter((value) => {
          let xName;
          if (this.dinamicData[key]) {
             xName = this.dinamicData[key].data.find((x) => x.name === value.name);
          }
          //console.log(xName);
          if (!xName) {
              this.form.setControl(key + '.' + value.name + '.name', new FormControl());
              this.form.setControl(key + '.' + value.name + '.type', new FormControl());
              this.form.setControl(key + '.' + value.name + '.typeCode', new FormControl());
              this.form.setControl(key + '.' + value.name + '.precision', new FormControl());
              this.form.setControl(key + '.' + value.name + '.key', new FormControl());
            return  {name: value.name, template_header : value.template_header};
          }
        });
        this.dinamicData[key].data = [...this.dinamicData[key].data, ...inter];
        
      }
    }
   
  }

  removeInterfaces(id: number) {
    console.log(this.dinamicData);
    Object.entries(this.dinamicData).forEach((value, key) => {
      Object.entries(value).forEach((v, k) => {
        if (typeof v[1] === 'object') {
          Object.entries(v[1]).forEach((vl, cl) => {
          console.log(vl[1]);
            if (typeof vl[1] === 'object') {

              vl[1].forEach((vlr, ky) => {
                if (vlr.id === id) {
                  console.log('la interfaz id ' + id + ' se encuentra presente en la llave');
                }
              });
            }
          });
        }
      });
    });

    this.selectedItems = this.selectedItems.filter((x) => x.id !== id);
    if (this.selectedItems.length === 0) {
      this.form.get('interface_name').setValue('');
    }
  }



  createForm() {
    
    this.form = this.fb.group({
      out: [''],
      typeConciliation : [0],
      interfaces: this.fb.array([]),
      cronForm: [this.cronValue, []],
      conciliation_name: [null, [Validators.required, Validators.minLength(3)]],
      interface_name: [null, [Validators.required]],
    });
  }
  createNewForm() {
    
    this.newForm = this.fb.group({
      cronForm: [this.cronValue, []],
      conciliation_name: [null, [Validators.required, Validators.minLength(3)]],
     
    });
  }

  get getInterface() {
    return this.form.get('interfaces') as FormArray;
  }

  // addInterface(name?: string, id? : number) {
  //   console.log(name);
  //   const control = <FormArray>this.form.controls['interfaces'];
  //   control.push(this.fb.group({
  //     name: [name ? name : ''],
  //     id: [id ? id : ''],
  //     templateOut : ['',[Validators.required]]
  //   }));
  // }

  removeInter() {
    const control = <FormArray>this.form.controls['interfaces'];
    control.clear();
  }
 








  get conciliationNameNotValid() {
    return this.newForm.get('conciliation_name').invalid &&
      this.newForm.get('conciliation_name').touched;
  }

  get conciliationNameValid() {
    return this.newForm.get('conciliation_name').valid &&
      this.newForm.get('conciliation_name').touched;
  }


  handelSave() {
    const obj = [];
    const form = this.form.value;
    console.log(form);
   for (const key in form) {

     if (Object.prototype.hasOwnProperty.call(form, key)) {
       const element = form[key];
       const j = key.split('.')[1];
      if (j) {
        const k = key.split('.');
        if (obj[k[0]]) {
          const l = obj[k[0]];
          if (l[k[1]]) {
            l[k[1]][k[2]] = element;
          } else {
            const x = k[2];
            l[k[1]] = {[x] : element};
          }
        } else {
          const result = [];
          result[k[1]] = {[k[2]] : element};
          obj[k[0]] = result;
        }
      }

     }
    }
    const request = [];
    for (const key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        const element = obj[key];
        const arreglo = [];
        for (const k in element) {
          if (Object.prototype.hasOwnProperty.call(element, k)) {
            const el = element[k];

            arreglo.push(el);
          }
        }
        request.push(arreglo);
      }
    }

    

    const interfaces: any[] = [];

    this.selectedItems.forEach((value, key) => {
      const inter = {
        'interfaceId' : value.id,
        'name' : value.name ,
        'code' : value.code};
      interfaces.push(inter);

    });

    console.log(interfaces);


    this.form.patchValue({'interface_name': request});


    
    // this.outgoings_templates = [];
    //   for (const i of form.interfaces) {
    //       const obj = {
    //       interface_id: +i.id,
    //       outgoing_template_id : +i.templateOut
    //     }
    //     this.outgoings_templates.push(obj);
        
    //   }
      
    //   console.log(form.out);
    //   const out = form['out'];
      
    //   const templateOut = form['interfaces'][0]['templateOut'];
      
    //   if((out && out != null && out != '') && (templateOut && templateOut != null && templateOut != '')){
    //     this.out_validated = 1	
    //   }else{
    //     this.out_validated = -1
    //   }
      
    const newForm = this.newForm.value;

    const saveData = {
      'schema' : this.enterprise_code,
      'model' : 'conf_conciliation',
      'id_name': 'id',
      'object': {
        'name' : newForm.conciliation_name,
        'user_id' : this.user_id,
        'cron_time': newForm.cronForm,
        'outgoings_id' : +form.out,
        'outgoings_templates': this.outgoings_templates,
        'out_validated': this.out_validated,
        'type_conciliation' : +form.typeConciliation,
        'keys': request,
        'interfaces' : interfaces
      }
    };

    

    
    this.conciliaService.create(saveData).subscribe((data) => {
      if (data.status === '200') {
        Swal.fire(
          'Correcto!',
          'La configuración de conciliación ha sido creada correctamente',
          'success'
        );
        setTimeout(() => {
          Swal.close();
          this.router.navigateByUrl('/config-conciliation');
        }, 3000);
      } 
       if (data.message === 'OBJECT EXISTS' || data.status === 400 ) {
          Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'No se ha podido crear la configuración de conciliación, intentelo con otro nombre'
        });
      }

    }, (err) => {
      console.log(err);
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Ocurrió un error, intente más tarde'
      });
    });


  }

}
