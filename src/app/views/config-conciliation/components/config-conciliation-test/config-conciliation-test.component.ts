import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef  } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import { ConciliationService } from '../../services/conciliation.service';
import { Template, Interface, DataType } from '../../model/template';
import { Tabs } from '../../model/tabs';
import Swal from 'sweetalert2';
import { ENTERPRISE_CODE, USER_ID } from '../../../login/models/login';

@Component({
  selector: 'app-config-conciliation-test',
  templateUrl: './config-conciliation-test.component.html',
  styleUrls: ['./config-conciliation-test.component.scss']
})
export class ConfigConciliationTestComponent implements OnInit {

  constructor(private fb: FormBuilder,private conciliaService: ConciliationService,private router: Router, private route: ActivatedRoute) {}

  @ViewChild('file') fileInput1: ElementRef;
  @ViewChild('Input1' ) Input1: ElementRef;
  @ViewChild('file2') fileInput2: ElementRef;
  @ViewChild('Input2' ) Input2: ElementRef;

  enterprise_code = localStorage.getItem(ENTERPRISE_CODE);
  user_id = localStorage.getItem(USER_ID);
  sub: any;
  id_url: number;
  interfaces: any;
  keys: any;
  intKyes: any[] = [];
  intKyes2: any[] = [];
  interf: any;
  templates: any;
  tempts: any;
  fileToUpload1: File = null;
  fileToUpload2: File = null;
  test_conc_form: FormGroup;
  responseMessage: string = '';
  responseServer: boolean;
  initialKeys: boolean;
  responseKeys: boolean;

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.id_url = +params['id'];
    });

    this.conciliaService.getInterfaceById(this.id_url, this.enterprise_code).subscribe((data) => {
      console.log(data)
      this.interfaces = data.interfaces;
      this.keys = data.keys;
    });

    this.conciliaService.getInterfaces(this.enterprise_code).subscribe((data) => {
      console.log(data)
      this.interf = data;
    })
    
    this.conciliaService.getTemplates(this.enterprise_code).subscribe((data) => {
      console.log(data)
      this.templates = data;
    })
    this.createForm();
    this.responseServer = false;
    this.initialKeys = true;
    this.responseKeys = false;
  }

   
  

  handleFileInput(files: FileList) {
    this.fileToUpload1 = files.item(0);
    this.test_conc_form.controls['file_name'].setValue(this.fileToUpload1?.name);
  }
  handleFileInput2(files: FileList) {
    this.fileToUpload2 = files.item(0);
    this.test_conc_form.controls['file_name2'].setValue(this.fileToUpload2?.name);
  }

  createForm() {
    this.test_conc_form = this.fb.group({ 
      file_name: [],
      file_name2: []
    });
  }


  clearFile(){
    this.fileInput1.nativeElement.value = null;
    this.test_conc_form.controls['file_name'].setValue(null);
    this.fileInput2.nativeElement.value = null;
    this.test_conc_form.controls['file_name2'].setValue(null);
  }

  probarConciliacion(){
    let arr = [];
    let arrT = [];
    let resD = [];
    let resD2 = [];
    console.log(this.keys)
    

    if(this.fileToUpload1 == null ){
      this.responseServer = true;
      this.responseMessage = '<i>Debes adjuntar 2 archivos para realizar la prueba de conciliaci&oacute;n</i>';
      setTimeout(() => {
        this.Input1.nativeElement.focus()
      },100)
    }
    else if(this.fileToUpload2 == null){
      this.responseServer = true;
      this.responseMessage = '<i>Debes adjuntar 2 archivos para realizar la prueba de conciliaci&oacute;n</i>';
      setTimeout(() => {
        this.Input2.nativeElement.focus()
      },100)
    }
    else if(this.fileToUpload1.size > 10240){
      this.responseServer = true;
      this.responseMessage = '<i>El tama&ntilde;o de los archivos no puede ser mayor a 10 KB.</i>';
      setTimeout(() => {
        this.Input1.nativeElement.focus()
      },100)
    }
    else if(this.fileToUpload2.size > 10240){
      this.responseServer = true;
      this.responseMessage = '<i>El tama&ntilde;o de los archivos no puede ser mayor a 10 KB.</i>';
      setTimeout(() => {
        this.Input2.nativeElement.focus()
      },100)
    }
    else{
      this.interfaces.forEach((key, value) => {
        arr.push(this.interf.find((x) => x.id === +key.interfaceId));
      })
      
      
      arr.forEach((key, value) =>{
        let file_n: string = "";
  
        if(value == 0){
          file_n = this.fileToUpload1.name;
        }
        else{
          file_n = this.fileToUpload2.name;
        }
        let objeto = { 
          "interfaceId" : key.id, 
          "name" : key.name, 
          "code" : key.code, 
          "header": key.template_header, 
          "is_titled": key.is_titled, 
          "separator": this.templates.find((x) => x.id === +key.template_id).separator
          
        };
        arrT.push(objeto);
      })
  
      this.conciliaService.testConciliation(this.fileToUpload1, this.fileToUpload2, this.keys, arrT).subscribe((data) => {
        console.log(data)
        if(data.Result){
          this.responseServer = true;
          this.responseMessage = '<i>Interfaces conciliadas.</i>';
        }
        else{
          this.responseServer = true;
          this.responseMessage = '<i>Interfaces no conciliadas.</i>';
        }
        this.intKyes2 = [];

        //se ordena el response en base al orden de las interfaces
        this.interfaces.forEach((value, key) => {
          Object.entries(data.DataConciliation).forEach((v, k) => {
            if(value.code == v[1][1].code){
              resD.push(v[1][2]);
            }
          });
        });
        console.log(resD)

        resD.forEach((value, key) =>{
          Object.entries(value).forEach((v, k) =>{
            let name = "";
            let val = "";
            this.keys.forEach((valor, llave) => {
              valor.forEach((vl, ll) => {
                Object.entries(v[1]).forEach((va, ke) => {
                  if(vl.key == va[0]){
                    name = va[0].toString();
                    val = va[1].toString();
                    let obj2 = {
                      "name": name,
                      "value": val
                    };
                    resD2.push(obj2)
                  }
                });
              });
            });
          })
        })

        this.keys.forEach((value, key) => {
          this.intKyes = [];
          value.forEach((v,k) => {
            resD2.forEach((value,key) =>{
              if(v.key == value.name){
                this.intKyes.push(value);
              }
            })
          });
          this.intKyes2.push(this.intKyes);
        });
        this.responseKeys = true;
        this.initialKeys = false;

      },
      error => {
        console.log(error)
        
        this.fileToUpload1 = null;
        this.fileToUpload2 = null;
        this.clearFile();
        this.responseServer = true;
        this.responseMessage = '<i>Ocurrió un error desconocido, por favor intente nuevamente más tarde</i>';
        if(error.status == '406'){
          this.responseMessage = '<i>'+error.error.Status+'</i>';
        }
        
        
      })
    }
  }
}