import { Component, OnInit } from '@angular/core';
import {Tabs} from '../../model/tabs';
import {DataType, Interface, Template} from '../../model/template';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import {ConciliationService} from '../../services/conciliation.service';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ENTERPRISE_CODE, USER_ID } from '../../../login/models/login';
import { IsLoadingService } from '@service-work/is-loading';
import { OutService } from '../../../out/service/out.service';

@Component({
  selector: 'app-config-conciliation-edit',
  templateUrl: './config-conciliation-edit.component.html',
  styleUrls: ['./config-conciliation-edit.component.scss']
})
export class ConfigConciliationEditComponent implements OnInit {

  dataValidate: any = [];
  activeButton = true;
  out_validated = 1;
  outgoings_templates = [];
  templateOut = [];
  typeConciliation = [
    {
      name: 'Conciliados',
      id : 1
    },
    {
      name: 'No conciliados',
      id : 0
    },
  ]
  dinamicData = {};
  dataSource = []
  enterprise_code = localStorage.getItem(ENTERPRISE_CODE);
  user_id = localStorage.getItem(USER_ID);
  

  tabs: Tabs[] = [{
      title: "Campo 1",
      content: '',
      active : true,
      disabled: false,
      removable: false
    }];
  addNewTab(): void {
    const newTabIndex = this.tabs.length + 1;
    const title = `Campo ${newTabIndex}`;
    this.tabs.push({
      title,
      content: `Dynamic content ${newTabIndex}`,
      disabled: false,
      removable: false
    });
    this.dinamicData[title] = {
      data: [],
      decimal : false,
     }
    this.setDynamicData();
  }

  removeTabHandler(tab: any): void {
    this.tabs.splice(this.tabs.indexOf(tab), 1);
    // console.log('Remove Tab handler');
  }

  selectDecimal = false;
  template : Template[]
  selectedItems: Template[] =[];
  form: FormGroup;
  newForm: FormGroup;
  cronValue = "0 0 7 ? * * *";
  interfaces : Interface[];
  DataType : DataType[];
  sub: any;
  id_url: number;
  keys: any;
  resetState: boolean = true;
  del: boolean = false;

  constructor(
  private fb: FormBuilder,
  private route: ActivatedRoute,
  private router: Router,
  private conciliaService: ConciliationService,
  private loading: IsLoadingService,
  private outService: OutService) {
   
    this.dinamicData[this.tabs[0].title] = {
      data: [],
      decimal: false,
    }
  }

  ngOnInit(): void {

    this.getOuts();
    this.getInterfaces();
    this.conciliaService.getDataType(this.enterprise_code).subscribe((data)=>{
      this.DataType = data;
    });
    this.sub = this.route.params.subscribe(params => {
      this.id_url = +params['id'];
    });

    this.createForm();
    this.createNewForm();
    this.getInterfaceToEdit(this.resetState);
  }













  getInterfaces(){
    this.conciliaService.getInterfaces(this.enterprise_code).subscribe((data)=>{
      this.interfaces = data;
    });
  }
    
  getInterfaceToEdit(reset: boolean){
    let int_name;
    this.keys = [];
    this.conciliaService.getInterfaceById(this.id_url, this.enterprise_code).subscribe((data) => {
      console.log(data)
      console.log(data.interfaces)
      // console.log(data)
      if(!this.del){
        int_name = data.interfaces[0].interfaceId;
      }
      else{
        int_name = null;
      }
      let form = this.form.value;
      data.outgoings_templates.forEach(a => {
        form.interfaces.forEach(b => {
          b.templateOut = a.outgoing_template_id;
          // console.log('ssssss',b.templateOut);
        });
          
      });

      // for (const i of data.outgoings_templates) {
      //   console.log('sssssssssssss',i.outgoings_templates);
      // }

      this.keys = data.keys;
      this.form = this.fb.group({
        out: [data.outgoings_id],
        typeConciliation : [data.out_validated],
        interfaces: this.fb.array([]),
        interface_name: [int_name, [Validators.required]],
      });
      this.newForm = this.fb.group({
        
        cronForm: [data.cron_time, []],
        conciliation_name: [data.name, [Validators.required, Validators.minLength(3)]],
        
      });
      
      
      if(reset){
        setTimeout(()=>{
          
          data.interfaces.forEach((value, key) => {
            this.onInterfaceSelected(parseInt(value.interfaceId))
          });
        }, 700)
  
        data.keys.forEach((value, key) => {        
          if(key > 0){
            this.addNewTab();
          }
        });
      }
    })
  }

  onChangeDato(e : any, tabz : any) {
    this.dataValidate.push(e);
    console.log(this.dataValidate);

    if ( this.dataValidate.length >= 4 && this.selectedItems.length >= 2) {
      this.activeButton = false;
    } else {
      this.activeButton = true;
    }
    
  }
  onChangeTipoDato(e : any, tabz : any) {
    this.dataValidate.push(e);
    console.log(this.dataValidate);
    if (this.dataValidate.length >= 4 && this.selectedItems.length >= 2) {
      this.activeButton = false;
    }else {
      this.activeButton = true;
    }
    
  }

  onKey(value: any, tabz: string, item: any, index : number) {
    // console.log(this.selectedItems);
    
    // console.log(tabz)
    // console.log(index)
    this.dinamicData[tabz].data[index].decimal = false;

    const id = value.target.selectedOptions[0].id;
    const code = `${tabz}.${item.name}.typeCode`;
    
    
    const formControlValue = this.DataType.find((x) => x.id === +id).code;
    this.form.setControl(code, new FormControl(formControlValue))
    
    if (+id === 78) {
      this.dinamicData[tabz].data[index].decimal = true;
    }
    else{
      this.dinamicData[tabz].data[index].decimal = false;
    }
    
  }

  addCode(val: any, value: any, tabz: string, item: any, index : number){
    const id = item.id;
    
    const formControlValue = this.interfaces.find((x) => x.id === +id);
    const code = `${tabz}.${item.name}.key`;
    const interfaceCode = `${tabz}.${item.name}.interfaceCode`;

    const codes = formControlValue.template_header.find((x) => x.name == val).code;
    const idInterfaz = this.interfaces.find((x) => x.id === +id).code;
    // console.log(idInterfaz)
    this.form.setControl(code, new FormControl(codes))
    this.form.setControl(interfaceCode, new FormControl(idInterfaz))
  }

  onInterfaceSelected(value: any) {
    const id = +value;
    if (this.selectedItems.find((x) => x.id === +id)) {
      // this.form.controls['interface_name'].setValue(this.selectedItems.map((x) => x.id));
      return;
    }
    this.selectedItems.push(this.interfaces.find((x) => x.id === +id));
    

    this.setDynamicData();
    // this.getTemplatesOutByInterface(id);
     let name = this.selectedItems[this.selectedItems.length -1]
    // this.conciliaService.getTemplatesOutByInterface(this.enterprise_code, id).subscribe(data => {
    //   console.log('dataaaaaa', data);
    //   data.forEach(element => {
    //     this.addInterfaces(name.name, name.id, element.id);
        
    //   });
    // })
    
  }

  resetkey() {
    this.dataValidate = [];
    console.log(this.dataValidate);
    this.activeButton = true;
    this.createForm();
    this.removeInter();
    this.selectedItems = [];
    this.interfaces = [];
    this.tabs = [{
      title: `Campo 1`,
      content: '',
      active : true,
      disabled: false,
      removable: true
    }];
    this.dinamicData[this.tabs[0].title] = {
      data: [],
      decimal: false,
    };
    this.form.get('interface_name').setValue('');
    this.getInterfaces();
    this.resetState = true;
    this.del = false;
    this.getInterfaceToEdit(this.resetState);
    // this.setDynamicData();
  }

  // reset() {
  //   this.form.get('out').setValue('');
  //   let form = this.form.value;
  //   this.templateOut = []
   
  //   for (const i of form.interfaces) {
  //     i.templateOut = '';
      
  //   }

  // }

  deletekey() {
     this.dataValidate = [];
    console.log(this.dataValidate);
    this.activeButton = true;
    this.createForm();
    this.selectedItems = [];
    this.interfaces = [];
    this.tabs = [{
      title: `Campo 1`,
      content: '',
      active : true,
      disabled: false,
      removable: false
    }];
    this.dinamicData[this.tabs[0].title] = {
      data: [],
      decimal: false,
    };
    this.setDynamicData();
    this.form.get('interface_name').setValue('');
    this.getInterfaces();
  }
  
 
  setDynamicData() {
    // console.log(this.dinamicData)
    for (const key in this.dinamicData) {
      if (Object.prototype.hasOwnProperty.call(this.dinamicData, key)) {
        const inter = this.selectedItems.filter((value) => {
          // this.dataValidate.push(value.template_header)
          let xName;
          if (this.dinamicData[key]) {
             xName = this.dinamicData[key].data.find((x) => x.name === value.name)
          }
          // console.log(xName);
          if (!xName) {
              this.form.setControl(key + '.' + value.name + '.name', new FormControl())
              this.form.setControl(key + '.' + value.name + '.type', new FormControl())
              this.form.setControl(key + '.' + value.name + '.typeCode', new FormControl())
              this.form.setControl(key + '.' + value.name + '.precision', new FormControl())
              this.form.setControl(key + '.' + value.name + '.key', new FormControl())
            return  {name: value.name,template_header : value.template_header,}
          }
        });
        this.dinamicData[key].data = [...this.dinamicData[key].data, ...inter]
     }
    }
    if(this.resetState){
      setTimeout(()=> {
        this.populateData();
      }, 100)
    }

    // if (this.dataValidate.length > 0 && this.selectedItems.length >= 2) {
    //   this.activeButton = false;
    // } else {
    //   this.activeButton = true;
    // }
  }


  populateData( ) {
    this.keys.forEach((value, key) =>{
      value.forEach((v, k) => {
        if(this.selectedItems[k]){
          // console.log("Campo "+ (key+1)+"."+ this.selectedItems[k].name+".name")
          this.form.setControl("Campo "+ (key+1)+"."+ this.selectedItems[k].name+".name", new FormControl(v.name))
          this.form.setControl("Campo "+ (key+1)+"."+ this.selectedItems[k].name+".type", new FormControl(v.type))
          this.form.setControl("Campo "+ (key+1)+"."+ this.selectedItems[k].name+".key", new FormControl(v.key))
          this.form.setControl("Campo "+ (key+1)+"."+ this.selectedItems[k].name+".precision", new FormControl(v.precision))
          this.form.setControl("Campo "+ (key+1)+"."+ this.selectedItems[k].name+".typeCode", new FormControl(v.typeCode))
          this.form.setControl("Campo "+ (key+1)+"."+ this.selectedItems[k].name+".interfaceCode", new FormControl(v.interfaceCode))
        }
      });
    });
  }

  removeInterfaces(id: number) {
    // console.log(this.dinamicData)
    Object.entries(this.dinamicData).forEach((value, key) =>{
      Object.entries(value).forEach((v, k) => {
        if(typeof v[1] == 'object'){
          Object.entries(v[1]).forEach((vl, cl) =>{ 
            if(typeof vl[1] == 'object'){
              vl[1].forEach((vlr, ky)=>{
                if(vlr.id == id){
                  // console.log("la interfaz id " + id + " se encuentra presente en la llave")
                }
              })
            }
          })
        }
      });
    });

    this.selectedItems = this.selectedItems.filter((x) => x.id !== id);
    if (this.selectedItems.length === 0) {
      this.form.get('interface_name').setValue(null);
    }
  }



  createForm() {
    
    this.form = this.fb.group({
      out: [''],
      typeConciliation : [0],
      interfaces: this.fb.array([]),
      cronForm: [this.cronValue, []],
      conciliation_name: [null, [Validators.required, Validators.minLength(3)]],
      interface_name: [null, [Validators.required]],
    });
  }
  createNewForm() {
    
    this.newForm = this.fb.group({
      cronForm: [this.cronValue, []],
      conciliation_name: [null, [Validators.required, Validators.minLength(3)]],
     
    });
  }

  get conciliationNameNotValid() {
    return this.newForm.get('conciliation_name').invalid && this.newForm.get('conciliation_name').touched;
  }

  get conciliationNameValid() {
    return this.newForm.get('conciliation_name').valid && this.newForm.get('conciliation_name').touched;
  }

  get getInterface() {
    return this.form.get('interfaces') as FormArray;
  }

  // getTemplatesOutByInterface(id : number) {
  //   this.loading.add(
  //     this.conciliaService.getTemplatesOutByInterface(this.enterprise_code, id).subscribe(data => {
  //       // console.log('template de salida por interface',data);
  //       this.templateOut[id] = data;
  //       // console.log(this.templateOut);
       
  //    })
  //   )
  // }

  getOuts() {
    this.loading.add(
      this.outService.getOuts(this.enterprise_code ).subscribe(data => {
        this.dataSource = data;
        // console.log(this.dataSource);
     })
    )
  }

  touched(e: any) {
    //  this.getTemplatesOutByInterface(e);
  }

  // addInterfaces(name?: string, id? : number, templateOut? : any) {
  //   // console.log(name);
  //   const control = <FormArray>this.form.controls['interfaces'];
  //   control.push(this.fb.group({
  //     name: [name ? name : ''],
  //     id: [id ? id : ''],
  //     templateOut : [templateOut? templateOut : '']
  //   }));
    
  // }

   removeInter() {
    const control = <FormArray>this.form.controls['interfaces'];
    control.clear();
  }


  handelSave() {
    const obj = [];
    const form = this.form.value;
    // console.log(form) 
   for (const key in form) { 
     
     if (Object.prototype.hasOwnProperty.call(form, key)) {
       const element = form[key];
       const j = key.split('.')[1];
      if(j) {
        const k = key.split('.');
        if (obj[k[0]]) {
          const l = obj[k[0]];
          if (l[k[1]]) {
            l[k[1]][k[2]] = element;
          } else {
            const x = k[2];
            l[k[1]] = {[x] : element}
          }
        } else {
          const result = [];
          result[k[1]] = {[k[2]] : element}
          obj[k[0]] = result;
        }
      }
       
     }
    }
    const request = [];
    for (const key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        const element = obj[key];
        const arreglo = [];
        for (const k in element) {
          if (Object.prototype.hasOwnProperty.call(element, k)) {
            const el = element[k];
            arreglo.push(el);
          }
        } 
        request.push(arreglo);
      }
    }
    //console.log(this.selectedItems)
    //console.log(request);

    let interfaces: any[] = []; 

    this.selectedItems.forEach((value, key) => {
      let inter = {
        "interfaceId" : value.id, 
        "name" : value.name , 
        "code" : value.code};
      interfaces.push(inter);  
      
    })

    // console.log(interfaces)

    //  this.outgoings_templates = [];
    //   for (const i of form.interfaces) {
    //       const obj = {
    //       interface_id: +i.id,
    //       outgoing_template_id : +i.templateOut
    //     }
    //     this.outgoings_templates.push(obj);
        
    //   }
      
    //   // console.log(form.out);
    //   const out = form['out'];
      
    //   const templateOut = form['interfaces'][0]['templateOut'];
      
    //   if((out && out != null && out != '') && (templateOut && templateOut != null && templateOut != '')){
    //     this.out_validated = 1	
    //   }else{
    //     this.out_validated = -1
    //   }
      
    this.form.patchValue({'interface_name': request})

    // let editData = {
    //   "schema" : this.enterprise_code,
    //   "model" : "conf_conciliation",
    //   "id_name": "id",
    //   "object_new": {
    //       "id" : this.id_url,
    //       "name" : form.conciliation_name,
    //       "user_id" : this.user_id,
    //       "cron_time" : form.cronForm,
    //       "keys": request,
    //       "interfaces" : interfaces
    //   }
    // };
     const newForm = this.newForm.value;
    const editData = {
      'schema' : this.enterprise_code,
      'model' : 'conf_conciliation',
      'id_name': 'id',
      'object_new': {
        'id' : this.id_url,
        'name' : newForm.conciliation_name,
        'user_id' : this.user_id,
        'cron_time': newForm.cronForm,
        'outgoings_id' : +form.out,
        'outgoings_templates': this.outgoings_templates,
        'out_validated': this.out_validated,
        'type_conciliation' : +form.typeConciliation,
        'keys': request,
        'interfaces' : interfaces
      }
    };
  


    // console.log(editData);

    this.conciliaService.updateInterface(editData).subscribe((data)=>{
      if(data.status == '200'){
        Swal.fire(
          'Correcto!',
          'La configuración de conciliación ha sido actualizada correctamente',
          'success'
        );
        setTimeout(() => {
          Swal.close();
          this.router.navigateByUrl('/config-conciliation');
        }, 3000);
      }
      else{
         if (data.message === 'OBJECT EXISTS' || data.status === 400 ) {
          Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'No se ha podido crear la configuración de conciliación, intentelo con otro nombre'
        });
      }
      }
    },(err) => {
      // console.log(err)
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Ocurrió un error, intente más tarde'
      });
    });
  }
  addInterface() {}

}
