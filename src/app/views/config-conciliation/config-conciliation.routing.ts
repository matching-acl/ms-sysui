import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ConfigConciliationComponent} from './config-conciliation.component';
import {ConfigConciliationAddComponent} from './components/config-conciliation-add/config-conciliation-add.component';
import {ConfigConciliationEditComponent} from './components/config-conciliation-edit/config-conciliation-edit.component';
import {AuthGuardService} from '../login/service/auth-guard.service';
import { ConfigConciliationTestComponent } from './components/config-conciliation-test/config-conciliation-test.component';

const routes: Routes = [
  { path: '', component: ConfigConciliationComponent, canActivate: [AuthGuardService]},
  { path: 'create', component: ConfigConciliationAddComponent, canActivate: [AuthGuardService]},
  { path: 'edit/:id', component: ConfigConciliationEditComponent, canActivate: [AuthGuardService]},
  { path: 'test/:id', component: ConfigConciliationTestComponent, canActivate: [AuthGuardService]}
];

@NgModule ({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigConciliationRoutingModule {}
