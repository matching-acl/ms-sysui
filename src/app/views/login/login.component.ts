import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Enterprise, EnterprisesAllUser} from '../enterprise/model/enterprise';
import {LoginService} from './service/login.service';
import {NavigationExtras, Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  spinner = false;
  form: FormGroup;
  enterpriseByUser: Enterprise[];
  errorUser = '';
  isErrorUser = false;
  errorPass = '';
  isErrorPass = false;
  disableUser = false;
  enterpriseName = '';
  hidePassword = true;
  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private router: Router) {}

  ngOnInit(): void {
    this.createForm();

    const inputs = document.querySelectorAll('.input');
    const selects = document.querySelectorAll('.select');

    function focusFunc() {
      const parent = this.parentNode.parentNode;
      parent.classList.add('focus');
    }
    function blurFunc() {
      const parent = this.parentNode.parentNode;
      if (this.value === '') {
        parent.classList.remove('focus');
      }
    }

    inputs.forEach(input => {
      input.addEventListener('focus', focusFunc);
      input.addEventListener('blur', blurFunc);
    });
    selects.forEach(select => {
      select.addEventListener('focus', focusFunc);
      select.addEventListener('blur', blurFunc);
    });
  }


  // Formularios validos
  get userNameValid() {
    return this.form.get('user_name').valid && this.form.get('user_name').touched;
  }
  get passwordValid() {
    return this.form.get('password').valid && this.form.get('password').touched;
  }
  get enterpriseValid() {
    return this.form.get('enterprise').valid && this.form.get('enterprise').touched;
  }

  // Formularios invalidos
  get userNameInvalid() {
    return this.form.get('user_name').invalid && this.form.get('user_name').touched;
  }
  get passwordInvalid() {
    return this.form.get('password').invalid && this.form.get('password').touched;
  }
  get enterpriseInvalid() {
    return this.form.get('enterprise').invalid && this.form.get('enterprise').touched;
  }

  createForm() {
    this.form = this.fb.group({
      user_name: [null, [Validators.required]],
      password: [null, [Validators.required]],
      enterprise: [null, [Validators.required]]
    });
  }

  OnClickUser() {
    this.isErrorUser = false;
    this.errorUser = '';
    this.isErrorPass = false;
    this.errorPass = '';
    this.form.controls['user_name'].reset();
    this.form.controls['password'].reset();
    this.form.controls['enterprise'].reset();
    this.enterpriseByUser = [];
  }

  OnClickPass() {
    this.isErrorPass = false;
    this.errorPass = '';
    this.form.controls['password'].reset();
  }

  OnChange(event: any) {
    console.log({event: event});
    if (this.form && this.form.controls['user_name'].valid) {
        this.disableUser = true;
        this.loginService.getEnterprisesByUser(this.form.controls['user_name'].value).subscribe((data) => {
          console.log(data);
          if ( data && data.enterprises && data.enterprises.elements && data.enterprises.elements.length > 0) {
              this.enterpriseByUser = data.enterprises.elements;
              console.log({enterpriseByUser: this.enterpriseByUser });
              if (this.enterpriseByUser.length === 1) {
                this.form.controls['enterprise'].setValue(this.enterpriseByUser[0].code);
                this.enterpriseName = this.enterpriseByUser[0].name;
              }
          } else {
            this.isErrorUser = true;
            this.errorUser = 'Usuario sin empresa asignada.';
          }
          this.disableUser = false;
        }, (err) => {
          this.isErrorUser = true;
          this.disableUser = false;
          this.errorUser = 'Usuario sin empresa asignada.';
        });
    }
  }

  handelSave() {
    this.isErrorPass = false;
    this.errorPass = '';
    const user_name = this.form.controls['user_name'].value;
    const password = this.form.controls['password'].value;
    const enterprise_code = this.form.controls['enterprise'].value.toString();
    this.spinner = true;
    this.form.disable();
    this.loginService.getLogin(user_name, password, enterprise_code).subscribe((data) => {
      console.log({dataaa: data});
      if (data && data.status === '200') {
        if (data.active && data.role && data.role.privileges && data.role.privileges.length > 0) {
          const role = data.role.name;
          let permission_add = [];
          data.role.privileges.forEach(p => permission_add.push(p.name));
          permission_add = permission_add.sort();
          const enterprise_name = data.schemaName;
          const user_id = data.id;
          const client_id = data.clientId;

          this.loginService.setLocalStorageAndRedirectByLogin(enterprise_code, enterprise_name, user_name,
            user_id, permission_add, role, client_id);

        } else {
          this.isErrorUser = true;
          this.errorUser = 'Usuario inactivo.';
          this.spinner = false;
        }
        this.form.enable();
      } else {
        this.isErrorPass = true;
        this.errorPass = 'Contraseña incorrecta.';
        this.spinner = false;
        this.form.enable();
      }
    }, (err) => {
      console.log({ERRORRR: err}) ;
      this.spinner = false;
      this.isErrorPass = true;
      this.errorPass = 'Contraseña incorrecta.';
      this.form.enable();
    });
  }

  getForgotPassword() {
    const navigationExtras: NavigationExtras = {
      state: {
        user_name: this.form.controls['user_name'].value
      }
    };
    this.router.navigate(['/forgot-password'], navigationExtras);
  }

}
