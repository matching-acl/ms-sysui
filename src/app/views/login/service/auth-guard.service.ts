import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {ENTERPRISE_CODE, PERMISSIONS, USER_ID, USER_NAME} from '../models/login';
import {PrivilegesName} from '../../rol/model/rol';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  user = null;
  user_id = null;
  enterprise = null;
  permission = [];
  constructor(
    private router: Router
  ) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    this.user = localStorage.getItem(USER_NAME);
    this.user_id = localStorage.getItem(USER_ID);
    this.enterprise = localStorage.getItem(ENTERPRISE_CODE);
    this.permission = this.permissions;
    const url: string = state.url;
    const url_accept = this.urlAccept(url);
    // console.log({url: url, permissionssACTIVE: this.permission});
    if ( !this.user || !this.user_id || !this.enterprise || this.permission.length === 0 || url_accept === false) {
      localStorage.clear();
      this.router.navigate(['/login']);
      return false;
    } else {
      return true;
    }
  }

  get permissions(): string[] {
    const permissions = localStorage.getItem(PERMISSIONS);
    return permissions ? permissions.split(',') : [];
  }

  urlAccept(url: string): boolean {
    let url_accept = false;
    if (this.permission && this.permission.length > 0 ) {
      this.permission.forEach(p => {
        if (p === PrivilegesName.admin && ( url === '/enterprises' || url === '/users' || url === '/roles' )) {
          url_accept = true;
        } else if (p === PrivilegesName.gestion &&
          (url === '/out' || url === '/out/create' || url === '/out/edit' || url.includes('/out/edit/') || url === '/template-out' || url === '/template-out/create'  || url === '/template-out/edit' || url.includes('/template-out/edit/') ||url === '/templates' || url === '/templates/create' || url.includes('/templates/edit/') ||
           url === '/interface' || url === '/interface/create' || url.includes('/interface/edit') ||
           url === '/config-conciliation' || url === '/config-conciliation/create' || url.includes('/config-conciliation/edit/') || url.includes('/config-conciliation/test/'))) {
          url_accept = true;
        }
        if (p === PrivilegesName.reportes && (url === '/reportes-conciliacion' || url === '/reportes-interfaces' || url === '/reportes-outs')) {
          url_accept = true;
        }
      });
    }
    return url_accept;
  }
}
