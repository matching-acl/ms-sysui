import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment.prod';
import {CLIENT_ID, ENTERPRISE_CODE, ENTERPRISE_NAME, PERMISSIONS, ROLE, USER_ID, USER_NAME} from '../models/login';
import {PrivilegesName} from '../../rol/model/rol';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class LoginService {
  private BaseUrl = environment.coreApiUrl;
  username: any = 'admin';
  password: any = 'admin';
  authorization = 'Basic ' + btoa(this.username + ':' + this.password);

  headers(enterprise_code: string) {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization', this.authorization);
    if (enterprise_code) {
      header = header.set('xTenantId', enterprise_code);
    }
    return header;
  }

  constructor(
    private http: HttpClient,
    private router: Router) {
  }

  getEnterprisesByUser( user_name: string ): Observable<any> {
    const body = {'fields' : {}, 'page': 0, 'search': user_name};
    return this.http.post(this.BaseUrl + 'user/tenant', body, { headers: this.headers(null)});
  }

  getLogin (user: string, user_password: string, enterprise_code: string ): Observable<any> {
    const body = {'username': user, 'password': user_password, 'status': ''};
    return this.http.post(this.BaseUrl + 'tenant/security/login', body, { headers: this.headers(enterprise_code)});
  }

  getChangeEnterprise (user: string, enterprise_code: string ): Observable<any> {
    const body = {'username': user, 'status': ''};
      return this.http.post(this.BaseUrl + 'tenant/security/change_enterprise', body, { headers: this.headers(enterprise_code)});
  }

  setLocalStorageAndRedirectByLogin(enterprise_code: string, enterprise_name: string, user_name: string, user_id: string,
                                    permission_add: Array<string>, role: string, client_id: string) {
    localStorage.clear();
    localStorage.setItem(ENTERPRISE_CODE, enterprise_code);
    localStorage.setItem(ENTERPRISE_NAME, enterprise_name);
    localStorage.setItem(USER_NAME, user_name);
    localStorage.setItem(USER_ID, user_id);
    localStorage.setItem(PERMISSIONS, permission_add.join(','));
    localStorage.setItem(ROLE, role);
    localStorage.setItem(CLIENT_ID, client_id);

    if (permission_add.find(f => f === PrivilegesName.admin)) {
      this.router.navigateByUrl('/enterprises');
    } else if (permission_add.find(f => f === PrivilegesName.gestion)) {
      this.router.navigateByUrl('/config-conciliation');
    } else if (permission_add.find(f => f === PrivilegesName.reportes)) {
      this.router.navigateByUrl('/reportes-conciliacion');
    }
  }

  getListVersions(client_id: number): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');

    const filter = {'id_cliente': client_id};

    const pr = {'model': 'bitacora_cliente_version', 'filters': filter};

    return this.http.post(environment.bashRegisterApiUrl + 'available_versions', pr, {headers: header});
  }
}
