export const ENTERPRISE_CODE = 'ENTERPRISE_CODE';
export const ENTERPRISE_NAME = 'ENTERPRISE_NAME';
export const USER_NAME = 'USER_NAME';
export const USER_ID = 'USER_ID';
export const PERMISSIONS = 'PERMISSIONS';
export const ROLE = 'ROLE';
export const CLIENT_ID = 'CLIENT_ID';
