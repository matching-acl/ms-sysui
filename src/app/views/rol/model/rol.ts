import {INavData} from '@coreui/angular';

export interface RolesFilter {
    id: number;
    name: string;
}

export class Rol {
  id?: number;
  active?: boolean;
  created?: string;
  updated?: string;
  name?: string;
  description?: string;
  message?: boolean;
  status?: string;
  privileges?: Privileges [];
  schemaName?: string;
  clientId?: string;
}

export class Privileges {
  id?: number;
  active?: boolean;
  created?: string;
  name?: string;
  description?: string;
  message?: boolean;
  roles?: Rol[];
  status?: string;
  updated?: string;
}

export class PrivilegesSend {
  active?: boolean;
  name?: string;
  description?: string;
  id?: number;
  created?: string;
  updated?: string;
}

export enum RolesNumber {
  admin_holding = 1,
  admin = 2,
  admin_gestor = 3,
  gestor = 4,
  reportes = 5
}

export enum RolesText {
  admin_holding = 'Admin Holding',
  admin = 'Admin',
  admin_gestor = 'Admin Gestor',
  gestor = 'Gestor',
  reportes = 'Reportes'
}

export enum PrivilegesName {
  admin = 'Administración',
  gestion = 'Gestión',
  reportes = 'Reportes'
}
