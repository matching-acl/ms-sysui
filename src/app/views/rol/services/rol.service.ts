import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import {environment} from '../../../../environments/environment.prod';
import {Observable} from 'rxjs';
import {ENTERPRISE_CODE} from '../../login/models/login';

@Injectable({
  providedIn: 'root'
})
export class RolService {
  private BaseUrl = environment.coreApiUrl;
  username: any = 'admin';
  password: any = 'admin';
  authorization = 'Basic ' + btoa(this.username + ':' + this.password);
  enterprise_code = localStorage.getItem(ENTERPRISE_CODE);


  headers(enterprise_code: string) {
    console.log({enterprise_codeROLESS: enterprise_code});
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization', this.authorization);
    header = header.set('xTenantId', enterprise_code);
    console.log({HEADERSS: header});
    return header;
  }

  constructor(private http: HttpClient) {

  }

  getAllPrivileges(enterprise_code: string): Observable<any> {
    const body = {'fields' : {}, 'page': 0, 'search': ''};
    return this.http.post<any>(this.BaseUrl + 'tenant/privilege/search', body, { headers: this.headers(enterprise_code)});
  }

  getAllRoles(enterprise_code: string, pageIndex?: number, pageSize?: number): Observable<any> {
    const body = {'fields' : {}, 'page': pageIndex, 'search': '', 'status': '', 'size': pageSize};
    return this.http.post<any>(this.BaseUrl + 'tenant/role/search', body, { headers: this.headers(enterprise_code)});
  }

  updateActiveRol(id: number, active: boolean, enterprise_code: string): Observable<any> {
    const body =  { 'id': id, 'fieldValue': { 'active': active } };
    return this.http.patch(this.BaseUrl + 'tenant/role', body, {headers: this.headers(enterprise_code)});
  }

  createRol(data: any, enterprise_code: string): Observable<any> {
    return this.http.post(this.BaseUrl + 'tenant/role', data, { headers: this.headers(enterprise_code) });
  }

  updateRol(data: any, enterprise_code: string): Observable<any> {
    return this.http.put(this.BaseUrl + 'tenant/role', data, { headers: this.headers(enterprise_code) });
  }
}
