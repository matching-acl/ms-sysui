import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ReactiveFormsModule } from '@angular/forms';
import { RolRoutingModule } from './rol-routing.module';
import { RolComponent } from './rol.component';
import { AddRolModalComponent } from './components/add-rol-modal/add-rol-modal.component';
import { EditRolModalComponent } from './components/edit-rol-modal/edit-rol-modal.component';
import { DirectivesModule } from '../../directives/directives.module';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PipesModule } from '../../pipes/pipes.module';
import { PrivilegiesDetailsModalComponent } from './components/privilegies-details-modal/privilegies-details-modal.component';
import {MatPaginatorIntl, MatPaginatorModule} from '@angular/material/paginator';
import {CustomMatPaginatorIntl} from '../paginator-es';



@NgModule({
  declarations: [RolComponent, AddRolModalComponent, EditRolModalComponent, PrivilegiesDetailsModalComponent],
  imports: [
    PipesModule,
    TooltipModule,
    DirectivesModule,
    CommonModule,
    RolRoutingModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    MatPaginatorModule,
  ],
  providers: [
    {
      provide: MatPaginatorIntl,
      useClass: CustomMatPaginatorIntl,
    },
    DatePipe,
  ]
})
export class RolModule { }
