import { AlertService } from './../../../../service/alert.service';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import {Privileges, PrivilegesName, PrivilegesSend, Rol, RolesFilter, RolesText} from '../../model/rol';
import {formatDate} from '@angular/common';
import {RolService} from '../../services/rol.service';
import {HttpErrorResponse} from '@angular/common/http';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';
import {ENTERPRISE_CODE, ROLE} from '../../../login/models/login';


@Component({
  selector: 'app-add-rol-modal',
  templateUrl: './add-rol-modal.component.html',
  styleUrls: ['./add-rol-modal.component.scss']
})
export class AddRolModalComponent implements OnInit {

  @Input() modalRef: BsModalRef;
  @Output() executeAddRol: EventEmitter<any> = new EventEmitter();
  form: FormGroup;
  selectedItems: Privileges[] = [];
  items: RolesFilter[];
  spinner = false;
  privileges: Privileges[];
  enterprise_code: string;
  role: string;
  constructor(
    private fb: FormBuilder,
    private alert: AlertService,
    private rolService: RolService,
    private router: Router) {

  }
  ngOnInit() {
    this.enterprise_code = localStorage.getItem(ENTERPRISE_CODE);
    this.role = localStorage.getItem(ROLE);
    this.setPrivilegies();
    this.createForm();
  }

  setPrivilegies() {
    this.rolService.getAllPrivileges(this.enterprise_code).subscribe(data => {
        console.log({privileges: data});
        if (data && data.elements && data.elements.length > 0) {
          this.privileges = this.role !== RolesText.admin_holding ?
            data.elements.filter(f => f.description !== PrivilegesName.admin) : data.elements;
        }
      },
      (error: HttpErrorResponse) => {
       });
  }

  // Formularios validos
  get nameValid() {
    return this.form.get('name').valid && this.form.get('name').touched;
  }
  // get functionValid() {
  //   return this.form.get('function').valid && this.form.get('function').touched;
  // }

  // Formularios invalidos
  get nameInvalid() {
    return this.form.get('name').invalid && this.form.get('name').touched;
  }

  handelCancel() {
    this.modalRef.hide();
  }

  createForm() {
    this.form = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      function: [[], [Validators.required]],
      description: ['', []]
    });
  }


  onSelecetedRol($event: any) {
    const id = $event.target.value;

    if (this.selectedItems.find((x) => x.id === +id)) {
      this.form.controls['function'].setValue(this.selectedItems.map((x) => x.id));
      return;
    }
    this.selectedItems.push(this.privileges.find((x) => x.id === +id));
  }

  removeAll() {
    this.selectedItems = [];
    this.form.get('function').setValue('');
  }

  removeRoles(id: number) {
    this.selectedItems = this.selectedItems.filter((x) => x.id !== id);

    if (this.selectedItems.length === 0) {
      this.form.get('function').setValue('');
    }
  }

  handelSave() {
    this.form.controls['function'].setValue(this.selectedItems.map((x) => x.id));
    const created = formatDate( new Date(), `yyyy-MM-dd'T'HH:mm:ssZ`, 'en');
    const form = this.form.value;
    const privileges = this.selectedItems && this.selectedItems.length > 0 ?
      this.selectedItems.map(
        (p: any) =>
          <PrivilegesSend> {
            name: p.description,
            description: p.description,
            active: p.active,
            id: p.id,
            created: p.created,
            updated: p.updated
          }
      ) : [];

    const newRol: Rol = {
      message: true,
      active: true,
      description: form.description,
      name: form.name,
      privileges: privileges,
    };

    this.spinner = true;

    this.rolService.createRol(newRol, this.enterprise_code).subscribe((data) => {
      console.log(data);
      if ( data.status === '200') {
        Swal.fire({
          icon: 'success',
          title: 'Correcto',
          text: 'Se ha registrado el rol exitosamente.',
          timer: 2000,
          showCancelButton: false,
          showConfirmButton: false
        });
        this.spinner = false;
        this.handelCancel();
        this.executeAddRol.emit();

      } else if (data && data.status === '400') {
        this.spinner = false;
        this.handelCancel();
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: data.message,
          timer: 3000,
          showCancelButton: false,
          showConfirmButton: false
        });
      } else {
        this.spinner = false;
        this.handelCancel();
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'No ha registrado el rol.',
          timer: 2000,
          showCancelButton: false,
          showConfirmButton: false
        });
      }
    }, (err) => {
      console.log({errrrr: err});
      this.spinner = false;
      this.handelCancel();
      this.alert.error({
        title: 'Error',
        text: 'Ha ocurrido un error, por favor vuelva a intentarlo más tarde.'
      });
    });
  }

}
