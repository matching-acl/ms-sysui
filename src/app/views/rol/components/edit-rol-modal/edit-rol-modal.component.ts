import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Privileges, PrivilegesName, PrivilegesSend, Rol, RolesFilter, RolesText} from '../../model/rol';
import {AlertService} from '../../../../service/alert.service';
import {RolService} from '../../services/rol.service';
import {Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {formatDate} from '@angular/common';
import Swal from 'sweetalert2';
import {ENTERPRISE_CODE, ROLE} from '../../../login/models/login';

@Component({
  selector: 'app-edit-rol-modal',
  templateUrl: './edit-rol-modal.component.html',
  styleUrls: ['./edit-rol-modal.component.scss']
})
export class EditRolModalComponent implements OnInit {

  @Input() modalRef: BsModalRef;
  @Input() dataEdit: Rol;
  @Output() executeEditRol: EventEmitter<any> = new EventEmitter();
  form: FormGroup;
  selectedItems: Privileges[] = [];
  items: RolesFilter[];
  spinner = false;
  privileges: Privileges[];
  enterprise_code: string;
  role: string;
  constructor(
    private fb: FormBuilder,
    private alert: AlertService,
    private rolService: RolService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.enterprise_code = localStorage.getItem(ENTERPRISE_CODE);
    this.role = localStorage.getItem(ROLE);
    this.selectedItems = this.dataEdit && this.dataEdit.privileges ? this.dataEdit.privileges : [];
    this.createForm();
    this.rolService.getAllPrivileges(this.enterprise_code).subscribe(data => {
        console.log({privilegeeess: data});
        if (data && data.elements && data.elements.length > 0) {
          this.privileges = this.role !== RolesText.admin_holding ?
            data.elements.filter(f => f.description !== PrivilegesName.admin) : data.elements;
        }
      },
      (error: HttpErrorResponse) => {
      });
  }

  createForm() {
    this.form = this.fb.group({
      name: [this.dataEdit && this.dataEdit.name ? this.dataEdit.name : '', [Validators.required, Validators.minLength(3)]],
      function: [null, []],
      description: [this.dataEdit && this.dataEdit.description ? this.dataEdit.description : '', []]
    });
  }
  // Formularios validos
  get nameValid() {
    return this.form.get('name').valid && this.form.get('name').touched;
  }

  // Formularios invalidos
  get nameInvalid() {
    return this.form.get('name').invalid && this.form.get('name').touched;
  }

  handelCancel() {
    this.modalRef.hide();
  }

  removeAll() {
    this.selectedItems = [];
    this.form.get('function').setValue('');
  }

  removeRoles(id: number) {
    this.selectedItems = this.selectedItems.filter((x) => x.id !== id);

    if (this.selectedItems.length === 0) {
      this.form.get('function').setValue('');
    }
  }

  onSelecetedRol($event: any) {
    const id = $event.target.value;

    if (this.selectedItems.find((x) => x.id === +id)) {
      this.form.controls['function'].setValue(this.selectedItems.map((x) => x.id));
      return;
    }
    this.selectedItems.push(this.privileges.find((x) => x.id === +id));
  }

  handelSave() {
    this.form.controls['function'].setValue(this.selectedItems.map((x) => x.id));
    const created = formatDate( new Date(), `yyyy-MM-dd'T'HH:mm:ssZ`, 'en');
    const form = this.form.value;
    const privileges = this.selectedItems && this.selectedItems.length > 0 ?
      this.selectedItems.map(
        (p: any) =>
          <PrivilegesSend> {
            name: p.description,
            description: p.description,
            active: p.active,
            id: p.id,
            created: p.created,
            updated: p.updated
          }
      ) : [];

    const updateRol: Rol = {
      id: this.dataEdit && this.dataEdit.id ? this.dataEdit.id : 0,
      message: true,
      active: true,
      description: form.description,
      name: form.name,
      privileges: privileges,
    };

    this.spinner = true;

    this.rolService.updateRol(updateRol, this.enterprise_code).subscribe((data) => {
      console.log(data);
      if (data == null) {
        Swal.fire({
          icon: 'success',
          title: 'Correcto',
          text: 'Se ha actualizado el rol exitosamente.',
          timer: 2000,
          showCancelButton: false,
          showConfirmButton: false
        });
        this.spinner = false;
        this.handelCancel();
        this.executeEditRol.emit();
      } else if (data && data.status === '400') {
        this.spinner = false;
        this.handelCancel();
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: data.message,
          timer: 3000,
          showCancelButton: false,
          showConfirmButton: false
        });
      } else {
        this.spinner = false;
        this.handelCancel();
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'No ha actualizado el rol.',
          timer: 3000,
          showCancelButton: false,
          showConfirmButton: false
        });
      }
    },
      (error: HttpErrorResponse) => {
        this.spinner = false;
        this.handelCancel();
        this.alert.error({
          title: 'Error',
          text: 'Ha ocurrido un error, por favor vuelva a intentarlo más tarde.'
        });
      });
  }
}
