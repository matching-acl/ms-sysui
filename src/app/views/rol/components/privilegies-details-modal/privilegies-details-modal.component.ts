import {Component, Input, OnInit} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {Privileges} from '../../model/rol';

@Component({
  selector: 'app-privilegies-details-modal',
  templateUrl: './privilegies-details-modal.component.html',
  styleUrls: ['./privilegies-details-modal.component.scss']
})
export class PrivilegiesDetailsModalComponent implements OnInit {
  @Input() modal_ref: BsModalRef;
  @Input() privileges: Privileges[];
  constructor() { }

  ngOnInit(): void {
    console.log(this.privileges);
  }

  handelCancel() {
    this.modal_ref.hide();
  }

}
