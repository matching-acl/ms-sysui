import { RolService } from './services/rol.service';
import {Component, OnInit, TemplateRef} from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { HttpErrorResponse } from '@angular/common/http';
import { IsLoadingService } from '@service-work/is-loading';
import {Privileges, Rol} from './model/rol';
import Swal from 'sweetalert2';
import {ENTERPRISE_CODE, ROLE} from '../login/models/login';
import {PageEvent} from '@angular/material/paginator';

@Component({
  selector: 'app-rol',
  templateUrl: './rol.component.html',
  styleUrls: ['./rol.component.scss']
})
export class RolComponent implements OnInit {
  noData = false;
  noDataMessage = 'No se encontraron registros';
  modalRef: BsModalRef;
  dataSource: Rol[];
  privileges: Privileges[];
  dataEdit: Rol;
  enterprise_code: string;
  totalItems: number;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [1, 5, 10, 25];
  showFirstLastButtons = true;
  constructor(
    private modalService: BsModalService,
    private rolService: RolService,
    private loadingService: IsLoadingService) { }

  ngOnInit(): void {
    this.enterprise_code = localStorage.getItem(ENTERPRISE_CODE);
    this.getAllRoles();
  }

  getAllRoles(pageIndex?: number, pageSize?: number) {
    this.totalItems = 0;
      this.loadingService.add(
       this.rolService.getAllRoles(this.enterprise_code, pageIndex, pageSize).subscribe(data => {
         console.log(data);
         if (data && data.elements) {
            this.dataSource = data.elements;
            this.totalItems = data.totalElements;
         } else {
           this.noData = true;
         }
       },
       (error: HttpErrorResponse) => {
         console.log({error: error});
         this.noData = true;
         this.noDataMessage = 'Error de servidor, favor intentelo nuevamente.';
       })
     );
  }

  handlePageEvent(event: PageEvent) {
    this.totalItems = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.getAllRoles(this.pageIndex, this.pageSize);
  }

  getRowValue(value: Privileges[]): string {
    let returnValue = '';
    if (value && value.length > 0) {
      value.forEach((element, index) => {
        returnValue += index === value.length - 1 ? element.description : element.description + ',' + ' ';
      });
    }
    return returnValue;
  }

  openModal(template: TemplateRef<any>, privileges: any) {
    this.privileges = privileges;
    this.modalRef = this.modalService.show(template);
    this.modalRef.setClass('modal-xs');
  }

  openModalCreate(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    this.modalRef.setClass('modal-lg');
  }
  openModalEdit(template: TemplateRef<any>, data: Rol) {
    this.modalRef = this.modalService.show(template);
    this.modalRef.setClass('modal-lg');
    this.dataEdit = data;
  }

  openUpdateActiveRol(id: number, active: boolean) {
    this.loadingService.add(
      this.rolService.updateActiveRol(id, active, this.enterprise_code).subscribe((data) => {
        if (data && data.status === '200') {
          setTimeout(() => {
            Swal.fire({
              icon: 'success',
              title: 'Correcto',
              text: 'Se ha actualizado el estado del rol correctamente.',
              timer: 3000,
              showCancelButton: false,
              showConfirmButton: false
            });
            this.getAllRoles();
          }, 1500);
        } else {
          setTimeout(() => {
            Swal.fire({
              icon: 'error',
              title: 'Error',
              text: 'No se ha podido actualizar el estado del rol.',
              timer: 3000,
              showCancelButton: false,
              showConfirmButton: false
            });
          }, 1500);
        }
      }, ( err ) => {
        setTimeout(() => {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'No se ha podido actualizar el estado del rol.',
            timer: 3000,
            showCancelButton: false,
            showConfirmButton: false
          });
        }, 1500);
      }));
  }

  isRolLog(rol_name: string) {
    return rol_name === localStorage.getItem(ROLE);
  }
}
