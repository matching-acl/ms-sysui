import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { UserService } from './services/user.service';
import {Rol, User} from './model/user';
import { IsLoadingService } from '@service-work/is-loading';
import { HttpErrorResponse } from '@angular/common/http';
import {ENTERPRISE_CODE, ROLE, USER_NAME} from '../login/models/login';
import Swal from 'sweetalert2';
import {RolesNumber, RolesText} from '../rol/model/rol';
import {PageEvent} from '@angular/material/paginator';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  user: User;
  noData = false;
  noDataMessage = 'No se encontraron registros';
  modalRef: BsModalRef;
  dataSource: User[];
  enterprise_code: string;
  roles: Rol[];

  totalItems: number;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [1, 5, 10, 25];
  showFirstLastButtons = true;

  constructor(
    private modalService: BsModalService,
    private userService: UserService,
    private loadService: IsLoadingService,
    private loadingService: IsLoadingService) {}

  ngOnInit() {
    this.enterprise_code = localStorage.getItem(ENTERPRISE_CODE);
    this.getAllUser();
    this.getAllRoles();
  }

  getAllUser(pageIndex?: number, pageSize?: number) {
    this.totalItems = 0;
    this.loadService.add(
      this.userService.getAllUsers(this.enterprise_code, pageIndex, pageSize).subscribe(data => {
        if (data && data.elements) {
          this.dataSource = data.elements;
          this.totalItems = data.totalElements;
          console.log(this.dataSource);
        } else {
          this.noData = true;
        }
      },
        (error: HttpErrorResponse) => {
          this.noData = true;
          this.noDataMessage = 'Error de servidor, favor intentelo nuevamente.';
        })
    );
  }

  handlePageEvent(event: PageEvent) {
    this.totalItems = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.getAllUser(this.pageIndex, this.pageSize);
  }

  getAllRoles() {
    if (this.enterprise_code) {
      this.userService.getAllRolesActive(this.enterprise_code).subscribe((data: any) => {
        if (data && data.elements) {
          this.roles = data.elements;
        }
    });
  }
}
  hiddenEdit(role: number) {
    const user_role = localStorage.getItem(ROLE);
    return (user_role !== RolesText.admin_holding && role === RolesNumber.admin_holding) ||
      (user_role !== RolesText.admin && user_role !== RolesText.admin_holding && role === RolesNumber.admin);
  }

  isUserLog(user_name: string, role: number) {
    return user_name === localStorage.getItem(USER_NAME) || role === RolesNumber.admin_holding;
  }

  openModalCreate(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    this.modalRef.setClass('modal-lg');
  }

  openModalEdit(template: TemplateRef<any>, data: User) {
    this.modalRef = this.modalService.show(template);
    this.modalRef.setClass('modal-lg');
    this.user = data;
    console.log({userrr: this.user});
  }

  openUpdateActiveUser(id: number, active: boolean) {
    this.loadingService.add(
      this.userService.enableDisableUser(id, active, this.enterprise_code).subscribe((data) => {
        console.log({data: data});
        setTimeout(() => {
          Swal.fire({
            icon: 'success',
            title: 'Correcto',
            text: 'Se ha actualizado el estado del usuario correctamente.',
            timer: 3000,
            showCancelButton: false,
            showConfirmButton: false
          });
          this.getAllUser();
        }, 1500);
      }, ( err ) => {
        setTimeout(() => {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'No se ha podido actualizar el estado del usuario.',
            timer: 3000,
            showCancelButton: false,
            showConfirmButton: false
          });
        }, 1500);
      }));
  }

}
