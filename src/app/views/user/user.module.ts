import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AddUserModalComponent } from './components/add-user-modal/add-user-modal.component';
import { EditUserModalComponent } from './components/edit-user-modal/edit-user-modal.component';
import { AppBreadcrumbModule } from '@coreui/angular';
import { DirectivesModule } from '../../directives/directives.module';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PipesModule } from '../../pipes/pipes.module';
import {MatPaginatorIntl, MatPaginatorModule} from '@angular/material/paginator';
import {CustomMatPaginatorIntl} from '../paginator-es';


@NgModule({
  declarations: [UserComponent, AddUserModalComponent, EditUserModalComponent],
  imports: [
    PipesModule,
    TooltipModule,
    DirectivesModule,
    CommonModule,
    UserRoutingModule,
    ReactiveFormsModule,
    AppBreadcrumbModule,
    ModalModule.forRoot(),
    MatPaginatorModule,
  ],
  providers: [
    {
      provide: MatPaginatorIntl,
      useClass: CustomMatPaginatorIntl,
    },
    DatePipe,
  ]
})
export class UserModule { }
