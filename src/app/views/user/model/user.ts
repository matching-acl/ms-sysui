export interface User {
  firstName: string;
  active: boolean;
  email: string;
  emailVerified: boolean;
  createdDate: string;
  created: string;
  updated: string;
  username?: string;
  password?: string;
  recoveryCode?: string;
  role?: number;
  createdBy: string;
  id: number;
  lastName: string;
  schemaName: string;
  schemasRoles: SchemaRoles[];
}

export class SchemaRoles {
  code: string;
  schemaName: string;
  roles: Roles;
}

export class Roles {
  roles: Rol[];
}

export class Rol {
  id?: number;
  name?: string;
}
export class Enterprise {
  id?: number;
  code?: string;
  name?: string;
}


export interface SchemasRole {
  role: number;
  schemaName: string;
}


