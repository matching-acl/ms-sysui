import { UserService } from '../../services/user.service';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import {FormGroup, FormBuilder, Validators, FormArray} from '@angular/forms';
// @ts-ignore
import {Rol, User, Enterprise, Roles} from '../../model/user';
// @ts-ignore
import {ENTERPRISE_CODE, ROLE, USER_NAME} from '../../../login/models/login';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';
// @ts-ignore
import {RolesNumber, RolesText} from '../../../rol/model/rol';
import {AlertService} from '../../../../service/alert.service';
import {EnterpriseService} from '../../../enterprise/services/enterprise.service';

@Component({
  selector: 'app-edit-user-modal',
  templateUrl: './edit-user-modal.component.html',
  styleUrls: ['./edit-user-modal.component.scss']
})
export class EditUserModalComponent implements OnInit {
  @Input() modalRef: BsModalRef;
  @Input() userData: User;
  @Input() roless: Rol[];
  rolesAdd = new Array<Roles>();
  @Output() executeEditUser: EventEmitter<any> = new EventEmitter();
  roles: Rol;
  form: FormGroup;
  spinner = false;
  enterprise_code: string;
  user_name_login: string;
  user_login: boolean;
  role: string;
  step1 = true;
  step2 = false;
  hiddenButton = false;
  enterprise: Enterprise[] = [];
  enterprise_selected = new Array<string>();
  schemaRoles = null;
  emailDiferent = false;
  hidePassword = true;
  hideSecurityCode = true;
  user: string;
  constructor(
    private userService: UserService,
    private fb: FormBuilder,
    private router: Router,
    private alert: AlertService,
    private enterpriseService: EnterpriseService) { }

  ngOnInit(): void {
    this.user_name_login = localStorage.getItem(USER_NAME);
    this.enterprise_code = localStorage.getItem(ENTERPRISE_CODE);
    this.user = localStorage.getItem('USER_NAME');
    this.role = localStorage.getItem(ROLE);
    this.user_login = this.userData && this.userData.username && this.userData.username === this.user_name_login;

    this.formEdit();
    this.getEnterprise();
  }

    disabled(item: Rol) {
     return item.name === RolesText.admin && this.role !== RolesText.admin_holding;
  }

  get hiddenAdminHolding() {
    return this.role === RolesText.admin_holding && this.userData && this.userData.role === RolesNumber.admin_holding;
  }

  disabledEnterprise(item: Enterprise) {
    return this.enterprise_selected.find(f => f === item.code);
  }

  getEnterprise() {
    this.enterpriseService.getEnterprisesByUser(this.user).subscribe(data => {
      if (data && data.enterprises && data.enterprises.elements) {
        this.enterprise = data.enterprises.elements;
      }
    });
  }

  onSelectEnterprise($event, items: any) {
    const code = $event;
    this.userService.getAllRolesActive(code).subscribe((data: any) => {
      if (data && data.elements && data.elements.length > 0 ) {
        let roles_add = data.elements;
        roles_add = this.role !== RolesText.admin_holding ? roles_add.filter(f => f.name !== RolesText.admin) : roles_add;
        this.rolesAdd.push({roles: roles_add});
      } else {
        this.rolesAdd.push({roles: []});
      }
    });
    this.setEnterpriseSelected();
  }

  setEnterpriseSelected() {
    const shemas = this.form.get('schemasRoles').value;
    let i = 0;
    const enterprise_add = [this.enterprise_code];
    if (shemas && shemas.length > 0) {
      shemas.forEach( f => {
        if ( i > 0) {
          enterprise_add.push(f.schemaName);
        }
        i++;
      } );
    }
    console.log({enterprise_selecteddd: this.enterprise_selected});
    this.enterprise_selected = enterprise_add;
  }

  formEdit() {
    console.log({email: this.userData.email, userData: this.userData});
    this.form = this.fb.group({
      username: [this.userData && this.userData.username ? this.userData.username : '', [Validators.required, Validators.minLength(3)]],
      email: [this.userData && this.userData.email ? this.userData.email : '', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      password: [{value: this.userData && this.userData.password ? this.userData.password : '',
        disabled: this.userData && !this.userData.emailVerified},
        [Validators.minLength(8)]],
      security_code: [{value: this.userData && this.userData.recoveryCode ? this.userData.recoveryCode : '',
        disabled: this.userData && !this.userData.emailVerified},
        [Validators.minLength(5)]],
      firstName: [this.userData && this.userData.firstName ? this.userData.firstName : '', [Validators.minLength(3)]],
      lastName: [this.userData && this.userData.lastName ? this.userData.lastName : '', [Validators.minLength(3)]],
      schemasRoles: this.fb.array([])
    });

    const control = <FormArray>this.form.controls['schemasRoles'];
    this.schemaRoles = this.userData.schemasRoles;
    if (this.schemaRoles && this.schemaRoles.length > 0) {
      console.log({schemaRolessss: this.schemaRoles});
      let i = 0;
      this.schemaRoles.forEach(f => {
          control.push(this.fb.group({
            role: [{value: f.roles[0].id, disabled: i === 0 && this.role === RolesText.admin}],
            schemaName: [{value: f.code, disabled: i === 0}, []]
          }));
          i++;
        this.rolesAdd.push({
          roles: (this.role !== RolesText.admin_holding && this.role !== RolesText.admin) ?
            f.roles.filter(r => r.name !== RolesText.admin) : f.roles
        });
        console.log({rolesAdd: this.rolesAdd});
        this.enterprise_selected.push(f.code);
      });
    } else {
      control.push(this.fb.group({
        role: [{value: null, disabled: true}],
        schemaName: [{value: this.enterprise_code, disabled: true}, []]
      }));
      this.enterprise_selected.push(this.enterprise_code);
    }
  }

  get getSchemasRoles() {
    return this.form.get('schemasRoles') as FormArray;
  }

  // formularios validos
  get usernameValid() {
    return this.form.get('username').valid && this.form.get('username').touched;
  }
  get emailValid() {
    return this.form.get('email').valid && this.form.get('email').touched;
  }
  get nameValid() {
    return this.form.get('firstName').valid && this.form.get('firstName').touched;
  }
  get lastNameValid() {
    return this.form.get('lastName').valid && this.form.get('lastName').touched;
  }
 // Formulario inválidos
  get usernameInvalid() {
    const username = this.form.get('username').value;
    return username && username.length === 0 && this.form.get('username').invalid && this.form.get('username').touched;
  }
  get usernameInvalidLength() {
    const name = this.form.get('username').value;
    return name && name.length > 0 && this.form.get('username').errors && (this.form.get('username').touched);
  }
  get emailInvalid() {
    const email = this.form.get('email').value;
    if (email && email !== '') {
      return this.form.get('email').invalid && this.form.get('email').touched;
    } return false;
  }
  get emailRequired() {
    return (this.form.get('email').value === '' || this.form.get('email').value === null) && this.form.get('email').touched;
  }
  get passwordInvalid() {
    const pass = this.form.get('password').value;
    if (pass && pass !== '') {
      return (pass.length === 0 &&  this.form.get('password').invalid && this.form.get('password').touched) ;
    } return false;
  }

  get passwordInvalidLength() {
    const pass = this.form.get('password').value;
    if (pass && pass !== '') {
      return  pass.length > 0 && this.form.get('password').errors && this.form.get('password').touched;
    } return false;
  }
  get securityCodeInvalidLength() {
    const securityCode = this.form.get('security_code').value;
    if (securityCode && securityCode !== '') {
      return securityCode.length > 0 && this.form.get('security_code').errors && this.form.get('security_code').touched;
    } return false;
  }

  get comparePassUserName() {
    const value = this.form.get('password').value;
    if (value && value !== '') {
      return  value.length >= 8 && this.form.get('username').value === this.form.get('password').value;
    } return false;
  }
  get comparePassUser() {
    const firstName = this.form.get('firstName').value;
    const pass = this.form.get('password').value;
    if (firstName && pass && firstName !== '' && pass !== '') {
      return firstName.length >= 8 && this.form.get('firstName').valid && this.form.get('firstName').touched &&
        firstName === pass && this.form.get('password').touched;
    } return false;
  }

  get nameInvalid() {
    const firstName = this.form.get('firstName').value;
    if (firstName && firstName !== '') {
      return firstName.length === 0 && this.form.get('firstName').invalid && this.form.get('firstName').touched;
    } return false;
  }
  get nameInvalidLength() {
    const name = this.form.get('firstName').value;
    if (name && name !== '') {
      return  name.length > 0 && this.form.get('firstName').errors && this.form.get('firstName').touched;
    } return false;
  }
  get lastNameInvalid() {
    const name = this.form.get('lastName').value;
    if (name && name !== '') {
      return name.length === 0 && this.form.get('lastName').invalid && this.form.get('lastName').touched;
    } return false;
  }
  get lastNameInvalidLength() {
    const lastName = this.form.get('lastName').value;
    if (lastName && lastName !== '') {
      return  lastName.length > 0 && this.form.get('lastName').errors && this.form.get('lastName').touched;
    } return false;
  }

  enterpriseInvalid(i: number) {
    const schemasRoles = this.form.get('schemasRoles').value;
    return schemasRoles && schemasRoles.length > 0 && schemasRoles[i] && schemasRoles[i].schemaName === null;
  }

  rolInvalid(i: number) {
    const schemasRoles = this.form.get('schemasRoles').value;
    return schemasRoles && schemasRoles.length > 0 && schemasRoles[i] && schemasRoles[i].role === null;
  }

  get allEnterpriseInvalid() {
    const schemasRoles = this.form.get('schemasRoles').value;
    return schemasRoles && schemasRoles.length > 0 && schemasRoles.find(f => f.schemaName === null);
  }

  get allRolInvalid() {
    const schemasRoles = this.form.get('schemasRoles').value;
    return schemasRoles.find(f => f.role === null);
  }

  editForm() {
    this.spinner = true;
    this.hiddenButton = true;
    setTimeout(() => {
      this.spinner = false;
      this.hiddenButton = false;
    }, 4000);
    const arreglo = [];
    const schemas = this.form.get('schemasRoles').value;
    console.log({schemasssssssssss: schemas});
    let request = {};
    for (const iterator of schemas) {
      request = {
        role: +iterator.role,
        schemaName: iterator.schemaName,
      };
      arreglo.push(request);
    }
    if (arreglo && arreglo.length > 0 ) {
      if (this.enterprise_code) {
        arreglo[0].schemaName = this.enterprise_code;
      }
    }
    console.log({ARREGLOO_SAVE:  arreglo, ARREGLO: this.userData.schemasRoles});
    const form = this.form.value;
    const body = {
      active: true,
      email: form.email,
      emailVerified: this.userData && this.userData.emailVerified !== null ? this.userData.emailVerified : false,
      firstName: form.firstName,
      id: this.userData.id,
      lastName: form.lastName,
      password: this.userData && !this.userData.emailVerified ? this.userData.password : form.password,
      recoveryCode: this.userData && !this.userData.emailVerified ? this.userData.recoveryCode : form.security_code,
      username: form.username,
      oldUserName: this.userData && this.userData.username ? this.userData.username : '',
      schemasRoles: !this.hiddenAdminHolding ? arreglo : null
    };
    const changeSchemasRoles = this.changesSchemasRoles(arreglo);
    const redirect_login = (this.user_login && (form.username !== this.userData.username || form.password !== this.userData.password ||
      this.EmailDiferent || !this.hiddenAdminHolding && changeSchemasRoles ));
    console.log({redirect_login: redirect_login});
       this.spinner = true;
       this.form.disable();
    this.userService.updateUser(body, this.enterprise_code).subscribe(data => {
      console.log(data);
     if (data === null) {
        if (redirect_login) {
          setTimeout(() => {
            Swal.fire({
              icon: 'info',
              title: 'Correcto',
              text: 'Al cambiar el nombre de usuario, el email, la contraseña o las empresas y sus roles del usuario logueado se redireccionará al login.',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Aceptar'
            }).then((result) => {
              if (result.isConfirmed) {
                this.spinner = false;
                this.form.enable();
                this.handelCancel();
                this.router.navigateByUrl('/login');
              }
            });
          });
        } else {
          Swal.fire({
            icon: 'success',
            title: 'Correcto',
            text: 'Usuario actualizado correctamente.',
            timer: 3000,
            showCancelButton: false,
            showConfirmButton: false
          });
          this.spinner = false;
          this.form.enable();
          this.handelCancel();
          this.executeEditUser.emit();
        }
      } else if (data && data.status === '400') {
        this.alert.error({
          title: 'Error',
          text: data.message
        });
      }
      this.spinner = false;
      this.handelCancel();
    }, (err) => {
      this.spinner = false;
      this.handelCancel();
      this.alert.error({
        title: 'Error',
        text: 'Ha ocurrido un error, por favor vuelva a intentarlo más tarde.'
      });
    });
  }
  changesSchemasRoles(arreglo: any) {
    let changeSchemasRoles = false;
    const userSchemasRoles = this.userData.schemasRoles;
    if (arreglo.length !== userSchemasRoles.length) {
      changeSchemasRoles = true;
    } else {
      const changeSchemas = userSchemasRoles.filter(d => arreglo.every(a => d.code !== a.schemaName));
      const changeRole = userSchemasRoles.filter(d => arreglo.every(a => d.roles[0].id !== a.role));
      console.log({changeSchemas: changeSchemas, changeRole: changeRole});
      changeSchemasRoles = changeSchemas && changeSchemas.length > 0 || changeRole && changeRole.length > 0;
    }
    return changeSchemasRoles;
  }
  add() {
    const control = <FormArray>this.form.controls['schemasRoles'];
    control.push(this.fb.group({
      role: [],
      schemaName : []
    }));
  }

  removeSchemasRoles(index: number, item: any) {
    const control = <FormArray>this.form.controls['schemasRoles'];
    console.log({control: control.value[index]});
    if (control && control.value && control.value[index] && control.value[index].schemaName) {
      console.log({controlSSSS: control.value[index], controlSSSSchemaName: control.value[index].schemaName});
      const index_enterprise = this.enterprise_selected.findIndex(f => f === control.value[index].schemaName);
      this.enterprise_selected.splice(index_enterprise, 1);
      this.rolesAdd.splice(index_enterprise, 1);
    }

    control.removeAt(index);
    console.log({enterprise_selectedALDELETE: this.enterprise_selected, item: item});
  }
  handelCancel() {
    this.modalRef.hide();
  }

  nextPrev(step: number) {
    this.step1 = step === -1;
    this.step2 = step === 1;
  }

  disabledNext() {
    const username = this.form.get('username').value;
    const email = this.form.get('email').value;
    const pass =  this.form.get('password').value;
    const securityCode = this.form.get('security_code').value;

    return (this.step1 && (username === '' || email === '' || this.usernameInvalid || this.usernameInvalidLength ||
      ((!this.EmailDiferent || this.userData && this.userData.emailVerified) && (
        this.passwordInvalid || this.securityCodeInvalidLength) ||
      this.comparePassUserName || this.passwordInvalidLength || this.emailInvalid)) ) ||
      (this.step2 && !this.hiddenAdminHolding && (this.allEnterpriseInvalid || this.allRolInvalid));
  }

  get EmailDiferent() {
    return this.emailDiferent;
  }

  changeEmail() {
    if (this.userData.email !== this.form.controls['email'].value) {
      this.form.controls['password'].disable();
      this.form.controls['security_code'].disable();
      this.emailDiferent = true;
    } else {
      this.form.controls['password'].enable();
      this.form.controls['security_code'].enable();
      this.emailDiferent = false;
    }
  }
}
