import { AlertService } from './../../../../service/alert.service';
import { UserService } from './../../services/user.service';
import {Component, EventEmitter, Input, OnInit, Output, TemplateRef} from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { IsLoadingService } from '@service-work/is-loading';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { RolService } from '../../../rol/services/rol.service';
import {Rol, Enterprise, Roles} from '../../model/user';
import { EnterpriseService } from '../../../enterprise/services/enterprise.service';
import {ENTERPRISE_CODE, ROLE} from '../../../login/models/login';
import {RolesText, RolesNumber} from '../../../rol/model/rol';

@Component({
  selector: 'app-add-user-modal',
  templateUrl: './add-user-modal.component.html',
  styleUrls: ['./add-user-modal.component.scss']
})
export class AddUserModalComponent implements OnInit {
  @Input() modalRef: BsModalRef;
  @Input() roless: Rol[];
  rolesAdd = new Array<Roles>();
  @Output() executeAddUser: EventEmitter<any> = new EventEmitter();
  body = {};
  hiddenButton = false;
  spinner = false;
  formModal: FormGroup;
  step1 = true;
  step2 = false;
  step3 = false;
  enterprise: Enterprise[];
  enterprise_selected = new Array<string>();
  enterprise_code: string;
  role: string;
  user: string;
  constructor(
    private fb: FormBuilder,
    private loadingService: IsLoadingService,
    private modalService: BsModalService,
    private rolService: RolService,
    private enterpriseService: EnterpriseService,
    private userService: UserService,
    private alert: AlertService
  ) {}

  ngOnInit(): void {
    this.enterprise_code = localStorage.getItem(ENTERPRISE_CODE);
    this.role = localStorage.getItem(ROLE);
    this.user = localStorage.getItem('USER_NAME');
    this.roless = this.role !== RolesText.admin_holding ? this.roless.filter(f => f.name !== RolesText.admin) : this.roless;
    this.rolesAdd.push({
      roles: this.roless
    });
    this.enterprise_selected.push(this.enterprise_code);
    console.log({rolesAdd: this.rolesAdd});
    this.createFormModal();
    this.getEnterprise();
  }

  disabledEnterprise(item: Enterprise) {
    return this.enterprise_selected.find(f => f === item.code);
  }

  getEnterprise() {
    this.enterpriseService.getEnterprisesByUser(this.user).subscribe(data => {
      if (data && data.enterprises && data.enterprises.elements) {
        this.enterprise = data.enterprises.elements;
      }
    });
  }

  onSelectEnterprise($event) {
    const code = $event;
    this.userService.getAllRolesActive(code).subscribe((data: any) => {
      if (data && data.elements && data.elements.length > 0 ) {
        console.log({TODOS_LOSS_ROLESS: data.elements, ADMIN_HOLDING: this.role !== RolesText.admin_holding});
        let roles_add = data.elements;
        roles_add = this.role !== RolesText.admin_holding ? roles_add.filter(f => f.name !== RolesText.admin) : roles_add;
        this.rolesAdd.push({roles: roles_add});
      } else {
        this.rolesAdd.push({roles: []});
      }
    });
    this.setEnterpriseSelected();
    console.log({rolesAddDDD: this.rolesAdd, enterprise_selectedddd: this.enterprise_selected});
  }

  setEnterpriseSelected() {
    const shemas = this.formModal.get('schemasRoles').value;
    let i = 0;
    const enterprise_add = [this.enterprise_code];
    if (shemas && shemas.length > 0) {
      shemas.forEach( f => {
        if ( i > 0) {
          enterprise_add.push(f.schemaName);
        }
        i++;
      } );
    }
    this.enterprise_selected = enterprise_add;
  }

  createFormModal(): void {
    this.formModal = this.fb.group({
      username: ['', [Validators.required, Validators.minLength(3)]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: [
        {
          value: '',
          disabled : true,
        },
        [Validators.required, Validators.minLength(8)],
      ],
      name: ['', [Validators.minLength(3)]],
      lastName: ['', [Validators.minLength(3)]],
      email: ['', [ Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      emailVerified : [true],
      schemasRoles: this.fb.array([this.fb.group({
        role: [null],
        schemaName : [{value: this.enterprise_code, disabled : true}]
      })])
    });
  }

  onChange() {
    if (this.formModal.get('password').valid && !this.comparePassUserName) {

      this.formModal.controls['confirmPassword'].enable();
    }
    if (this.comparePassUserName || this.formModal.get('password').invalid) {
      this.formModal.controls['confirmPassword'].disable();
    }
  }

  get getSchemasRoles() {
    return this.formModal.get('schemasRoles') as FormArray;
  }

  // formularios validos
  get nameValid() {
    return this.formModal.get('name').valid && this.formModal.get('name').touched;
  }
  get lastNameValid() {
    return this.formModal.get('lastName').valid && this.formModal.get('name').touched;
  }
  get emailValid() {
    return this.formModal.get('email').valid && this.formModal.get('email').touched;
  }
  get usernameValid() {
    return this.formModal.get('username').valid && this.formModal.get('username').touched;
  }
  get passwordValid() {
    return this.formModal.get('password').valid && this.formModal.get('password').touched ;
  }
  get confirmPasswordValid() {
    return this.formModal.get('confirmPassword').valid && this.formModal.get('confirmPassword').touched &&
      this.formModal.get('password').value === this.formModal.get('confirmPassword').value;

  }
  get enterpriseValid() {
    const schemasRoles = this.formModal.get('schemasRoles').value;
    return schemasRoles[0].schemaName !== null;
  }

  get rolValid() {
    const schemasRoles = this.formModal.get('schemasRoles').value;
    return schemasRoles[0].role !== null;
  }

  // Formularios invalidos
  get nameInvalid() {
    const name = this.formModal.get('name').value;
    return name.length === 0 && this.formModal.get('name').invalid && (this.formModal.get('name').touched );
  }
  get nameInvalidLength() {
    const name = this.formModal.get('name').value;
    return  name.length > 0 && this.formModal.get('name').errors && (this.formModal.get('name').touched );
  }
  get lastNameInvalid() {
    const name = this.formModal.get('lastName').value;
    return name.length === 0 && this.formModal.get('lastName').invalid && (this.formModal.get('lastName').touched );
  }
  get lastNameInvalidLength() {
    const lastName = this.formModal.get('lastName').value;
    return  lastName.length > 0 && this.formModal.get('lastName').errors && (this.formModal.get('lastName').touched );
  }
  get usernameInvalid() {
    const username = this.formModal.get('username').value;
    return username.length === 0 && this.formModal.get('username').invalid && this.formModal.get('username').touched;
  }
  get usernameInvalidLength() {
    const name = this.formModal.get('username').value;
    return  name.length > 0 && this.formModal.get('username').errors && (this.formModal.get('username').touched );
  }
  get emailInvalid() {
    return this.formModal.get('email').invalid && this.formModal.get('email').touched;
  }
  get emailRequired() {
    return (this.formModal.get('email').value === '' || this.formModal.get('email').value === null) && this.formModal.get('email').touched;
  }
  get passwordInvalid() {
    const pass = this.formModal.get('password').value;
    return (pass.length === 0 &&  this.formModal.get('password').invalid && this.formModal.get('password').touched) ;
  }

  get passwordInvalidLength() {
    const pass = this.formModal.get('password').value;
    return  pass.length > 0 && this.formModal.get('password').errors && this.formModal.get('password').touched;
  }
  get confirmPasswordInvalid() {
    const confirmPass = this.formModal.get('confirmPassword').value;
    return confirmPass.length === 0 && this.formModal.get('confirmPassword').value === '' && this.formModal.get('confirmPassword').invalid;
  }
  get passwordIsDifferent() {
    const confirmPass = this.formModal.get('confirmPassword').value;
    return confirmPass.length >= 1 && this.formModal.get('confirmPassword').touched &&
      this.formModal.get('password').value !== this.formModal.get('confirmPassword').value;
  }
  get comparePassUserName() {
    const value = this.formModal.get('password').value;
    return  value.length >= 8 && this.formModal.get('username').value ===  this.formModal.get('password').value;
  }
  get comparePassUser() {
    const value = this.formModal.get('name').value;
    return value.length >= 8 && this.formModal.get('name').valid && this.formModal.get('name').touched &&
      this.formModal.get('name').value ===  this.formModal.get('password').value && this.formModal.get('password').touched;
  }

  enterpriseInvalid(i: number) {
    const schemasRoles = this.formModal.get('schemasRoles').value;
    return schemasRoles[i].schemaName === null;
  }

  rolInvalid(i: number) {
    const schemasRoles = this.formModal.get('schemasRoles').value;
    return schemasRoles[i].role === null;
  }

  get allEnterpriseInvalid() {
    const schemasRoles = this.formModal.get('schemasRoles').value;
    return schemasRoles.find(f => f.schemaName === null);
  }

  get allRolInvalid() {
    const schemasRoles = this.formModal.get('schemasRoles').value;
    return schemasRoles.find(f => f.role === null);
  }

  get buttonDisabled() {
    return this.formModal.get('username').valid &&
      this.formModal.get('rol').valid &&
      this.formModal.get('password').valid &&
      this.formModal.get('confirmPassword').valid;
  }

  handelCancel() {
    this.modalRef.hide();
  }

  search() {
    this.spinner = true;

    setTimeout(() => {
      this.spinner = false;
    }, 2000);
  }

  handelSave() {
    this.spinner = true;
    this.hiddenButton = true;
    setTimeout(() => {
      this.spinner = false;
      this.hiddenButton = false;
    }, 4000);
    const arreglo = [];
    const shemas = this.formModal.get('schemasRoles').value;
    let request = {};
    for (const iterator of shemas) {
      request = {
            role: +iterator.role,
            schemaName: iterator.schemaName,
      };
      arreglo.push(request);
    }
    if (arreglo && arreglo.length > 0 ) {
      if (this.enterprise_code) {
        arreglo[0].schemaName = this.enterprise_code;
      }
    }
    console.log({ARREGLOO_SAVE:  arreglo});

    this.body = {
        username: this.formModal.get('username').value,
          firstName: this.formModal.get('name').value,
          lastName: this.formModal.get('lastName').value,
          email: this.formModal.get('email').value,
          status: 'status',
          message: true,
          active : true,
          emailVerified: true,
          schemasRoles: arreglo
      };
    console.log(this.body);

    this.loadingService.add(
      this.userService.createUser(this.body).subscribe(data => {
        console.log({DATAAA: data});
        if (data === null) {
          this.alert.success({
            title: 'Correcto',
            text: 'Registro de usuario exitoso ',
          });
          this.executeAddUser.emit();
          this.handelCancel();
        } else if (data && data.status === '400') {
          this.alert.error({
            title: 'Error',
            text: data.message
          });
          this.handelCancel();
        }
      }, (err) => {
        this.alert.error({
          title: 'Error',
          text: 'Ha ocurrido un error, por favor vuelva a intentarlo más tarde.'
        });
        this.handelCancel();
      }));
  }

  nextPrev(step: number) {
    this.step1 = step === -1;
    this.step2 = step === 1;
    this.step3 = step === 2;
  }

  add() {
    const control = <FormArray>this.formModal.controls['schemasRoles'];
    control.push(this.fb.group({
      role: [],
      schemaName : []
    }));
  }

  removeSchemasRoles(index: number, item: any) {
    const control = <FormArray>this.formModal.controls['schemasRoles'];
    if (control.value[index].schemaName) {
      console.log({controlSSSS: control.value[index], controlSSSSchemaName: control.value[index].schemaName});
      const index_enterprise = this.enterprise_selected.findIndex(f => f === control.value[index].schemaName);
      this.enterprise_selected.splice(index_enterprise, 1);
      this.rolesAdd.splice(index_enterprise, 1);
    }

    control.removeAt(index);
    console.log({enterprise_selectedALDELETE: this.enterprise_selected, item: item});
  }

  disabledNext() {
    const username = this.formModal.get('username').value;
    const email = this.formModal.get('email').value;
    return (this.step1 && (username === '' || email === '' || this.usernameInvalid || this.usernameInvalidLength || this.emailInvalid)) ||
      (this.step2 && (this.allEnterpriseInvalid || this.allRolInvalid)) ||
      (this.step3 && ( this.nameInvalidLength || this.lastNameInvalidLength));
  }

}
