import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  private BaseUrl = environment.coreApiUrl;
  username: any = 'admin';
  password: any = 'admin';
  authorization = 'Basic ' + btoa(this.username + ':' + this.password);

  headers(enterprise_code: string) {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization', this.authorization);
    if (enterprise_code) {
      header = header.set('xTenantId', enterprise_code);
    }
    return header;
  }

  getAllUsers(enterprise_code: string, pageIndex?: number, pageSize?: number): Observable<any> {
    const body = {
      'fields': {},
      'message': true,
      'page': pageIndex,
      'search': enterprise_code,
      'size': pageSize,
      'status': ''
    };

    return this.http.post<any>(this.BaseUrl + 'tenant/user/search', body, { headers: this.headers(enterprise_code)});
  }

  getAllRolesActive(code: string): Observable<any> {
    const body = {'fields': {'active': 'true'}, 'page': 0, 'size': 0, 'search': ''};
    return this.http.post<any>(this.BaseUrl + 'tenant/role/search', body,
      { headers: this.headers(code)});
  }

  createUser(data): Observable<any> {
    return this.http.post(this.BaseUrl + 'tenant/user', data, { headers: this.headers(null)});
  }

  updateUser(data, enterprise_code: string): Observable<any> {
    return this.http.put(this.BaseUrl + 'tenant/user', data, { headers: this.headers(enterprise_code)});
  }

  enableDisableUser(id: number, active: boolean, enterprise_code: string): Observable<any> {
    const body = {
      fieldValue: {active : active},
      id: id,
      message: true,
      status: ''
    };

    return this.http.patch(this.BaseUrl + 'tenant/user', body, { headers: this.headers(enterprise_code)});
  }

}
