import { ConfConciliation } from './../../config-conciliation/model/conf-conciliation';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {environment} from '../../../../environments/environment.prod';
import { Observable } from 'rxjs';
import { Report } from '../model/reports';


@Injectable({
  providedIn: 'root'
})
export class ReportsService {
  username: any = 'admin';
  password: any = 'admin';
  authorizationData = 'Basic ' + btoa(this.username + ':' + this.password);
  constructor(private http: HttpClient) { }
  
  
  getInterfaces(enterprise_code: string): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization',  'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
    const pr = {'is_enum': '0', 'model': 'interfaces', 'schema': enterprise_code};
    return this.http.post(environment.pythonApiUrl + 'read', pr, {headers: header});
    
  }
  
  getConciliation(enterprise_code: string): Observable<ConfConciliation[]> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization', 'Bearer md53b2b0eef781e8a6aaf7cf0565b780845');
    let params = new HttpParams();
    params = params.set('is_enum', '0');
    params = params.set('model', 'conf_conciliation');
    params = params.set('schema', enterprise_code);
    const filters = {
     'active' : true
    }
    const pr = {
      'model': 'conf_conciliation',
      'schema': enterprise_code,
      // 'filters' : filters
    };
    return this.http.post<ConfConciliation[]>(environment.pythonApiUrl + 'read', pr, {headers: header});
    
  }
  
  getReportsInterface(body : any): Observable<any>{
    let header = new HttpHeaders();
    header = header.set("Content-Type", "application/json");
    header = header.set("Authorization", this.authorizationData);
    return this.http.post(environment.apiReport + 'find_report_interfaces', body, { headers: header });
  }
  getReportsInterfaceOut(body : any): Observable<any>{
    let header = new HttpHeaders();
    header = header.set("Content-Type", "application/json");
    header = header.set("Authorization", this.authorizationData);
    return this.http.post(environment.apiReport + 'find_report_out_interfaces ', body, { headers: header });
  }
  getReportsConciliation(body : any): Observable<any>{
    let header = new HttpHeaders();
    header = header.set("Content-Type", "application/json");
    header = header.set("Authorization", this.authorizationData);
    return this.http.post(environment.apiReport + 'find_report_conciliation', body, { headers: header });
  }

  getTotalItemsConciliation(body : any): Observable<any>{
    let header = new HttpHeaders();
    header = header.set("Content-Type", "application/json");
    header = header.set("Authorization", this.authorizationData);
    return this.http.post(environment.apiReport + 'total_items_conciliation', body, { headers: header });
  }
  getTotalItemsInterfaces(body : any): Observable<any>{
    let header = new HttpHeaders();
    header = header.set("Content-Type", "application/json");
    header = header.set("Authorization", this.authorizationData);
    return this.http.post(environment.apiReport + 'total_items_interfaces', body, { headers: header });
  }

  downloadExcelConci(): Observable<any>{
    let header = new HttpHeaders();
    header = header.set("Content-Type", "application/json");
    header = header.set("Authorization", this.authorizationData);
    return this.http.get(environment.apiReport + 'export_conciliation_to_excel', { headers: header, responseType: 'blob' });
  }

  downloadExcelInter(): Observable<any>{
    let header = new HttpHeaders();
    header = header.set("Content-Type", "application/json");
    header = header.set("Authorization", this.authorizationData);
    return this.http.get(environment.apiReport + 'export_interfaces_to_excel', { headers: header, responseType: 'blob' });
  }
  
}
