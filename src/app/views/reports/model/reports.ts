export interface Report {
  total_trxs_conciliated: number;
  total_trxs_not_conciliated: Totaltrxsnotconciliated[];
  trxs: Trx[];
}

export interface Trx {
  status: string;
  time: string;
  interface_origin: string;
  interface_dest: string;
  update: string;
  keys_origin: string[];
  keys_dest: string[];
  details: Details;
  line_data: string;
}

export interface Details {
  RUT_CLIENTE: string;
  MONTO: number;
}

export interface Totaltrxsnotconciliated {
  name?: string;
  total_notC?: number;
}