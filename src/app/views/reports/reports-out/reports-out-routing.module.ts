import { AuthGuardService } from './../../login/service/auth-guard.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportsOutComponent } from './reports-out.component';

const routes: Routes = [
  { path: '', component: ReportsOutComponent, canActivate: [AuthGuardService] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsOutRoutingModule { }
