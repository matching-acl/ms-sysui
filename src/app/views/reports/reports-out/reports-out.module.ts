import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { ReportsOutRoutingModule } from './reports-out-routing.module';
import { ReportsOutComponent } from './reports-out.component';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';
import { PipesModule } from '../../../pipes/pipes.module';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ReportsInterfacesRoutingModule } from '../reports-interfaces/reports-interfaces-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomMatPaginatorIntl } from '../../paginator-es';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
defineLocale('es', esLocale);

@NgModule({
  declarations: [ReportsOutComponent],
  imports: [
    CommonModule,
    ReportsOutRoutingModule,
    MatPaginatorModule,
    PipesModule,
    TooltipModule,
    BsDatepickerModule,
    CollapseModule,
    ReportsInterfacesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
   providers: [
    {
      provide: MatPaginatorIntl,
      useClass: CustomMatPaginatorIntl,
    },
    DatePipe,
  ]
})
export class ReportsOutModule { }
