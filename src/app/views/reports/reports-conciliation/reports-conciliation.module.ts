import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PipesModule } from './../../../pipes/pipes.module';
import { ReportsConciliationComponent } from './reports-conciliation.component';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ReportsConciliationRoutingModule } from './reports-conciliation-routing.module';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatPaginatorModule, MatPaginatorIntl } from '@angular/material/paginator';
import { CustomMatPaginatorIntl } from './../../paginator-es';
import { MatSelectModule } from '@angular/material/select';
import { ReportConciliationModalComponent } from './report-conciliation-modal/report-conciliation-modal.component';
// import {SelectDropDownModule} from ''
defineLocale('es', esLocale);

@NgModule({
  declarations: [ReportsConciliationComponent, ReportConciliationModalComponent],
  imports: [
    MatSelectModule,
    MatPaginatorModule,
    CommonModule,
    PipesModule,
    TooltipModule,
    BsDatepickerModule,
    CollapseModule,
    ReportsConciliationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot()
  ],
   providers: [
    {
      provide: MatPaginatorIntl,
      useClass: CustomMatPaginatorIntl,
    },
    DatePipe,
  ]
})
export class ReportsConciliationModule { }
