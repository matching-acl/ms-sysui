import { ReportsService } from './../services/reports.service';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { PageEvent } from '@angular/material/paginator';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Report, Details, Totaltrxsnotconciliated, Trx } from '../model/reports';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { IsLoadingService } from '@service-work/is-loading';
import { saveAs } from 'file-saver';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-reports-conciliation',
  templateUrl: './reports-conciliation.component.html',
  styleUrls: ['./reports-conciliation.component.scss']
})
export class ReportsConciliationComponent implements OnInit {

 
  totalItems : number;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [5, 10, 25];
  showFirstLastButtons = true;

  

  bsValue = new Date();
  bsRangeValue: Date[];
  maxDate = new Date();
  form: FormGroup;
  isCollapsed = false;
  colorTheme = 'theme-blue';
  bsConfig: Partial<BsDatepickerConfig>;
  locale = 'es';
  conciliaciones :any[];
  dataConciliation: Report[];
  transacciones: Trx[];
  totalConciliated: number;
  totalNotConciliated: Totaltrxsnotconciliated[];
  name1: string;
  name2: string;
  keys = [];
  values: any;
  code: string;
  interfaces: string;
  modalRef: BsModalRef;
  data = [];
  dataConciliada = [];
  originDest = [];
  noData = false;
  noDataMessage = 'No se encontraron registros para este filtro';
  typeReport = [
    {
      name: 'Conciliados',
      id : 1
    },
    {
      name: 'No conciliados',
      id : 0
    },
    {
      name: 'Todos',
      id : 2
    }
  ]
  constructor(private loading : IsLoadingService, private modalService: BsModalService,private localeService: BsLocaleService, private fb : FormBuilder, private reportService : ReportsService) {
     this.bsConfig = Object.assign({}, { containerClass: this.colorTheme, DateInputFormat: 'DD/MM/YYYY',  displayOneMonthRange: true, adaptivePosition: true });
    this.code = localStorage.getItem('ENTERPRISE_CODE');
    this.maxDate.setDate(this.maxDate.getDate());
    this.bsRangeValue = [this.bsValue, this.maxDate];
   }

  ngOnInit(): void {
    this.localeService.use(this.locale);
    this.createForm();
    this.getConciliation();
  }

 

  getConciliation() {
    this.loading.add(this.reportService.getConciliation(this.code).subscribe(data => {
      this.conciliaciones = data;
    }));

  }

  createForm() {
    this.form = this.fb.group({
      schema: this.code,
      totalConciliated: [
        {
          value: '',
          disabled : true
        }
      ],
      totalNotConciliated1: [
        {
          value: '',
          disabled : true
        }
      ],
      totalNotConciliated2: [
        {
          value: '',
          disabled : true
        }
      ],
      code: [
        {
          value: null,
          disabled : false
        },
        [Validators.required]
      ],
      type_report: [
        {
          value: 0,
          disabled : false
        },
        [Validators.required]
      ],
      start_date: ['', Validators.required],
      end_date: '',
    });
    
  }

  handlePageEvent(event: PageEvent) {
    this.totalItems = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    console.log('Page size',this.pageSize, );
    console.log('Page index',this.pageIndex);
    console.log('total Items', this.totalItems);
    this.filter(this.pageSize, this.pageIndex);
    
  }

  filter(pageSize? : number, pageIndex? : number) {
    
    const form = this.form.value;
    const date1 = this.bsRangeValue[0] ? this.bsRangeValue[0] : form.start_date[0];
    const date2 = this.bsRangeValue[1] ? this.bsRangeValue[1] : form.start_date[1];
    const startdate = new Date(date1.toString()).toLocaleDateString('en-GB');
    const endDate = new Date(date2.toString()).toLocaleDateString('en-GB');
    
    const forma = this.form.value;
    const body = {
      schema: forma.schema,
      code: forma.code,
      number_page: pageIndex? pageIndex +1 : 1,
      size_page: pageSize ? pageSize : 10,
      filters: {
        type_report : +forma.type_report,
        start_date: startdate,
        end_date: endDate,
      }
    }

    console.log(body);
    
    this.reportService.getTotalItemsConciliation(body).subscribe(data => {
      this.totalItems = data.total_items;
      console.log( this.totalItems);
    });


    this.loading.add(this.reportService.getReportsConciliation(body).subscribe(data => {
      this.dataConciliation = [];
      if (data.trxs.length === 0) {
        this.transacciones = []
        this.noData = true;
      } else {
         this.noData = false;
        this.dataConciliation = data;
        this.transacciones = data.trxs
        data['total_trxs_not_conciliated'].forEach(element => {
          if (element == null) {
            return;
          } else {
            this.totalNotConciliated = this.dataConciliation['total_trxs_not_conciliated'];
            this.form.get('totalConciliated').setValue(data.total_trxs_conciliated);
            const noC1 = this.totalNotConciliated[0].total_notC;
            const noC2 = this.totalNotConciliated[1].total_notC;
            this.name1 = this.totalNotConciliated[0].name;
            this.name2 = this.totalNotConciliated[1].name;
            this.form.get('totalNotConciliated1').setValue(noC1);
            this.form.get('totalNotConciliated2').setValue(noC2);
            
          }
        });
      }
    },
      (error: HttpErrorResponse) => {
        this.noData = true;
        this.noDataMessage = 'Error de servidor, favor inténtelo nuevamente.';
      })
    );
  }
  openModalKeys(template: TemplateRef<any>, details : any, origin : any, dest : any, interOrigin : any, interDest : any) {
    this.modalRef = this.modalService.show(template);
    this.modalRef.setClass('modal-lg');
    this.data = [];
    this.keys = [];
    this.originDest = [];
    this.dataConciliada = [];
    this.keys.push(origin, dest)
    this.originDest.push(interOrigin, interDest)
  }
  openModalData(template: TemplateRef<any>, data: any, status : any) {
    this.modalRef = this.modalService.show(template);
    
    this.data = [];
    this.keys = [];
    this.dataConciliada = [];
    if (status === 'NO CONCILIADO') {
       this.modalRef.setClass('modal-xs');
      this.data.push(data);
      return;
    }
    if (status === 'CONCILIADO') {
      this.modalRef.setClass('modal-lg');
      this.dataConciliada.push(data);
      return;
    }
  }

  downloadExcel() {
    this.reportService.downloadExcelConci().subscribe((data) => {
        if (data) {
        saveAs(data, 'conciliation_report.xls');
    }
    })
  }

}
