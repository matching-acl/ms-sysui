import { AuthGuardService } from './../../login/service/auth-guard.service';
import { ReportsConciliationComponent } from './reports-conciliation.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: ReportsConciliationComponent, canActivate: [AuthGuardService] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsConciliationRoutingModule { }
