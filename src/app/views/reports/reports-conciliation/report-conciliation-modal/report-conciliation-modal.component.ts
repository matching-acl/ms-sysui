import { Component, OnInit, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-report-conciliation-modal',
  templateUrl: './report-conciliation-modal.component.html',
  styleUrls: ['./report-conciliation-modal.component.scss']
})
export class ReportConciliationModalComponent implements OnInit {
  @Input() modal_ref: BsModalRef;
  @Input() keys: any[];
  @Input() data: any[];
  @Input() dataConciliada: any[];
  @Input() originDest: any[];

  llaves = false;
  datos = false;
  datosConci = false;
  constructor() { }
  ngOnInit(): void {
    
    if (this.keys.length > 0) {
      this.llaves = true;
    }
    if (this.data.length > 0) {
      this.datos = true;
    }
    if (this.dataConciliada.length > 0) {
      this.datosConci = true;
    }
   
  }

  handelCancel() {
    this.modal_ref.hide();
  }

  getKeys(item: any) {
    let html = ''
      for (const key in item) {
       if (Object.prototype.hasOwnProperty.call(item, key)) {
         const element = item[key];
         html += `<p> <strong>${key} : </strong>  <span> ${element} </span> </p><hr>`
       }
    }
    return html;
  }


}
