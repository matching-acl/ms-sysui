import { ConciliationService } from './../../config-conciliation/services/conciliation.service';
import { ReportsService } from './../services/reports.service';
import { Component, OnInit } from '@angular/core';
import { Interface } from '../../config-conciliation/model/template';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { PageEvent } from '@angular/material/paginator';
import {ENTERPRISE_CODE} from '../../login/models/login';
import { IsLoadingService } from '@service-work/is-loading';
import { HttpErrorResponse } from '@angular/common/http';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-reports-interfaces',
  templateUrl: './reports-interfaces.component.html',
  styleUrls: ['./reports-interfaces.component.scss']
})
export class ReportsInterfacesComponent implements OnInit {
  totalItems : number;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [5, 10, 25];
  showFirstLastButtons = true;
  noDataMessage = 'No se encontraron registros para este filtro';

   bsValue = new Date();
  bsRangeValue: Date[];
  maxDate = new Date();

  typeReport = [
    {
      name: 'Correctos',
      id : 1
    },
    {
      name: 'Incorrectos',
      id : 0
    },
    {
      name: 'Todos',
      id : 2
    }
  ]
  form: FormGroup;
  noData = false;
  code: string;
  isCollapsed = false;
  interfaces: Interface[];
  keys: any;
  values: any;
  dataReport: any[];
  colorTheme = 'theme-blue';
  bsConfig: Partial<BsDatepickerConfig>;
  locale = 'es';
  enterprise_code: string;


  constructor(private localeService: BsLocaleService, private reportService: ReportsService, private fb : FormBuilder, private loading : IsLoadingService) {
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme, DateInputFormat: 'DD/MM/YYYY',  displayOneMonthRange: true, adaptivePosition: true });
    this.code = localStorage.getItem('ENTERPRISE_CODE');
    this.maxDate.setDate(this.maxDate.getDate());
    this.bsRangeValue = [this.bsValue, this.maxDate];
   }

  ngOnInit(): void {
    console.log('current date', this.bsRangeValue);
    this.enterprise_code = localStorage.getItem(ENTERPRISE_CODE);
    this.createForm();
    this.getInterfaces();
	this.localeService.use(this.locale);
  }

  handlePageEvent(event: PageEvent) {
    this.totalItems = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.filter(this.pageIndex, this.pageSize)
  }

  getInterfaces() {
    this.reportService.getInterfaces(this.enterprise_code).subscribe((data) => {
      this.interfaces = data;
      console.log('Interfaces', this.interfaces);
    });
  }

  createForm() {
    this.form = this.fb.group({
      schema: this.code,
      code: [
        {
          value: null,
          disabled : false
        },
        [Validators.required]
      ],
      type_report: [
        {
          value: 0,
          disabled : false
        },
        [Validators.required]
      ],
      start_date: ['', Validators.required],
      end_date: null,
    });
  }

  filter(pageIndex? :number, pageSize? : number) {
    const form = this.form.value;
    const date1 = this.bsRangeValue[0] ? this.bsRangeValue[0] : form.start_date[0];
    const date2 = this.bsRangeValue[1] ? this.bsRangeValue[1] : form.start_date[1];
    const startdate = new Date(date1.toString()).toLocaleDateString('en-GB');
    const endDate = new Date(date2.toString()).toLocaleDateString('en-GB');

    console.log('fecha inicio', startdate);
    console.log('fecha fin', endDate);
    const forma = this.form.value;
    const body = {
      schema: forma.schema,
      code: forma.code,
      type_report: +forma.type_report,
      number_page: pageIndex? pageIndex+1 :1,
      size_page: pageSize? pageSize :10,
      filters: {
        start_date: startdate,
        end_date: endDate,
      }
    }

    this.reportService.getTotalItemsInterfaces(body).subscribe(data => {
      this.totalItems = data.total_items;
      console.log('Total items',this.totalItems);
    });
    
    this.loading.add(this.reportService.getReportsInterface(body).subscribe(data => {
      this.dataReport = [];
      this.dataReport = data;
      console.log(this.dataReport);
      if (this.dataReport.length === 0) {
        
        this.dataReport = [];
        this.noData = true;
      } else {
        this.noData = false;
        let vals: any = [];
         for (const i of this.dataReport) {
           const j = i.keys;
           const header = Object.keys(j);
            vals.push(Object(j));
           this.keys = header;
        }
        this.values = vals;

        let valores: any[] = [];
        let array: any[] = [];
        this.keys.forEach(element => {
          valores = [];
          console.log(element);
          this.values.forEach((v,k) => {
            Object.entries(v).forEach((value, key) => {
             if (element == value[0]) {
               valores.push(value[1]);
             }
            });
          });
          array.push(valores);
        });
        this.values = array;
          console.log(array);
          
          
      }  
    },
      (error: HttpErrorResponse) => {
        this.dataReport = [];
        this.noData = true;
        this.noDataMessage = 'Error de servidor, favor inténtelo nuevamente.';
      })
    );

  }

  downloadExcel() {
    console.log('descargar');
    this.reportService.downloadExcelInter().subscribe((data) => {
        if (data) {
        saveAs(data, 'interface_report.xls');
    }
    })
  }


}
