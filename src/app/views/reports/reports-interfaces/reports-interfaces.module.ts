import { ReportsInterfacesComponent } from './reports-interfaces.component';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PipesModule } from './../../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { MatPaginatorModule, MatPaginatorIntl } from '@angular/material/paginator';
import { ReportsInterfacesRoutingModule } from './reports-interfaces-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
import { CustomMatPaginatorIntl } from '../../paginator-es';
defineLocale('es', esLocale);

@NgModule({
  declarations: [ReportsInterfacesComponent],
  imports: [
    MatPaginatorModule,
    PipesModule,
    TooltipModule,
    BsDatepickerModule,
    CollapseModule,
    CommonModule,
    ReportsInterfacesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
   providers: [
    {
      provide: MatPaginatorIntl,
      useClass: CustomMatPaginatorIntl,
    },
    DatePipe,
  ]
})
export class ReportsInterfacesModule { }
