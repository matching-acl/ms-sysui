import { AuthGuardService } from './../../login/service/auth-guard.service';
import { ReportsInterfacesComponent } from './reports-interfaces.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: ReportsInterfacesComponent, canActivate: [AuthGuardService] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsInterfacesRoutingModule { }
