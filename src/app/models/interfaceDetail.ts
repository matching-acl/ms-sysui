export interface interfaceDetail{
    name: string; 
    length: number, 
    isEditable: boolean;
}