import { interfaceDetail } from "./interfaceDetail";

export interface currentInterface{
    active: boolean;
    created: Date;
    e_state: string;
    header: string;
    id: number;
    name: string;
    separator: string;
    separator_line: string;
    tc_template_id: number;
}