export interface Enterprise {
    enterpriseName     : string;
      customerName     : string;
      email            : string;
      username         : string;
      phone?           : number;
      password         : string;
      confirmPassword  : string;
}