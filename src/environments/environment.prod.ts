//coreApiUrl -> enroll
//pythonApiUrl-> core
export const environment = {
  production: true,
  pythonApiUrl: 'http://34.136.213.127:5000/',
  coreApiUrl: 'http://34.136.82.54:8080/',
  apiReport : 'http://35.224.37.58:5555/',
  goApiUrl: 'http://34.132.206.4:9090/',
  bashRegisterApiUrl: 'http://35.238.177.37:5500/'
};
