//coreApiUrl -> enroll
//pythonApiUrl-> core
export const environment = {
  production: true,
  pythonApiUrl: 'http://10.8.3.219:5000/',
  coreApiUrl: 'http://10.8.11.186:8080/',
  apiReport : 'http://10.8.2.80:5000/',
  goApiUrl: 'http://10.8.4.179:9090/',
  bashRegisterApiUrl: 'http://35.192.41.70:5500/'
};